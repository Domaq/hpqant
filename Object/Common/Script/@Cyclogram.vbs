Public Cyclograms

Class Frame
	Public Sub AddValve(valve)
		If Len(valve) = 0 Then Exit Sub
		Dim offname
		If Left(valve, 1) = "~" Then
			offname = Right(valve, Len(valve) - 1)
		Else
			offname = ""
		End If
		Dim i : For i = 0 To UBound(v)
			If UCase(v(i)) = UCase(valve) Then Exit Sub
			If UCase(v(i)) = UCase(offname) Then
				Dim j : For j = i To UBound(v) - 1
					v(j) = v(j + 1)
				Next
				ReDim Preserve v(UBound(v) - 1)
				Exit Sub
			End If
		Next
		ReDim Preserve v(UBound(v) + 1)
		v(UBound(v)) = valve
	End Sub
	Public Function GetValves
		GetValves = v
	End Function
	Public Time
	Public Action
	Public Period
	Private v
	Private Sub Class_Initialize
		Time = CLng(0)
		Action = ""
		v = Array()
		Period = 1
	End Sub
End Class

Class Cyclogram
	Public Sub Reset
		ImpossibleTime = CLng(1536000)
		f = Array()
		Size = 0
		Started = False
		FrameIndex = Size
		FrameTimeRest = ImpossibleTime
		InBeginOfFrame = False
		CyclesCounter = 1
	End Sub
	Public Sub StartAction
		FrameIndex = Size
		FrameTimeRest = ImpossibleTime
		Started = True
	End Sub
	Public Sub StopAction
		Started = False
		FrameIndex = Size
		FrameTimeRest = ImpossibleTime
		Threads.SetCurrentSub Name, ""
	End Sub
	Public Function IsStarted
		IsStarted = Started
	End Function
	Public Function GetValves
		If FrameIndex >= Size Then
			GetValves = Array()
		Else
			GetValves = f(FrameIndex).GetValves
		End If
	End Function
	Public Function GetActionSub
		If FrameIndex >= Size Or Not InBeginOfFrame Then
			GetActionSub = ""
		Else
			GetActionSub = f(FrameIndex).Action
		End If
	End Function
	Public Sub ToNextFrame
		FrameTimeRest = 0
	End Sub
	Public Sub Action
		InBeginOfFrame = False
		If Not Started Then Exit Sub
		If Size = 0 Then Exit Sub
		If FrameTimeRest >= ImpossibleTime Then
			If FrameIndex >= Size Then FrameIndex = 0
			FrameTimeRest = f(FrameIndex).Time - 1
			InBeginOfFrame = True
			Exit Sub
		End If
		'������������ �� ��������� ����
		If FrameTimeRest <= 0 Then
			Dim SkipFrame: SkipFrame = True
			While SkipFrame = True
			If FrameIndex >= Size - 1 Then
					CyclesCounter = CyclesCounter + 1
					If CyclesCounter > 100000 Then CyclesCounter = 0
				FrameIndex = 0
			Else
				FrameIndex = FrameIndex + 1
			End If
				'���� ������� �� ������� ����� ����� ���� - ������� �� ����� (�.�. ����������� ����)
				Dim period: period = f(FrameIndex).Period
				Dim rest: rest = CyclesCounter - (CyclesCounter \ period) * period
				If rest = 0 Then SkipFrame = False
			Wend
			FrameTimeRest = f(FrameIndex).Time - 1
			InBeginOfFrame = True
		Else
			FrameTimeRest = FrameTimeRest - 1
		End If
	End Sub
	Public Function ConstructCyclogram
		With Form.IniFile
			Dim i, j
			Dim TempParameter : TempParameter = ""
			.Read Cyclograms.IniFileName
			.Section = "Cyclogram:" & Name
			Dim ActualNames : ActualNames = Array()
			Dim TemplateNames : TemplateNames = Array()
			If .NumberOfParameters = 0 Then
				ActualNames = Split(Name, ":")
				If UBound(ActualNames) < 1 Then
					InformAboutError
					ConstructCyclogram = False
					Exit Function
				End If
				Dim sectnames : sectnames = .AllSections
				Dim sect : For Each sect In sectnames
					TemplateNames = Split(sect, ":")
					If UBound(TemplateNames) > 1 Then
						If UCase(TemplateNames(0)) = UCase("Cyclogram") Then
							If UBound(TemplateNames) - 1 = UBound(ActualNames) Then
								For i = 0 To UBound(TemplateNames) - 1
									TemplateNames(i) = TemplateNames(i + 1)
								Next
								ReDim Preserve TemplateNames(UBound(TemplateNames) - 1)
								If UCase(TemplateNames(0)) = UCase(ActualNames(0)) Then
									If UBound(TemplateNames) = UBound(ActualNames) Then
										.Section = sect
										Exit For
									End If
								End If
							End If
						End If
					End If
				Next
				If .NumberOfParameters = 0 Then
					InformAboutError
					ConstructCyclogram = False
					Exit Function
				End If
			End If
			Dim parnames : parnames = .AllParameters
			Dim par : For Each par In parnames
				.Parameter(par)
				.Switch("Time")
				Dim tm : tm = .SwitchValue
				If Not IsNumeric(tm) Then tm = CLng(0)
				tm = CLng(tm)
				If tm <> 0 Then
					If tm < 0 Then tm = ImpossibleTime
					Dim TempFrame : Set TempFrame = New Frame
					TempFrame.Time = tm
					Dim allvalves : allvalves = Array()
					.Switch("Assemblies")
					Dim items
					items = .AllItems
					If Not IsEmpty(items) Then 
						For i = 0 To UBound(items)
							If Left(items(i), 1) = "@" Then
								Dim found : found = False
								For j = 1 To UBound(TemplateNames)
									If UCase(items(i)) = UCase(TemplateNames(j)) Then
										items(i) = ActualNames(j)
										found = True
										Exit For
									End If
								Next
								If found = False Then
									InformAboutError
									ConstructCyclogram = False
									Exit Function
								End If
							End If
							Form.AddArray allvalves, ValvesAssemblies.GetAssembly(items(i))
						Next
					End If
					.Switch("Valves")
					items = .AllItems
					Form.AddArray allvalves, items
					Dim tildaarr : tildaarr = Array()
					Dim arr : arr = Array()
					For i = 0 To UBound(allvalves)
						If Left(allvalves(i), 1) = "~" Then
							ReDim Preserve tildaarr(UBound(tildaarr) + 1)
							tildaarr(UBound(tildaarr)) = allvalves(i)
						Else
							ReDim Preserve arr(UBound(arr) + 1)
							arr(UBound(arr)) = allvalves(i)
						End If
					Next
					Form.AddArray arr, tildaarr
					For i = 0 To UBound(arr)
						TempFrame.AddValve arr(i)
					Next
					Form.IniFile.Switch("Action")
					TempFrame.Action = Form.IniFile.SwitchValue
					Form.IniFile.Switch("Period")
					If Form.IniFile.SwitchValue <> "" Then TempFrame.Period = Form.IniFile.SwitchValue
					If TempFrame.Period < 1 Then TempFrame.Period = 1					
					AddFrame TempFrame
				End If
			Next
			ConstructCyclogram = True
		End With
	End Function
	Private Sub AddFrame(ByVal fr)
		Size = Size + 1
		ReDim Preserve f(Size - 1)
		Set f(Size - 1) = fr
		FrameIndex = Size
	End Sub
	Private Sub InformAboutError
		MsgBox "���������� �������� ����������� " & Name & vbCrLf & vbCrLf _
				& "������ Cyclogram:" & Name & " ����������� � ini-�����" _
				, vbOKOnly, "������"
	End Sub
	Public Name
	Private f
	Private Size
	Private Started
	Private FrameIndex
	Private FrameTimeRest
	Private InBeginOfFrame
	Private ImpossibleTime
	Private CyclesCounter
	Private Sub Class_Initialize
		Reset
	End Sub
End Class

Class CyclogramCollection
	Public Sub Reset
		Set c = Nothing
		c = Array()
		InStoppingProcess = False
	End Sub
	Public Sub AddCyclogram(ByVal cl)
		Dim i : For i = 0 To UBound(c)
			If UCase(c(i).Name) = UCase(cl.Name) Then
				Set c(i) = Nothing
				Set c(i) = cl
				Exit Sub
			End If
		Next
		ReDim Preserve c(UBound(c) + 1)
		Set c(UBound(c)) = cl
	End Sub
	Public Function StartCyclogram(Name)
		Dim found : found = False
		If ConstructCyclogram(Name) Then
			Dim cc : For Each cc In c
				If UCase(cc.Name) = UCase(Name) Then
					cc.StartAction
					StartCyclogram = True
					found = True
				End If
			Next
		End If
		If found Then
			VCU.rbtAuto.Value = True
			Exit Function
		End If
		StartCyclogram = False
	End Function
	Public Function StopCyclogram(Name)
		Dim cc : For Each cc In c
			If UCase(cc.Name) = UCase(Name) Then
				cc.StopAction
				StopCyclogram = True
				Exit Function
			End If
		Next
		StopCyclogram = False
	End Function
	Public Function IsCyclogramStarted(Name)
		Dim cc : For Each cc In c
			If UCase(cc.Name) = UCase(Name) Then
				IsCyclogramStarted = cc.IsStarted
				Exit Function
			End If
		Next
		IsCyclogramStarted = False
	End Function
	Public Function IsAnyCyclogramStarted
		Dim cc : For Each cc In c
			If cc.IsStarted Then
				IsAnyCyclogramStarted = True
				Exit Function
			End If
		Next
		IsAnyCyclogramStarted = False
	End Function
	Public Sub StopSelectedCyclograms(NameTemplate)
		Dim cc : For Each cc In c
			If CompareStringBegin(NameTemplate, cc.Name) Then
				cc.StopAction
			End If
		Next
	End Sub
	Public Sub StopAllCyclograms
		Dim cc : For Each cc In c
			cc.StopAction
		Next
		InStoppingProcess = True
	End Sub
	Public Sub ToNextFrame(Name)
		Dim cc : For Each cc In c
			If UCase(cc.Name) = UCase(Name) Then
				cc.ToNextFrame
				Exit Sub
			End If
		Next
	End Sub
	Public Sub Action
'		If VCU.rbtAuto.Value = False And Not InStoppingProcess Then Exit Sub
		If Not IsAnyCyclogramStarted Then Exit Sub
		Dim allValves : allValves = Array()
		Dim cc : For Each cc In c
			cc.Action
			Dim actionSub : actionSub = cc.GetActionSub
			If actionSub <> "" Then
				Threads.SetCurrentSub cc.Name, actionSub
			End If
			Form.AddArray allValves, cc.GetValves
		Next
		Dim vv : For Each vv In VCU.Valves
			Dim found : found = False
			Dim v : For Each v In allValves
				If v = UCase(vv.Title & vv.TitleExt) Then
					vv.ButtonChecked = True
					found = True
				End If
			Next
			If found = False Then
				vv.ButtonChecked = False
			End If
		Next
		If InStoppingProcess Then
			InStoppingProcess = False
		End If
	End Sub
	Private Function ConstructCyclogram(Name)
		Dim TempCyclogram : Set TempCyclogram = New Cyclogram
		TempCyclogram.Name = Name
		If TempCyclogram.ConstructCyclogram = True Then
			AddCyclogram TempCyclogram
			ConstructCyclogram = True
		End If
		ConstructCyclogram = True
	End Function
	Public IniFileName
	Private c
	Private InStoppingProcess
	Private Sub Class_Initialize
		IniFileName = ""
		Reset
	End Sub
End Class

Sub OnCyclogram
	Cyclograms.Action
End Sub

Public Sub InitializeCyclograms
	Set Cyclograms = New CyclogramCollection
	Cyclograms.IniFileName = IniDir & "Cyclograms.ini"
End Sub
