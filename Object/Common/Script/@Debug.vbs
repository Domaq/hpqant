Public StartSymulationTime
Public TimeFromSymulationFile		'����� ���������, ���������� �� ����� � ������� � ������� SymulateNextLine

Public ProcessedData				'������������ ������

Sub Dump(ByRef what)
	'On Error Resume Next
	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
	Dim fso, fl
	Set fso = CreateObject("Scripting.FileSystemObject")

	Dim i
	Dim Size
	Dim names
	Dim text
	Dim fname
	Dim tmpData
	Dim ItemFrameSize : ItemFrameSize = 13
	Dim NumOfSpaces : NumOfSpaces = 1
	Dim IsSymulation: IsSymulation = IsButtonPressed("���������")
	
	If what = "Sensors" Then
		names = Array("")
		tmpData = Array("")
		ReDim names(NumberOfSensors - 1)			
		ReDim tmpData(NumberOfSensors - 1)
		Size = -1
		For Each s In VCU.Sensors
			Size = Size + 1
			names(Size) = CStr(s.Title)
			tmpData(Size) = CStr(s.Value)
		Next
		ReDim Preserve names(Size)
		ReDim Preserve tmpData(Size)
		fname = ArchiveDir & "Sensors"
	ElseIf what = "DebugSensors" Then
		names = Array("")
		ReDim names(NumberOfSensors - 1)
		Size = UBound(names)
		For i = 0 To Size
			names(i) = Format(i + 1, 2, 0)
		Next
		fname = ArchiveDir & "DebugSensors"
	Else
		names = AttributesOfGases.GetAllNames
		Size = UBound(names)
		If what = "RawData" Then
			fname = ArchiveDir & "RawData"
		ElseIf what = "DataWithoutFone" Then 
			fname = ArchiveDir & "DataWithoutFone"
		ElseIf what = "ProcessedData" Then 
			fname = ArchiveDir & "ProcessedData"
		ElseIf what =  "FinalData" Then
			fname = ArchiveDir & "Percents"
		Else
			OutLog "Unknown dump " & what
			Exit Sub
		End If
	End If

	If IsSymulation Then
		fname = fname & " " & StartSymulationTime & " sym.out"
	Else
		fname = fname & " " & Form.GTime.FormatDate(CurrentDateTime) & ".out"
	End If
		
	If fso.FileExists(fname) = False Then
		Set fl = fso.OpenTextFile(fname, ForWriting, True)
		' � ������ �����
		text = "/td .x"
		For i = 0 To Size
			text = text & "y"
		Next
		fl.WriteLine text
		Dim Color
		For i = 0 To Size
			Color = i + 1
			While Color > 16
				Color = Color - 16
			Wend
			text = "/sa m " & Color & " " & i + 3 & " ;mark, dash & color for column " & i + 3
			fl.WriteLine text
		Next
		fl.WriteLine "/sc ON ;connect points"
		fl.WriteLine "/sm OFF ;data marks"
		fl.WriteLine "/sd OFF ;dashed lines"
		fl.WriteLine "/sa d 0.50 ;data mark size"
		For i = 0 To Size
			text = "/sa l  """ & names(i) & """ " & i + 3 & " ;legend for column " & i + 3
			fl.WriteLine text
		Next
		text = "; ����� ������          "
		'     "01.12.2006 14:17:36.123"
		For i = 0 To Size
			If ItemFrameSize < Len(names(i)) Then
				NumOfSpaces = 1
			Else
				NumOfSpaces = ItemFrameSize - Len(names(i)) + 1
			End If
			text = text & names(i) & Space(NumOfSpaces)
		Next
		fl.WriteLine text
		fl.Close
	End If

	Set fl = fso.OpenTextFile(fname, ForAppending)
	text = ""

	Dim dt
	If IsSymulation Then
		dt = TimeFromSymulationFile
	Else
		dt = Form.GTime.FormatDate(CurrentDateTime) & " " & Form.GTime.FormatTimeExact(CurrentDateTime)
	End If
	
	NumOfSpaces = 3
	text = dt & Space(NumOfSpaces)
	' For "Sensors" tmpData defined above
	If what = "RawData" Then tmpData = RawData.GetValues("�������")
	If what = "DataWithoutFone" Then tmpData = RawDataWithoutFone.GetValues("�������")
	If what = "FinalData" Then tmpData = Percents.GetValues("�������")
	If what = "ProcessedData" Then tmpData = ProcessedData.GetValues("�������")
	For i = 0 To Size
		Dim Item
		Select Case what
		Case "Sensors"
			'Item = Format(Debug.Sensors(i).Value, 8, 0)
			Item = Format(tmpData(i), 4, 3)
		Case "DebugSensors"
			Item = Format(Debug.Sensors(i).Value, 8, 0)
		Case "RawData"
			Item = Format(tmpData(i), 8, 3)
		Case "DataWithoutFone"
			Item = Format(tmpData(i), 8, 3)
		Case "FinalData"
			Item = Format(tmpData(i), 8, 4)
		Case "ProcessedData"
			Item = Format(tmpData(i), 8, 3)
		Case Else
			Item = Format(tmpData(i), 8, 3)
		End Select
		NumOfSpaces = ItemFrameSize - Len(Item) + 1
		text = text & Item & Space(NumOfSpaces)
	Next
	fl.WriteLine text

	fl.Close
	Set fso = Nothing
End Sub

Sub WriteLog(ByVal text)
'	On Error Resume Next
	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
	Dim fso, fl
	Set fso = CreateObject("Scripting.FileSystemObject")

	Dim fname
	fname = ArchiveDir & "Log"
	fname = fname & " " & Form.GTime.FormatDate(CurrentDateTime) & ".txt"

	If fso.FileExists(fname) = False Then
		Set fl = fso.OpenTextFile(fname, ForWriting, True)
	Else
		Set fl = fso.OpenTextFile(fname, ForAppending)
	End If

	Dim dt : dt = Form.GTime.FormatDate(CurrentDateTime) & " " & Form.GTime.FormatTimeExact(CurrentDateTime)
	text = dt & Space(3) & text
	fl.WriteLine text

	fl.Close
	Set fl = Nothing
	Set fso = Nothing
End Sub

Sub EnableParameters
	EnableParametersButtons
	EnableParametersTextboxes
End Sub

Sub DisableParameters
	DisableParametersButtons
	DisableParametersTextboxes
End Sub

Sub EnableParametersButtons
	For Each c In Debug.btnParameters
		c.Enabled = True
	Next
End Sub

Sub DisableParametersButtons
	For Each c In Debug.btnParameters
		c.Enabled = False
	Next
End Sub

Sub EnableParametersTextboxes
	For Each c In Debug.tbParameters
		c.Enabled = True
	Next
End Sub

Sub DisableParametersTextboxes
	For Each c In Debug.tbParameters
		c.Enabled = False
	Next
End Sub

Sub EnableIndicatorsButtons
	For Each c In Debug.btnIndicators
		c.Enabled = True
	Next
End Sub

Sub DisableIndicatorsButtons
	For Each c In Debug.btnIndicators
		c.Enabled = False
	Next
End Sub

Sub EnableIndicatorsTextboxes
	For Each c In Debug.tbIndicators
		c.Enabled = True
	Next
End Sub

Sub DisableIndicatorsTextboxes
	For Each c In Debug.tbIndicators
		c.Enabled = False
	Next
End Sub
