Const ForReading = 1, ForWriting = 2, ForAppending = 8
Const DeviceStateOn = 0, DeviceStateOff = 1, DeviceStateBad = 2, DeviceStateUnknown = 3

Class NamedCode
	Private Sub Class_Initialize
		Name = ""
		Code = CLng(0)
	End Sub
	Public Name
	Public Code
End Class

Class CodesCollection
	Public Sub Reset
		Set codes = Nothing
		codes = Array()
	End Sub
	Public Sub AddCode(n, c)
		Dim i : For i = 0 To UBound(codes)
			If UCase(codes(i).Name) = UCase(n) Then
				If Not IsNumeric(c) Then Exit Sub
				codes(i).Code = CLng(c)
				Exit Sub
			End If
		Next
		If Not IsNumeric(c) Then Exit Sub
		ReDim Preserve codes(UBound(codes) + 1)
		Dim tmp : Set tmp = New NamedCode
		tmp.Name = n
		tmp.Code = CLng(c)
		Set codes(UBound(codes)) = tmp
	End Sub
	Public Function GetCode(Name)
		Dim c : For Each c In codes
			If UCase(c.name) = UCase(Name) Then
				GetCode = c.Code
				Exit Function
			End If
		Next
		GetCode = CLng(0)
	End Function
	Private codes
	Private Sub Class_Initialize
		Reset
	End Sub
End Class

Class NamedText
	Private Sub Class_Initialize
		Name = ""
		Text = ""
	End Sub
	Public Name
	Public Text
End Class

Class TextsCollection
	Public Sub Reset
		Set texts = Nothing
		texts = Array()
		Name = ""
	End Sub
	Public Sub AddText(n, t)
		Dim i : For i = 0 To UBound(texts)
			If UCase(texts(i).Name) = UCase(n) Then
				texts(i).Text = t
				Exit Sub
			End If
		Next
		ReDim Preserve texts(UBound(texts) + 1)
		Dim tmp : Set tmp = New NamedText
		tmp.Name = n
		tmp.Text = t
		Set texts(UBound(texts)) = tmp
	End Sub
	Public Function GetText(Name)
		Dim t : For Each t In texts
			If UCase(t.Name) = UCase(Name) Then
				GetText = t.Text
				Exit Function
			End If
		Next
		GetText = ""
	End Function
	Public Function GetTexts
		ReDim ts(UBound(texts))
		Dim i : For i = 0 To UBound(texts)
			Dim tmp : Set tmp = New NamedText
			tmp.Name = texts(i).Name
			tmp.Text = texts(i).Text
			Set ts(i) = tmp
		Next
		GetTexts = ts
	End Function
	Public Sub WriteTexts
		If Name = "" Then Exit Sub
		Dim fso : Set fso = CreateObject("Scripting.FileSystemObject")

		Dim i

		Dim fname : fname = SaveDir & Name & ".save"
		Dim fl : Set fl = fso.OpenTextFile(fname, ForWriting, True)

		Dim t : For Each t In texts
			Dim Str : Str = t.name & " = " & Chr(34) & t.Text & Chr(34)
			fl.WriteLine Str
		Next

		fl.Close
		Set fso = Nothing
	End Sub
	Public Sub ReadTexts
		If Name = "" Then Exit Sub
		Dim fso : Set fso = CreateObject("Scripting.FileSystemObject")

		Dim fname : fname = SaveDir & Name & ".save"
		If fso.FileExists(fname) = False Then
'			MsgBox "Can't read " & fname, vbOkOnly
			Exit Sub
		End If
		Dim fl : Set fl = fso.OpenTextFile(fname, ForReading)

		Do While fl.AtEndOfStream <> True
			Str = fl.ReadLine
			If Len(Str) < 3 Then
				fl.Close
				Set fso = Nothing
				Exit Sub
			End If
			Str = Trim(Str)
			Dim pos : pos = InStr(Str, "=")
			If pos > 1 Then
				Dim n : n = Left(Str, pos - 1)
				n = Trim(n)
				Dim t : t = Right(Str, Len(Str) - pos)
				t = Trim(t)
				If Left(t, 1) = Chr(34) Then t = Right(t, Len(t) - 1)
				If Len(t) <= 1 Then
					t = ""
				Else
					If Right(t, 1) = Chr(34) Then t = Left(t, Len(t) - 1)
				End If
				AddText n, t
			End If
		Loop
		fl.Close
		Set fso = Nothing
	End Sub
	Public Name
	Private texts
	Private Sub Class_Initialize
		Reset
	End Sub
End Class

Class ThreadsCollection
	Public Sub SetCurrentSub(thname, subname)
		th.AddText thname, subname
	End Sub
	Public Sub Run
		Debug.Threads.Clear
		Dim ts : ts = th.GetTexts
		Dim t : For Each t In ts
			Debug.Threads.AddItem t.name & " = " & t.text
			If t.text <> "" Then
				SC.Run t.text
			End If
		Next
	End Sub
	Private Function CheckExistance(thname)
		Dim ts : ts = th.GetTexts
		Dim t : For Each t In ts
			If UCase(t.Name) = UCase(thname) Then
				CheckExistance = True
				Exit Function
			End If
		Next
		CheckExistance = False
	End Function
	Private th
	Private Sub Class_Initialize
		Set th = New TextsCollection
	End Sub
End Class

Class FlagsCollection
	Public Sub SetFlag(Name, Val)
		Dim i : For i = 0 To UBound(names)
			If UCase(names(i)) = UCase(Name) Then
				f(i) = CBool(Val)
				Exit Sub
			End If
		Next
		ReDim Preserve names(UBound(names) + 1)
		names(UBound(names)) = Name
		ReDim Preserve f(UBound(f) + 1)
		f(UBound(f)) = CBool(Val)
	End Sub
	Public Function GetFlag(Name)
		Dim i : For i = 0 To UBound(names)
			If UCase(names(i)) = UCase(Name) Then
				GetFlag = f(i)
				Exit Function
			End If
		Next
		GetFlag = False
	End Function
	Public Sub SetSelectedFlags(NameTemplate, Val)
		Dim i : For i = 0 To UBound(names)
			If CompareStringBegin(NameTemplate, names(i)) Then
				f(i) = CBool(Val)
			End If
		Next
	End Sub
	Public Function IfAny
		If GetSize = 0 Then
			IfAny = False
			Exit Function
		End If
		Dim ff : For Each ff In f
			If ff = True Then
				IfAny = True
				Exit Function
			End If
		Next
		IfAny = False
	End Function
	Public Function WhichOnly
		WhichOnly = ""
		If GetSize = 0 Then
			Exit Function
		End If
		Dim only : only = False
		Dim i : For i = 0 To UBound(names)
			If f(i) = True Then
				If only = False Then
					WhichOnly = names(i)
					only = True
				Else
					WhichOnly = ""
					Exit Function
				End If
			End If
		Next
	End Function
	Public Function GetSize
		GetSize = UBound(names) + 1
	End Function
	Private f
	Private names
	Private Sub Class_Initialize
		f = Array()
		names = Array()
	End Sub
End Class

Class NamesCollection
	Public Sub AddName(Name)
		Dim n : For Each n In names
			If UCase(n) = UCase(Name) Then
				Exit Sub
			End If
		Next
		ReDim Preserve names(UBound(names) + 1)
		names(UBound(names)) = Name
	End Sub
	Public Function GetSize
		GetSize = UBound(names) + 1
	End Function
	Public Function GetIndex(Name)
		Dim i : For i = 0 To UBound(names)
			If UCase(names(i)) = UCase(Name) Then
				GetIndex = i
				Exit Function
			End If
		Next
		GetIndex = -1
	End Function
	Public Function GetIndexes(NameTemplate)
		Dim arr : arr = Array()
		Dim i : For i = 0 To UBound(names)
			If Len(names(i)) >= Len(NameTemplate) Then
				If Left(UCase(names(i)), Len(NameTemplate)) = UCase(NameTemplate) Then
					ReDim Preserve arr(UBound(arr) + 1)
					arr(UBound(arr)) = i
				End If
			End If
		Next
		GetIndexes = arr
	End Function
	Public Function GetName(index)
		If index >= 0 And index <= UBound(names) Then
			GetName = names(index)
		Else
			GetName = ""
		End If
	End Function
	Public Function GetNames
		ReDim Preserve arr(UBound(names))
		Dim i : For i = 0 To UBound(names)
			arr(i) = names(i)
		Next
		GetNames = arr
	End Function
	Private names
	Private Sub Class_Initialize
		names = Array()
	End Sub
End Class

Class ArraysCollection
	Public Sub AddArray(ByVal aname, ByVal aa)
		If Not IsArray(aa) Then
			MsgBox "ArraysCollection.AddArray : Can't add not array by name " & aname, vbOkOnly
			Exit Sub
		End If
		Dim found : found = False
		Dim i : For i = 0 To UBound(names)
			If UCase(aname) = UCase(names(i)) Then
				found = True
				Exit For
			End If
		Next
		If found = False Then
			ReDim Preserve names(UBound(names) + 1)
			names(UBound(names)) = aname
			ReDim Preserve a(UBound(a) + 1)
			i = UBound(a)
		End If
		a(i) = aa
	End Sub
	Public Function GetArray(aname)
		Dim found : found = False
		Dim i : For i = 0 To UBound(names)
			If UCase(names(i)) = UCase(aname) Then
				found = True
				Exit For
			End If
		Next
		ReDim aa(1)
		If found = True Then
			VectorToVector a(i), aa
		End If
		GetArray = aa
	End Function
	Public Sub WriteArray(aname)
		Dim fso, fl
		Set fso = CreateObject("Scripting.FileSystemObject")

		Dim i
		Dim Size
		Dim onames
		Dim Str
		Dim ItemFrameSize : ItemFrameSize = 13
		Dim NumOfSpaces : NumOfSpaces = 1

		onames = AttributesOfGases.GetAllNames
		Size = UBound(onames)

		Dim fname : fname = SaveDir & Name & "_" & aname & ".save"
		Set fl = fso.OpenTextFile(fname, ForWriting, True)
		Str = ""
		For i = 0 To Size
			NumOfSpaces = ItemFrameSize - Len(onames(i)) + 1
			Str = Str & onames(i) & Space(NumOfSpaces)
		Next
		fl.WriteLine Str

		Dim arr : arr = GetArray(aname)
		If UBound(arr) <> Size Then
			MsgBox "Can't write " & fname & " - array is empty or size mismatch", vbOkOnly
			Exit Sub
		End If
		Str = ""
		For i = 0 To Size
			Dim Item : Item = Format(arr(i), 8, 3)
			NumOfSpaces = ItemFrameSize - Len(Item) + 1
			Str = Str & Item & Space(NumOfSpaces)
		Next
		fl.WriteLine Str

		fl.Close
		Set fso = Nothing
	End Sub
	Public Sub ReadArray(aname)
		Dim fso : Set fso = CreateObject("Scripting.FileSystemObject")

		Dim fname : fname = SaveDir & Name & "_" & aname & ".save"
		If fso.FileExists(fname) = False Then
'			MsgBox "Can't read " & fname, vbOkOnly
			Exit Sub
		End If
		Dim fl : Set fl = fso.OpenTextFile(fname, ForReading)

		Dim inames : inames = AttributesOfGases.GetAllNames
		Dim Str
		Str = fl.ReadLine
		If Len(Str) < UBound(inames) * 14 - 1 Then
			fl.Close
			Set fso = Nothing
			Exit Sub
		End If
		Dim i
		For i = 0 To UBound(inames)
			inames(i) = Trim(Mid(Str, i * 14 + 1, 13))
		Next

		Str = fl.ReadLine
		If Len(Str) < (UBound(inames) + 1) * 14 - 1 Then
			fl.Close
			Set fso = Nothing
			Exit Sub
		End If
		ReDim aa(UBound(inames))
		For i = 0 To UBound(inames)
			aa(GetGasIndex(inames(i))) = CDbl(Trim(Mid(Str, i * 14 + 1, 13)))
		Next
'OutVector aa
		AddArray aname, aa
		fl.Close
		Set fso = Nothing
	End Sub
	Private Sub Class_Initialize
		Name = ""
		a = Array()
		names = Array()
	End Sub
	Public Name
	Private names
	Private a
End Class

Function CompareStringBegin(template, s)
	If Len(template) > Len(s) Then
		CompareStringBegin = False
		Exit Function
	End If
	If UCase(template) = UCase(Left(s, Len(template))) Then
		CompareStringBegin = True
	Else
		CompareStringBegin = False
	End If
End Function

Function GetByte(ByVal value, total, number)
	If Not IsNumeric(value) Or number < 1 Or total < 1 Or total > 4 Or number > total Then
		GetByte = 0
		Exit Function
	End If
	If value > 2147483647 Then
		value = 2147483647
	End If
	If value < -2147483648 Then
		value = -2147483648
	End If
	value = CLng(value)
	Dim bt : bt = 0
	If value < 0 Then
		value = 2147483647 + value + 1
		bt = GetByte(value, total, number)
		If number = 4 Then
			bt = bt + 128
		End If
		GetByte = bt
		Exit Function
	End If
	Dim i : For i = total To 1 step -1
		bt = Int(CLng(value) / 256 ^ (i - 1))
		If i = number Then
			GetByte = bt
			Exit Function
		End If
		value = value - bt * 256 ^ (i - 1)
	Next
	GetByte = 0
End Function

Function GetIntegerFromArray(arr, begin, length, issined)
	If Not IsArray(arr) Or begin < 0 Or begin > UBound(arr) - length + 1 _
			Or length < 1 Or length > 4 Then
		GetIntegerFromArray = 0
		Exit Function
	End If
	Dim value : value = 0
	Dim i
	If issined And arr(begin) >= 128 Then
		value = (arr(begin) - 128) * 256 ^ (length - 1)
		For i = 1 To length - 1
			value = value + (arr(begin + i)) * 256 ^ (length - 1 - i)
		Next
		value = -(2 ^ (8 * length - 2) - value + 2 ^ (8 * length - 2))
	Else
		For i = 0 To length - 1
			value = value + (arr(begin + i)) * 256 ^ (length - 1 - i)
		Next
	End If
	GetIntegerFromArray = value
End Function

Function GetArrayOfIntegers(arr, shift, intsize, numofints, issined)
	If Not IsArray(arr) Or intsize < 1 Or intsize > 4 Or numofints < 1 _
			Or numofints > (UBound(arr) + 1 - shift) / intsize Then
			GetArrayOfIntegers = Array()
			Exit Function
	End If
	ReDim result(numofints - 1)
	Dim i : For i = 0 To numofints - 1
		result(i) = GetIntegerFromArray(arr, shift + i * intsize, intsize, issined)
	Next
	GetArrayOfIntegers = result
End Function

'=====================================================================================
Class ExtractorOfIntegers
	Public Sub SetSource(ByVal a)
		If IsArray(a) Then arr = a
	End Sub
	Public Sub SetShift(ByVal s)
		shift = s
	End Sub
	Public Sub SetAmount(ByVal am)
		amount = am
	End Sub
	Public Sub SetLength(ByVal l)
		length = l
	End Sub
	Public Sub SetIsSigned(ByVal sn)
		signed = CBool(sn)
	End Sub
	Public Sub SetIsIntelOrder(ByVal o)
		order = CBool(o)
	End Sub
	Public Function GetArray
		If Not Check Then
			GetArray = Array()
			Exit Function
		End If
		ReDim result(amount - 1)
		Dim i : For i = 0 To amount - 1
			result(i) = InternalGetInteger(shift + i * length)
		Next
		curshift = shift
		GetArray = result
	End Function
	Public Function GetInteger
		Dim tmpAmount : tmpAmount = amount
		amount = 1
		If Not Check Then
			amount = tmpAmount
			GetInteger = 0
			Exit Function
		End If
		amount = tmpAmount
		GetInteger = InternalGetInteger(shift)
	End Function
	Private Function Check
		If Not IsArray(arr) Or length < 1 Or length > 4 Or amount < 1 _
				Or shift < 0 _
				Or amount > (UBound(arr) + 1 - shift) / length Then
			Check = False
			Exit Function
		End If
		Check = True
	End Function
	Private Function InternalGetInteger(curshift)
		Dim value : value = 0
		ReDim essential(length - 1)
		For i = 0 To length - 1
			If order = True Then
				essential(i) = arr(curshift + i)
			Else
				essential(i) = arr(curshift + length - 1 - i)
			End If
		Next
		Dim i
		For i = 0 To length - 2
			value = value + (essential(i)) * 256 ^ i
		Next
		If signed And essential(length - 1) >= 128 Then
			value = value + (essential(length - 1) - 128) * 256 ^ (length - 1)
			value = -(2 ^ (8 * length - 2) - value + 2 ^ (8 * length - 2))
		Else
			value = value + (essential(length - 1)) * 256 ^ (length - 1)
		End If
		InternalGetInteger = value
	End Function
	Private Sub Class_Initialize
		arr = Array()
		shift = 0
		amount = 1
		length = 0
		signed = False
		order = False
	End Sub
	Private arr
	Private shift
	Private amount
	Private length
	Private signed
	Private order
End Class

'=====================================================================================
Class ExtractorOfNumericals
	Public Sub SetSource(ByVal a)
		If IsArray(a) Then arr = a
	End Sub
	Public Sub SetShift(ByVal s)
		shift = s
	End Sub
	Public Sub SetAmount(ByVal am)
		amount = am
	End Sub
	Public Sub SetLength(ByVal l)
		length = l
	End Sub
	Public Sub SetIsSigned(ByVal sn)
		signed = CBool(sn)
	End Sub
	Public Sub SetIsIntelOrder(ByVal o)
		order = CBool(o)
	End Sub
	Public Sub SetType(ByVal t)
		dtype = t
	End Sub
	Public Function GetArray
		If Not Check Then
			GetArray = Array()
			Exit Function
		End If
		ReDim result(amount - 1)
		Dim i : For i = 0 To amount - 1
			result(i) = InternalGetNumerical(shift + i * length)
		Next
		GetArray = result
	End Function
	Public Function GetNumerical
		Dim saveAmount : saveAmount = amount
		amount = 1
		If Not Check Then
			amount = saveAmount
			GetNumerical = 0
			Exit Function
		End If
		amount = saveAmount
		GetNumerical = InternalGetNumerical(shift)
	End Function
	Private Function Check
		If Not IsArray(arr) Or length < 1 Or length > 4 Or amount < 1 _
				Or shift < 0 _
				Or amount > (UBound(arr) + 1 - shift) / length Then
			Check = False
			Exit Function
		End If
		Check = True
	End Function
	Private Function InternalGetNumerical(curshift)
		Dim value : value = 0
		ReDim essential(length - 1)
		For i = 0 To length - 1
			If order = True Then
				essential(i) = arr(curshift + i)
			Else
				essential(i) = arr(curshift + length - 1 - i)
			End If
		Next
		Dim i
		If dtype = 1 Then signed = True
		For i = 0 To length - 2
			value = value + (essential(i)) * 256 ^ i
		Next
		If signed And essential(length - 1) >= 128 Then
			value = value + (essential(length - 1) - 128) * 256 ^ (length - 1)
			value = -(2 ^ (8 * length - 2) - value + 2 ^ (8 * length - 2))
		Else
			value = value + (essential(length - 1)) * 256 ^ (length - 1)
		End If
		If dtype = 1 Then
			value = Form.LongRepresentationToSingle(value)
		End If
		InternalGetNumerical = value
	End Function
	Private Sub Class_Initialize
		arr = Array()
		shift = 0
		amount = 1
		length = 0
		signed = False
		order = False
		dtype = 0
	End Sub
	Private arr
	Private shift
	Private amount
	Private length
	Private signed
	Private order
	Private dtype
End Class

Function HexStringToIntArray(ByVal s)
	On Error Resume Next
	ReDim res(Int(Len(s) / 4) - 1)
	Dim i : For i = 0 To UBound(res)
		res(i) = CLng("&H" & Mid(s, i * 4 + 1, 4))
	Next
	HexStringToIntArray = res
End Function

Function ByteToHex(b, lo)
	If lo = True Then
		ByteToHex = Right(Hex(b), 1)
	Else
		ByteToHex = Left(Right("00" & Hex(b), 2), 1)
	End If
End Function

Sub InformAboutParameterError(Name, Section)
	MsgBox "�������� " & Name & " � ������ " & Section _
			& " ����������� ��� ����� �������� ��������." & vbCrLf & vbCrLf _
			& "��������� ����� �������.", vbOKOnly, "������"
End Sub

Function ProcessFormula(formula, value)
	If formula <> "" Then
		ProcessFormula = Eval(formula)
	Else
		ProcessFormula = value
	End If
End Function
