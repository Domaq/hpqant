Public UserLamps

Sub LoadLamps
	Form.LoadUserLamps UserLamps.GetSize
	Dim i : For i = 0 To UserLamps.GetSize - 1
		Form.lblUserLamp(i).Title = UserLamps.GetName(i)
	Next
End Sub

Sub LocateLamps
	Dim l : l = Form.lblUserLamp(0).Left
	Dim t : t = Form.lblGasValue(MaxGases).Top + Form.lblGasValue(MaxGases).Height + 150		'maxgases-1
	Dim w : w = Form.lblUserLamp(0).Width
	Dim h : h = Form.lblUserLamp(0).Height
	Dim i : For i = 0 To UserLamps.GetSize - 1
		Form.lblUserLamp(i).Move l, t, w, h
		Form.lblUserLamp(i).Visible = True
		t = t + h
	Next
End Sub

Sub FireLamp(Name, state)
	Dim index : index = UserLamps.GetIndex(Name)
	If index < 0 Then Exit Sub
	Select Case state
		Case DeviceStateOn
			Form.lblUserLamp(index).LampColor = RGB(0, 192, 0)
			Form.lblUserLamp(index).Value = DeviceStateOn
		Case DeviceStateOff
			Form.lblUserLamp(index).LampColor = RGB(0, 0, 192)
			Form.lblUserLamp(index).Value = DeviceStateOff
		Case DeviceStateBad
			Form.lblUserLamp(index).LampColor = RGB(192, 0, 0)
			Form.lblUserLamp(index).Value = DeviceStateBad
		Case DeviceStateUnknown
			Form.lblUserLamp(index).LampColor = RGB(192, 192, 0)
			Form.lblUserLamp(index).Value = DeviceStateUnknown
	End Select
End Sub

Function LampState(Name)
	Dim index : index = UserLamps.GetIndex(Name)
	If index < 0 Then
		LampState = DeviceStateUnknown
		Exit Function
	End If
	LampState = Form.lblUserLamp(index).Value
End Function
