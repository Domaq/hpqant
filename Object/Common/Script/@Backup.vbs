
Public InSendingParameters : InSendingParameters = False
Public InReceivingParameters : InReceivingParameters = False

Private RealBackupFolders

Sub StoreBackup
	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
	Dim TextDateTime
	TextDateTime = _
			Form.GTime.FormatDate(CurrentDateTime) & _
			" " & _
			Right("0" & Form.GTime.GetHour(CurrentDateTime), 2) & "'" & _
			Right("0" & Form.GTime.GetMinute(CurrentDateTime), 2) & "'" & _
			Right("0" & Form.GTime.GetSecond(CurrentDateTime), 2) & "''" & _
			Right("0" & Form.GTime.GetMilliSecond(CurrentDateTime), 3)
	Dim folder
	folder = MainDir & "Ini.backup"
	If Not fso.FolderExists(folder) Then fso.CreateFolder(folder)
	If serialNumber <> "" Then
		fso.CopyFolder Left(IniDir, Len(IniDir) - 1), folder & "\" & "������ " & serialNumber & " - " & TextDateTime
	Else
		fso.CopyFolder Left(IniDir, Len(IniDir) - 1), folder & "\" & TextDateTime
	End If
	Set fso = Nothing
End Sub

Sub OnChangeBackup
	Backup.cmdApply.Enabled = True
	Backup.cmdApplyFromIni.Enabled = True
End Sub

Sub OnCloseBackupWindow
	Backup.cmdApplyFromIni.Enabled = True
	ReleaseButton "������������"
	EnableAllButtons
End Sub

Sub FillBackupList
	Backup.Visible = True
	Backup.cmdApply.Enabled = False
	Backup.cmdClose.Enabled = True
	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	Dim mainBackupFolderName : mainBackupFolderName = MainDir & "Ini.backup"
	If Not fso.FolderExists(mainBackupFolderName) Then Exit Sub
	Dim mainBackupFolder : Set mainBackupFolder = fso.GetFolder(mainBackupFolderName)
	Dim backupFolders : Set backupFolders = mainBackupFolder.SubFolders
	Backup.lstSaves.Clear
	Set RealBackupFolders = New NamesCollection
	Dim folder : For Each folder In backupFolders
		RealBackupFolders.AddName folder.Name
		Dim backupName : backupName = Replace(folder.Name, "''", ".")
		backupName = Replace(backupName, "'", ":")
		Backup.lstSaves.AddItem backupName
	Next
	Set backupFolders = Nothing
	Set mainBackupFolder = Nothing
	Set fso = Nothing
End Sub

Sub RestoreBackup
	InSendingParameters = True
	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	Dim mainBackupFolderName : mainBackupFolderName = MainDir & "Ini.backup"
	If Not fso.FolderExists(mainBackupFolderName) Then Exit Sub
	Dim mainBackupFolder : Set mainBackupFolder = fso.GetFolder(mainBackupFolderName)
	Dim backupFolders : Set backupFolders = mainBackupFolder.SubFolders
	Dim folder : For Each folder In backupFolders
		If folder.Name = RealBackupFolders.GetName(Backup.lstSaves.ListIndex) Then
			fso.CopyFolder folder.Path, MainDir & "Ini"
			Exit For
		End If
	Next
	Set backupFolders = Nothing
	Set mainBackupFolder = Nothing
	Set fso = Nothing
	Dim ggnames : ggnames = RawData.GetGasGroupNames
	Dim n : For Each n In ggnames
		CalibrationMatrixes.Read n
		Fones.Read n
		CalibrationCoefficients.Read n
		OutputScalesMaximum.Read n
	Next
	SensorsDataMap.Read
	InSendingParameters = False
End Sub

'===============================================
' �������������� �� ������� ��� ����������
Sub RestoreBackupFromIni
	InSendingParameters = True
	Dim fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	Dim mainBackupFolderName : mainBackupFolderName = MainDir & "Ini"
	If Not fso.FolderExists(mainBackupFolderName) Then Exit Sub
	Dim mainBackupFolder : Set mainBackupFolder = fso.GetFolder(mainBackupFolderName)
	Dim backupFolders : Set backupFolders = mainBackupFolder.SubFolders
	Dim folder 
	'fso.CopyFolder folder.Path, mainBackupFolderName
	'MsgBox "folder.Path = " & mainBackupFolderName			'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	Set backupFolders = Nothing
	Set mainBackupFolder = Nothing
	Set fso = Nothing
	Dim ggnames : ggnames = RawData.GetGasGroupNames
	Dim n : For Each n In ggnames
		CalibrationMatrixes.Read n
		Fones.Read n
		CalibrationCoefficients.Read n
		OutputScalesMaximum.Read n
	Next
	SensorsDataMap.Read
	InSendingParameters = False
End Sub

