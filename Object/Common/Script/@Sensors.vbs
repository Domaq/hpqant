Private SensorsMap
Private SensorsFormula

Sub InitializeSensors
	With Form.IniFile
		Set SensorsMap = New CodesCollection
		.Read IniDir & "Sensors.ini"
		.Section = "Sensors"
		If .NumberOfParameters = 0 Then Exit Sub
		Dim parnames : parnames = .AllParameters
		Dim i : For i = 0 To UBound(parnames)
			.Parameter(parnames(i))
			Dim swnames : swnames = .AllSwitches
			Dim j : For j = 0 To UBound(swnames)
				.Switch(swnames(j))
				SensorsMap.AddCode swnames(j), CLng(.SwitchValue)
			Next
		Next
		Set SensorsFormula = New TextsCollection
		SensorsFormula.Name = "SensorsTuning"
		SensorsFormula.ReadTexts
	End With
End Sub

Function GetSensorValue(Name)
	Dim s : For Each s In VCU.Sensors
		If UCase(Name) = UCase(s.Title) Then
			GetSensorValue = s.Value
			Exit Function
		End If
	Next
	GetSensorValue = CDbl(0.0)
End Function
