Class ValvesAssembliesCollection
	Public Sub Reset
		v = Array()
		names = Array()
	End Sub
	Public Sub ConstructAssemblies
		With Form.IniFile
			.Read IniDir & "Valves.ini"
			.Section = "Valves:Assemblies"
			Dim parnames : parnames = .AllParameters
			If Not IsEmpty(parnames) Then
				Dim par : For Each par In parnames
					Dim TempAssembly : Set TempAssembly = New NamesCollection
					.Parameter(par)
					ReDim Preserve names(UBound(names) + 1)
					names(UBound(names)) = .Parameter
					.Switch("Valves")
					Dim items : items = .AllItems
					If Not IsEmpty(items) Then 
						Dim it : For Each it In items
							TempAssembly.AddName it
						Next
					End If
					ReDim Preserve v(UBound(v) + 1)
					Set v(UBound(v)) = TempAssembly
				Next
			End If
		End With
	End Sub
	Public Function GetAssembly(Name)
		Dim i : For i = 0 To UBound(names)
			If UCase(names(i)) = UCase(Name) Then
				GetAssembly = v(i).GetNames
				Exit Function
			End If
		Next
		GetAssembly = Array()
	End Function
	Private v
	Private names
	Private Sub Class_Initialize
		Reset
	End Sub
End Class


Private ValvesMap
Public ValvesAssemblies

Sub InitializeValves
	With Form.IniFile
		.Read IniDir & "Valves.ini"
		.Section = "VCU"
		.Parameter = "Name"
		If Form.LoadVCU(.ParameterValue) = False Then
			InformAboutParameterError "Name", "VCU"
			Form.UnloadRequest = True
			Exit Sub
		End If
		Set ValvesMap = New CodesCollection
		.Section = "Valves:Mapping"
		Dim parnames : parnames = .AllParameters
		Dim par : For Each par In parnames
			.Parameter(par)
			Dim swnames : swnames = .AllSwitches
			Dim sw : For Each sw In swnames
				.Switch(sw)
				Dim code : code = .SwitchValue
				If IsNumeric(code) Then
					If (code >= 1 And code <= 56) Or (code >= 65 And code <= 88) Or (code >= 97 And code <= 120) Then
						code = CLng(code)
						ValvesMap.AddCode UCase(sw), code
					End If
				End If
			Next
		Next
		Set ValvesAssemblies = New ValvesAssembliesCollection
		ValvesAssemblies.ConstructAssemblies
		FireLamp "�������", DeviceStateUnknown
		VCU.rbtManual.Value = True
	End With
	' ���������� ������� ������� �������� ������ � ����� ����
	Dim caption
	For i = 0 To NumberOfValves
		caption = i + 1
		while caption > 32
			caption = caption - 32
		Wend
		Debug.btnValves(i).Enabled = True
		Debug.Valves(i).Title = Format(caption, 2, 0)
	Next
End Sub
