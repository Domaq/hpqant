' ������� ��������
Public MainDir
Public IniDir
Public SaveDir
Public ArchiveDir
'
Public Threads

Sub OnStart
'On Error Resume Next
	MainDir = Form.CurrentDirectory & "\"
	IniDir = Form.CurrentDirectory & "\Ini\"
	SaveDir = Form.CurrentDirectory & "\Save\"
	'-------------------------------
	Dim archDrive
	Dim archPath
	With Form.IniFile
		.Read IniDir & "Output.ini"
		.Section = "ArchivePath"
		'-------
		' ���� ��� ������ ArchivePath, �� ���� �� ��������
		If .NumberOfParameters = 0 Then
	ArchiveDir = Form.CurrentDirectory & "\Archive\"
	
		Else
		'-------
		' ���� ������ ����, �� �������� �� ����� ������ � ��� � ������ ��������� ����
			.Parameter = "Disc"
			If .ParameterValue = "" Then
				MsgBox "�� ������ �������� Disc � ������ ArchivePath. ��������� ����� �������.", vbOKOnly, "������"
				Form.UnloadRequest = True
				Exit Sub
			End If
			archDrive = .ParameterValue

			.Parameter = "Dir"
			If .ParameterValue = "" Then
				MsgBox "�� ������ �������� Dir � ������ ArchivePath. ��������� ����� �������.", vbOKOnly, "������"
				Form.UnloadRequest = True
				Exit Sub
			End If
			archPath = .ParameterValue
			ArchiveDir = archDrive + ":\" + archPath
		End If
		'-------
	End With
	'--------------------------------
	'Form.CurrentDirectory & "\Archive\"
	
	Set Threads = New ThreadsCollection

	OutLog "������ ���������"
	
	' �������������
	' ������� ���������� ��������������� �����
	Dim Initializators
	Initializators = Array( _
			"Parameters", _
			"Analysis", _
			"UserButtons", _
			"UserLamps", _
			"Output", _
			"Calibration", _
			"Exchange", _
			"Spectrometer", _
			"Detectors", _
			"Sensors", _
			"Indicators", _
			"Cyclograms", _
			"Valves", _
			"Interop", _
			"Auto" _
			)
	Dim i : For Each i In Initializators
		SC.Run "Initialize" & i
		If Form.UnloadRequest = True Then
			Exit Sub
		End If
	Next
	
	' ������ ��������
	StartWork
	OutLog "������ ���������"
End Sub

Dim FinishCycleCounter : FinishCycleCounter = 0
Const FinishTimeout = 20

Sub OnFinish
	Cyclograms.StopAllCyclograms
	If FinishCriterion = True Then
		Form.ScriptFinished = True
		OutLog "��������� ���������"
		Exit Sub
	End If
	Cyclograms.StartCyclogram "���������� ������"
	FinishCycleCounter = 0
	SplashExit.lblText.Caption = vbNewLine & "���������� ���������"
	Form.ShowSplashExit True
	Threads.SetCurrentSub "��������", "FinishCycle"
End Sub

Sub FinishCycle
	FinishCycleCounter = FinishCycleCounter + 1
	If FinishCycleCounter > FinishTimeout Then 
		Form.ShowSplashExit False
		Form.ScriptFinished = True
		Exit Sub
	End If
	SplashExit.ProgressBar.Value = 100 / (FinishTimeout + 1) * FinishCycleCounter
	If FinishCriterion = False Then
		Exit Sub
	End If
	Threads.SetCurrentSub "���������� ������", ""
	Form.ShowSplashExit False
	Form.ScriptFinished = True
	OutLog "��������� ���������"
End Sub
