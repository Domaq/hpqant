Private DetectorsMap

Sub InitializeDetectors
	'--
	' ���������� ������� ���������� �������� � ����� ����
	Dim caption
	For i = 0 To NumberOfDetectors - 1
		caption = i + 1
		while caption > 32
			caption = caption - 32
		Wend
		Debug.Detectors(i).Title = Format(caption, 2, 0)
	Next
	'--
	With Form.IniFile
		Set DetectorsMap = New CodesCollection
		.Read IniDir & "Detectors.ini"
		.Section = "Detectors"
		If .NumberOfParameters = 0 Then Exit Sub
		Dim parnames : parnames = .AllParameters
		Dim i : For i = 0 To UBound(parnames)
			.Parameter(parnames(i))
			Dim swnames : swnames = .AllSwitches
			Dim j : For j = 0 To UBound(swnames)
				.Switch(swnames(j))
				DetectorsMap.AddCode swnames(j), CLng(.SwitchValue)
			Next
		Next
	End With
End Sub
