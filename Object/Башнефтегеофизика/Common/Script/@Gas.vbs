Class GasGroup
	Public Sub AddChannel(gas, num)
		ReDim Preserve Gases(UBound(Gases) + 1)
		Gases(UBound(Gases)) = gas
		ReDim Preserve Channels(UBound(Channels) + 1)
		Channels(UBound(Channels)) = CLng(num)
		ReDim Preserve Values(UBound(Values) + 1)
		Values(UBound(Values)) = CLng(0)
	End Sub
	Public Function GetSize
		GetSize = UBound(Gases) + 1
	End Function
	Public Name
	Public Channels
	Public Values
	Public Gases
	Private Sub Class_Initialize
		Name = ""
		Gases = Array()
		Channels = Array()
		Values = Array()
	End Sub
End Class

Class GasGroupCollection
	Public Sub AddGasGroup(ByVal gg)
		ReDim Preserve g(UBound(g) + 1)
		Set g(UBound(g)) = gg
	End Sub
	Public Sub SetValue(chan, Val)
		Dim gg : For Each gg In g
			Dim i : For i = 0 To UBound(gg.Channels)
				If gg.Channels(i) = chan Then
					gg.Values(i) = Val
				End If
			Next
		Next
	End Sub
	Public Sub SetValues(ggname, values)
		Dim gg : For Each gg In g
			If UCase(gg.Name) = UCase(ggname) Then
				Dim i : For i = 0 To UBound(gg.Values)
					gg.Values(i) = values(i)
				Next
				Exit Sub
			End If
		Next
	End Sub
	Public Function GetValues(ggname)
		Dim gg : For Each gg In g
			If UCase(gg.Name) = UCase(ggname) Then
				GetValues = gg.Values
				Exit Function
			End If
		Next
		GetValues = Array()
	End Function
	Public Function GetGasNames(ggname)
		Dim gg : For Each gg In g
			If UCase(gg.Name) = UCase(ggname) Then
				GetGasNames = gg.Gases
				Exit Function
			End If
		Next
		GetGasNames = Array()
	End Function
	Public Function GetChannel(ggname, gas)
		Dim gg : For Each gg In g
			If UCase(gg.Name) = UCase(ggname) Then
				Dim i : For i = 0 To UBound(gg.Gases)
					If UCase(gg.Gases(i)) = UCase(gas) Then
						GetChannel = gg.Channels(i)
						Exit Function
					End If
				Next
			End If
		Next
		GetChannel = -1
	End Function
	Public Function GetGasGroupNameByIndex(index)
		If index >= 0 And index <= UBound(g) Then
			GetGasGroupNameByIndex = g(index).Name
		Else
			GetGasGroupNameByIndex = ""
		End If
	End Function
	Public Function GetGasGroupNames
		Dim names : names = Array()
		Dim gg : For Each gg In g
			ReDim Preserve names(UBound(names) + 1)
			names(UBound(names)) = gg.Name
		Next
		GetGasGroupNames = names
	End Function
	Public Function GetGasGroupIndex(ggname)
		Dim i : For i = 0 To UBound(g)
			If UCase(g(i).Name) = UCase(ggname) Then
				GetGasGroupIndex = i
				Exit Function
			End If
		Next
		GetGasGroupIndex = -1
	End Function
	Public Function GetGasGroupSize(ggname)
		Dim gg : For Each gg In g
			If UCase(gg.Name) = UCase(ggname) Then
				GetGasGroupSize = gg.GetSize
				Exit Function
			End If
		Next
		GetGasGroupSize = 0
	End Function
	Public Function GetSize
		GetSize = UBound(g) + 1
	End Function
	Public Sub ConstructGasGroups
		With Form.IniFile
			.Read IniFileName
			.Section = "Gases:Mapping"
			Form.LoadGraphs .NumberOfParameters
			Dim parnames : parnames = .AllParameters
			Dim par : For Each par In parnames
				Dim tmpGasGroup : Set tmpGasGroup = new GasGroup
				.Parameter = par
				tmpGasGroup.Name = .Parameter
				Dim swnames : swnames = .AllSwitches
				Dim sw : For Each sw In swnames
					.Switch(sw)
					tmpGasGroup.AddChannel sw, .SwitchValue
				Next
				AddGasGroup tmpGasGroup
			Next
		End With
	End Sub
	Public IniFileName
	Private g
	Private Sub Class_Initialize
		IniFileName = ""
		g = Array()
	End Sub
End Class

Class GasAttributes
	Public Name
	Public ReadableName
	Public Colour
	Public Units
	Public MaxWorkLimit
	Public MaxTuneLimit
	Public Format
End Class

Class GasAttributesCollection
	Private Sub AddGasAttributes(ByVal ga)
		ReDim Preserve a(UBound(a) + 1)
		Set a(UBound(a)) = ga
	End Sub
	Public Function GetAllNames
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.Name
		Next
		GetAllNames = arr
	End Function
	Public Function GetReadableName(n)
		Dim ga : For Each ga In a
			If UCase(ga.Name) = UCase(n) Then
				GetReadableName = ga.ReadableName
				Exit Function
			End If
		Next
	End Function
	Public Function GetAllReadableNames
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.ReadableName
		Next
		GetAllReadableNames = arr
	End Function
	Public Function GetColour(n)
		Dim ga : For Each ga In a
			If UCase(ga.Name) = UCase(n) Then
				GetColour = ga.Colour
				Exit Function
			End If
		Next
	End Function
	Public Function GetAllColours
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.Colour
		Next
		GetAllColours = arr
	End Function
	Public Function GetUnits(n)
		Dim ga : For Each ga In a
			If UCase(ga.Name) = UCase(n) Then
				GetUnits = ga.Units
				Exit Function
			End If
		Next
	End Function
	Public Function GetAllUnits
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.Units
		Next
		GetAllUnits = arr
	End Function
	Public Function GetMaxWorkLimit(n)
		Dim ga : For Each ga In a
			If UCase(ga.Name) = UCase(n) Then
				GetMaxWorkLimit = ga.MaxWorkLimit
				Exit Function
			End If
		Next
	End Function
	Public Function GetAllMaxWorkLimits
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.MaxWorkLimit
		Next
		GetAllMaxWorkLimits = arr
	End Function
	Public Function GetMaxTuneLimit(n)
		Dim ga : For Each ga In a
			If UCase(ga.Name) = UCase(n) Then
				GetMaxTuneLimit = ga.MaxTuneLimit
				Exit Function
			End If
		Next
	End Function
	Public Function GetAllMaxTuneLimits
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.MaxTuneLimit
		Next
		GetAllMaxTuneLimits = arr
	End Function
	Public Function GetFormat(n)
		Dim ga : For Each ga In a
			If UCase(ga.Name) = UCase(n) Then
				GetFormat = ga.Format
				Exit Function
			End If
		Next
	End Function
	Public Function GetAllFormats
		Dim arr : arr = Array()
		Dim ga : For Each ga In a
			ReDim Preserve arr(UBound(arr) + 1)
			arr(UBound(arr)) = ga.Format
		Next
		GetAllFormats = arr
	End Function
	Public Function GetSize
		GetSize = UBound(a) + 1
	End Function
	Public Sub ConstructGasAttributes
		Form.IniFile.Read IniFileName
		Form.IniFile.Section = "Gases"
		MaxGases = Form.IniFile.NumberOfParameters
		Form.LoadGasLabels (MaxGases+1) 				'  + 1 ������ ����� ��� ����
		Dim glist : glist = Form.IniFile.AllParameters
		Dim gi : For Each gi In glist
			Dim tmpGasAttributes : Set tmpGasAttributes = New GasAttributes
			Form.IniFile.Parameter = gi
			tmpGasAttributes.Name = Form.IniFile.Parameter
			Form.IniFile.Switch = "Color"
			Dim col : col = Form.Colors(Form.IniFile.SwitchValue)
			tmpGasAttributes.Colour = col
			Form.IniFile.Switch = "Name"
			If Form.IniFile.SwitchValue = "" Then
				tmpGasAttributes.ReadableName = tmpGasAttributes.Name
			Else
				tmpGasAttributes.ReadableName = Form.IniFile.SwitchValue
			End If
			Form.IniFile.Switch = "Units"
			If Form.IniFile.SwitchValue = "" Then
				tmpGasAttributes.Units = "?"
			Else
				tmpGasAttributes.Units = Form.IniFile.SwitchValue
			End If
			Form.IniFile.Switch = "MaxWorkLimit"
			If Form.IniFile.SwitchValue = "" Then
				tmpGasAttributes.MaxWorkLimit = CDbl(100)
			Else
				tmpGasAttributes.MaxWorkLimit = CDbl(Form.IniFile.SwitchValue)
			End If
			Form.IniFile.Switch = "MaxTuneLimit"
			If Form.IniFile.SwitchValue = "" Then
				tmpGasAttributes.MaxTuneLimit = CDbl(100)
			Else
				tmpGasAttributes.MaxTuneLimit = CDbl(Form.IniFile.SwitchValue)
			End If
			Form.IniFile.Switch = "Format"
			If Form.IniFile.SwitchValue = "" Then
				tmpGasAttributes.Format = "%.0f"
			Else
				tmpGasAttributes.Format = Form.IniFile.SwitchValue
			End If
			AddGasAttributes tmpGasAttributes
		Next
	End Sub
	Public IniFileName
	Private a
	Private Sub Class_Initialize
		IniFileName = ""
		a = Array()
	End Sub
End Class


Function GetGasIndex(Name)
	Dim names : names = AttributesOfGases.GetAllNames
	Dim i : For i = 0 To MaxGases - 1
		If UCase(names(i)) = UCase(Name) Then
			GetGasIndex = i
			Exit Function
		End If
	Next
	OutLog "GetGasIndex: ���������� ���������� ������ ��� ���� " & Name
	GetGasIndex = -1
End Function
