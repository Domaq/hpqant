Public UserButtons
Dim maxButtonsInRow : maxButtonsInRow = 10			' ������������ ����� ������ � ����� �������, ���� ������, �� ������ � 2 �������

Sub LoadButtons
	Form.LoadUserButtons UserButtons.GetSize
	Dim i : For i = 0 To UserButtons.GetSize - 1
		Form.chkUserButton(i).Caption = UserButtons.GetName(i)
	Next
End Sub

Sub LocateButtons
	Dim l : l = Form.chkUserButton(0).Left
	Dim t : t = Form.lblGasName(MaxGases).Top + Form.lblGasName(MaxGases).Height + 150		'maxgases-1
	Dim w : w = Form.chkUserButton(0).Width
	Dim h : h = Form.chkUserButton(0).Height
	If UserButtons.GetSize <= maxButtonsInRow Then
		For i = 0 To UserButtons.GetSize - 1
			' ���� ������ �������� ��� ������� �������� ��� ������, ��� ������ 2
			If IsButtonPressed("�������� ���. �������") Or UserButtons.GetName(i) = "�������� ���. �������" Or UserButtons.GetName(i) = "�������" Then		'!!!!
				Form.chkUserButton(i).Move l, t, w, h
				Form.chkUserButton(i).Visible = True
				t = t + h
			End If 
		Next
	Else
	'-----------------
		' ���� ������ ������� �����, �� ������ �� � 2 �������
		For i = 0 To maxButtonsInRow - 1
			If IsButtonPressed("�������� ���. �������") Or UserButtons.GetName(i) = "�������� ���. �������" Or UserButtons.GetName(i) = "�������" Then		'!!!!
				Form.chkUserButton(i).Move l, t, w, h
				Form.chkUserButton(i).Visible = True
				t = t + h
			End If
		Next
		
		t = Form.chkUserButton(maxButtonsInRow - (UserButtons.GetSize - maxButtonsInRow) ).Top
		l = l + Form.chkUserButton(0).Width
		
		For i = 0 To (UserButtons.GetSize - maxButtonsInRow - 1)
			If IsButtonPressed("�������� ���. �������") Then		'!!!!
				Form.chkUserButton(maxButtonsInRow + i).Move l, t, w, h
				Form.chkUserButton(maxButtonsInRow + i).Visible = True
				t = t + h
			End If
		Next
	End If
	'-----------------
End Sub

Sub OnButtonControlMouseLeft
End Sub

Sub OnButtonControlMouseRight
	If Debug.Visible = True Then
		Debug.Visible = False
	Else
		Debug.Visible = True
	End If
End Sub

Sub EnableButton(Name)
	Dim i : i = UserButtons.GetIndex(Name)
	If i < 0 Then Exit Sub
	Form.chkUserButton(i).Enabled = True
End Sub

Sub EnableSelectedButtons(NameTemplate)
	Dim ii : ii = UserButtons.GetIndexes(NameTemplate)
	For Each i In ii
		Form.chkUserButton(i).Enabled = True
	Next
End Sub

Sub EnableAllButtons
	Dim b : For Each b In Form.chkUserButton
		b.Enabled = True
	Next
End Sub

Sub DisableButton(Name)
	Dim i : i = UserButtons.GetIndex(Name)
	If i < 0 Then Exit Sub
	Form.chkUserButton(i).Enabled = False
End Sub


Sub DisableSelectedButtons(NameTemplate)
	Dim ii : ii = UserButtons.GetIndexes(NameTemplate)
	For Each i In ii
		Form.chkUserButton(i).Enabled = False
	Next
End Sub

Sub DisableAllButtons
	Dim b : For Each b In Form.chkUserButton
		b.Enabled = False
	Next
End Sub

Sub PressButton(Name)
	Dim i : i = UserButtons.GetIndex(Name)
	If i < 0 Then Exit Sub
	Form.chkUserButton(i).Value = 1
End Sub

Sub PressSelectedButtons(NameTemplate)
	Dim ii : ii = UserButtons.GetIndexes(NameTemplate)
	For Each i In ii
		Form.chkUserButton(i).Value = 1
	Next
End Sub

Sub ReleaseButton(Name)
	Dim i : i = UserButtons.GetIndex(Name)
	If i < 0 Then Exit Sub
	Form.chkUserButton(i).Value = 0
End Sub

Sub ReleaseSelectedButtons(NameTemplate)
	Dim ii : ii = UserButtons.GetIndexes(NameTemplate)
	For Each i In ii
		Form.chkUserButton(i).Value = 0
	Next
End Sub

'--------
' ������� �� ������
Function IsButtonEnabled(Name)
	Dim i : i = UserButtons.GetIndex(Name)
	If i < 0 Then
		IsButtonEnabled = False
		Exit Function
	End If
	If Form.chkUserButton(i).Enabled = True Then
		IsButtonEnabled = True
	Else
		IsButtonEnabled = False
	End If	
End Function

Function IsButtonPressed(Name)
	Dim i : i = UserButtons.GetIndex(Name)
	If i < 0 Then
		IsButtonPressed = False
		Exit Function
	End If
	If Form.chkUserButton(i).Value = 0 then
		IsButtonPressed = False
	Else
		IsButtonPressed = True
	End If
End Function

Function IsAnySelectedButtonsPressed(NameTemplate)
	Dim ii : ii = UserButtons.GetIndexes(NameTemplate)
	For Each i In ii
		If Form.chkUserButton(i).Value = 1 Then
			IsAnySelectedButtonsPressed = True
			Exit Function
		End If
	Next
	IsAnySelectedButtonsPressed = False
End Function

Function IsAnySelectedButtonsPressed(NameTemplate)
	Dim ii : ii = UserButtons.GetIndexes(NameTemplate)
	For Each i In ii
		If Form.chkUserButton(i).Value = 1 Then
			IsAnySelectedButtonsPressed = True
			Exit Function
		End If
	Next
	IsAnySelectedButtonsPressed = False
End Function