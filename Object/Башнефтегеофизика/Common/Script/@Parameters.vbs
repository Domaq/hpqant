Private Parameters

Sub InitializeParameters
	Set Parameters = New TextsCollection
	Parameters.Name = "Parameters"
	RestoreParameters
End Sub

Sub RestoreParameters
	Parameters.ReadTexts
	Dim pars : pars = Parameters.GetTexts
	Dim p : For Each p In pars
		SC.ExecuteStatement p.name & " = " & p.text
	Next
End Sub

Sub SaveParameters
	Dim pars : pars = Parameters.GetTexts
	Dim i : For i = 0 To UBound(pars)
		Parameters.AddText pars(i).Name, Eval(pars(i).name)
	Next
	Parameters.WriteTexts
End Sub