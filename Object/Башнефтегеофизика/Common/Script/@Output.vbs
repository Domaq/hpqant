Public WorkTimeScale
Public CalibrationTimeScale

Sub InitializeOutput
	Dim l, t, w, h
	With Form.IniFile
		'--
		' ��������� ������ ������(�� ������� ��������� ���)
		Form.lblGasName(0).Height = Fix(Form.lblGasName(0).Height * 4 / 8)
		'--
		' ������ ������ �����
		t = Form.lblGasName(0).Top
		h = Form.lblGasName(0).Height
		Dim i : For i = 0 To MaxGases - 1
			l = Form.lblGasName(0).Left
			Form.lblGasName(i).Move l, t
			Form.lblGasName(i).LampAutoColor = False
			Form.lblGasName(i).Visible = True
			l = Form.lblGasValue(0).Left
			Form.lblGasValue(i).Move l, t
			Form.lblGasValue(i).Visible = True
			l = Form.lblGasUnits(0).Left
			Form.lblGasUnits(i).Move l, t + _
					(Form.lblGasValue(i).Height - Form.lblGasUnits(i).Height)
			Form.lblGasUnits(i).Visible = True
			t = t + h + 11
		'--
		' ��������� ������ ������
		Form.lblGasName(i).Height = h
		Form.lblGasValue(i).Height = h
		Form.lblGasUnits(i).Height = h
		'--
		Next
		'----					!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		' ��������� ������ ������� ��� CxHx
			l = Form.lblGasName(0).Left
			Form.lblGasName(MaxGases).Move l, t
			Form.lblGasName(MaxGases).LampAutoColor = False
			Form.lblGasName(MaxGases).Visible = True
			l = Form.lblGasValue(0).Left
			Form.lblGasValue(MaxGases).Move l, t
			Form.lblGasValue(MaxGases).Visible = True
			l = Form.lblGasUnits(0).Left
			Form.lblGasUnits(MaxGases).Move l, t + _
					(Form.lblGasValue(MaxGases).Height - Form.lblGasUnits(i).Height)
			Form.lblGasUnits(MaxGases).Visible = True
			t = t + h + 11
			' ��������� ������ ������
			Form.lblGasName(MaxGases).Height = h
			Form.lblGasValue(MaxGases).Height = h
			Form.lblGasUnits(MaxGases).Height = h
			Form.lblGasName(MaxGases).Title = "CxHy(�����)"
			
			Form.lblGasValue(MaxGases).Format = "%.4f"
			
			Form.lblGasUnits(MaxGases).Title = "%"
		'----					!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
		' ����������� ������ �����
		Dim rns : rns = AttributesOfGases.GetAllReadableNames
		Dim cs : cs = AttributesOfGases.GetAllColours
		Dim fs : fs = AttributesOfGases.GetAllFormats
		Dim us : us = AttributesOfGases.GetAllUnits
		Dim mas : mas = AttributesOfGases.GetAllMaxWorkLimits
		For i = 0 To MaxGases - 1
			Form.lblGasName(i).Title = rns(i) & units
			Form.lblGasName(i).LampColor = cs(i)
			Form.lblGasValue(i).Format = fs(i)
			Form.lblGasUnits(i).Title = us(i)
		Next
		' �������������� ������
		.Read IniDir & "Output.ini"
		.Section = "Graph"
		.Parameter = "WorkTimeScale"
		WorkTimeScale = .ParameterValue
		If WorkTimeScale = "" Or Not IsNumeric(WorkTimeScale) Then
			WorkTimeScale = CLng(8 * 60 * 60)
		Else
			WorkTimeScale = CLng(WorkTimeScale)
		End If
		.Parameter = "CalibrationTimeScale"
		CalibrationTimeScale = .ParameterValue
		If CalibrationTimeScale = "" Or Not IsNumeric(CalibrationTimeScale) Then
			CalibrationTimeScale = CLng(5 * 60)
		Else
			CalibrationTimeScale = CLng(CalibrationTimeScale)
		End If
		Dim gr : For Each gr In Form.Graphs
			gr.YScalesBounded = False
			gr.YAuto(0) = False
			gr.FieldColour = &H8000000C '&HFFFFFF
			'gr.XGridType = 3 'XGridAbsoluteTime
			'gr.YGridType = 1 'YGridCount
			'gr.YScaleType = 0 'YScaleLinear
			For i = 0 To MaxGases - 1
				gr.FieldMinX(i) = CDate(0)
				gr.FieldMaxX(i) = DateAdd("s", WorkTimeScale, CDate(0))
				gr.FieldMinY(i) = 0
				gr.FieldMaxY(i) = mas(i)
				gr.LineWidth(i) = 2
				gr.LineType(i) = 0
				gr.LineColour(i) = cs(i)
			Next
			gr.PointsVisible = False
		Next
	End With
	PressButton "�������� ���. �������"				' �������� ���������� ��� ������
	LocateButtons
	LocateLamps
	Dim edgeOfLastButton : edgeOfLastButton = _
			Form.chkUserButton(UserButtons.GetSize - 1).Top _
			+ Form.chkUserButton(UserButtons.GetSize - 1).Height
	Dim edgeOfLastLamp : edgeOfLastLamp = _
			Form.lblUserLamp(UserLamps.GetSize - 1).Top _
			+ Form.lblUserLamp(UserLamps.GetSize - 1).Height
	Dim edgeOfLastControl
	If edgeOfLastButton < edgeOfLastLamp Then
		edgeOfLastControl = edgeOfLastLamp
	Else
		edgeOfLastControl = edgeOfLastButton
	End If
	Dim expandSize : expandSize = edgeOfLastControl + 150 - Form.lblDeviceID.Top
	If expandSize < 0 Then expandSize = 0
	If expandSize <> 0 Then
		l = Form.lblDeviceID.Left
		t = Form.lblDeviceID.Top + expandSize
		Form.lblDeviceID.Move l, t
		l = Form.chkHydraulicScheme.Left
		t = Form.chkHydraulicScheme.Top + expandSize
		Form.chkHydraulicScheme.Move l, t
		l = Form.Graphs(0).Left
		t = Form.Graphs(0).Top
		w = Form.Graphs(0).Width
		h = Form.Graphs(0).Height + expandSize
		For Each gr In Form.Graphs
			gr.Move l, t, w, h
		Next
		Form.Height = Form.Height + expandSize
                Form.tbOutput.Move l, t + h + 50, w, 1200
	End If
End Sub

Function Log10(X)
   Log10 = Log(X) / Log(10)
End Function

Function Exp10(X)
   Exp10 = Exp(X * Log(10))
End Function

Function Format(ByVal value, ByVal minbefore, ByVal maxafter)
	If maxafter < 0 Then maxafter = 0
	value = FormatNumber(value, maxafter, , , TristateFalse)
	If minbefore > 0 Then
		Dim nulls
		nulls = String(20, "0")
		Dim length
		length = maxafter + minbefore
		If maxafter <> 0 Then length = length + 1
		If Left(value, 1) = "-" Then
			value = Right(value, Len(value) - 1)
			Format = "-" & Right(nulls & value, length)
		Else
			Format = " " & Right(nulls & value, length)
		End If
	Else
		Format = value
	End If
End Function

Function FormatExponentValue(value)
	Dim result
	result = " " & Format(value, 0, 7)
	If value < 1e-2 And value > 1e-20 Then
		Dim exponent : exponent = - Int(Log10(value))
		Dim mantissa : mantissa = value * 10 ^ exponent
		result = Format(mantissa, 1, 3) & "e-" & Right(Format(exponent, 2, 0), 2)
	End If
	FormatExponentValue = result
End Function

Sub OutGraphValues(Name)
	Dim p : p = Percents.GetValues(Name)
	Dim s
	ReDim s(Percents.GetGasGroupSize(Name))
'	s(0) = CDbl(Date + Time)
	s(0) = Form.GTime.GetCurrentDATE
	Dim i
	For i = 1 To Percents.GetGasGroupSize(Name)
		s(i) = CDbl(p(i - 1))
	Next
	Dim index : index = Percents.GetGasGroupIndex(Name)
	Form.Graphs(index).AddSlice s
End Sub

Sub SetWorkOutputConfiguration(Name)
	Dim fs : fs = AttributesOfGases.GetAllFormats
	Dim us : us = AttributesOfGases.GetAllUnits
	Dim mas : mas = AttributesOfGases.GetAllMaxWorkLimits
	Dim index : index = RawData.GetGasGroupIndex(Name)
	If index < 0 Then index = 0
	Dim j : For j = 0 To RawData.GetGasGroupSize(Name) - 1
		Form.Graphs(index).FieldMinY(j) = 0
		Form.Graphs(index).FieldMaxY(j) = mas(j)
		Form.lblGasValue(j).Format = fs(j)
		Form.lblGasUnits(j).Title = us(j)
	Next
	Form.Graphs(index).FieldMinX(0) = CDate(0)
	Form.Graphs(index).FieldMaxX(0) = DateAdd("s", WorkTimeScale, CDate(0))
	Form.Graphs(index).Refresh
End Sub

Sub SetTuneOutputConfiguration(Name)
	Dim mas : mas = AttributesOfGases.GetAllMaxTuneLimits
	Dim index : index = RawData.GetGasGroupIndex(Name)
	If index < 0 Then index = 0
	Dim j : For j = 0 To RawData.GetGasGroupSize(Name) - 1
		Form.Graphs(index).FieldMinY(j) = 0
		Form.Graphs(index).FieldMaxY(j) = mas(j)
		Form.lblGasValue(j).Format = "%.0f"
		Form.lblGasUnits(j).Title = "mv"
	Next
	Form.Graphs(index).FieldMinX(0) = CDate(0)
	Form.Graphs(index).FieldMaxX(0) = DateAdd("s", CalibrationTimeScale, CDate(0))
	Form.Graphs(index).Refresh
End Sub
'=============================================================================
' ������� � ����������� � �������
Sub OnOutMilligrams(Name)
	'MsgBox "Milligramms"
	Dim index : index = RawData.GetGasGroupIndex(Name)
	If index < 0 Then index = 0
	Dim j : For j = 0 To RawData.GetGasGroupSize(Name) - 1
		If Form.lblGasUnits(j).Title = "ppm" Then	Form.lblGasUnits(j).Title = "mg/m3"		End If
	Next
	Form.Graphs(index).Refresh
End Sub

Sub OnOutPPMs(Name)
	'MsgBox "PPMs"
	Dim index : index = RawData.GetGasGroupIndex(Name)
	If index < 0 Then index = 0
	Dim j : For j = 0 To RawData.GetGasGroupSize(Name) - 1
		If Form.lblGasUnits(j).Title = "mg/m3" Then
			Form.lblGasUnits(j).Title = "ppm"
		End If
	Next
	Form.Graphs(index).Refresh
End Sub

'=============================================================================
' ����� ��������� ���������� � ������� ���� ��������� �� ��������
Sub OutMess(mess)
	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
	Dim dt : dt = "|| " & Form.GTime.FormatDate(CurrentDateTime) & " || " & Form.GTime.FormatTimeExact(CurrentDateTime)
	TextBoxOutput.Text = dt + " |||| " + mess + vbcrlf + TextBoxOutput.Text
End Sub
'======
' ����� ��������� ���������� � ������� ���� ��������� ��� ������� � ��� ���
Sub OutMessNoTime(mess)
	TextBoxOutput.Text = mess + vbcrlf + TextBoxOutput.Text
End Sub
'======
' ����� ��������� � ���-����
Sub OutMessInDDE(mess)
	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
	Dim dt : dt = "|| " & Form.GTime.FormatDate(CurrentDateTime) & " || " & Form.GTime.FormatTimeExact(CurrentDateTime)
	Dim unit
	With Form.IniFile
		.Read IniDir & "Exchange.ini"
		.Section = "Exchange:GBPP"
		.Parameter = "Unit"
		unit = .ParameterValue
		If unit = "" Then
			Form.IpServer.ExportItem("message") = dt + " |||| " + mess
		Else
			Form.IpServer.ExportItem("U" & unit & "_message") = dt + " |||| " + mess	
		End If
	End With
End Sub

'����� ���������� � ���� ��������� � ������ � ���
Sub OutLog(mess)
	OutMess mess
	WriteLog mess
End Sub
'=============================================================================

Sub OutTextValues(Name)
'	If Not IsButtonPressed(Name) And Percents.GetSize > 1 Then Exit Sub
	Dim p : p = Percents.GetValues(Name)
	Dim fs : fs = AttributesOfGases.GetAllFormats
	Dim us : us = AttributesOfGases.GetAllUnits
	Dim i : For i = 0 To Percents.GetGasGroupSize(Name) - 1
		Form.lblGasValue(i).Value = p(i)
	Next
End Sub

Sub ClearTextValues(Name)
	Dim i : For i = 0 To Percents.GetGasGroupSize(Name) - 1
		Form.lblGasValue(i).Value = 0
	Next
End Sub

Sub OnXReperMove(index, value)
	Dim i
	For i = 0 To MaxGases - 1
		Form.lblGasValue(i).Value = Form.Graphs(index).Value(i, value)
	Next
End Sub

Sub OnXReperOn(index)
End Sub

Sub OnXReperOff(index)
	Dim Name : Name = Percents.GetGasGroupNameByIndex(index)
	Dim p : p = Percents.GetValues(Name)
	Dim i : For i = 0 To Percents.GetGasGroupSize(Name) - 1
		Form.lblGasValue(i).Value = p(i)
	Next
End Sub

Function FormatMatrix(ByRef m)
	Dim text : text = ""
	Dim row : For row = 0 To UBound(m, 1)
		Dim column : For column = 0 To UBound(m, 2)
			text = text & Space(3) & Format(m(row, column), 8, 4)
		Next
		text = text & vbNewLine
	Next
	FormatMatrix = text
End Function

Sub OutMatrix(ByRef m)
	Debug.Text.Text = Debug.Text.Text & FormatMatrix(m)
End Sub

Function FormatVector(ByRef v)
	Dim i
	Dim text : text = ""
	For i = 0 To UBound(v)
		text = text & Space(3) & Format(v(i), 7, 4)
	Next
	FormatVector = text
End Function

Sub OutVector(ByRef v)
	Debug.Text.Text = Debug.Text.Text & FormatVector(v)
End Sub
