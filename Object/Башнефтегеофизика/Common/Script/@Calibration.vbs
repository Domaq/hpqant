Class CalibrationMatrixCollection
	Public Sub AddCalibrationMatrix(Name, ByVal cm)
		Dim found : found = False
		Dim i : For i = 0 To UBound(names)
			If UCase(Name) = UCase(names(i)) Then
				found = True
				Exit For
			End If
		Next
		If found = False Then
			ReDim Preserve names(UBound(names) + 1)
			names(UBound(names)) = Name
			ReDim Preserve c(UBound(c) + 1)
			i = UBound(c)
		End If
		c(i) = cm
	End Sub
	Public Function GetCalibrationMatrix(Name)
		Dim i : For i = 0 To UBound(names)
			If UCase(names(i)) = UCase(Name) Then
				ReDim m(MaxGases - 1, MaxGases - 1)
				MatrixToMatrix c(i), m
				GetCalibrationMatrix = m
				Exit Function
			End If
		Next
		ReDim m(MaxGases - 1, MaxGases - 1)
		GetCalibrationMatrix = m
	End Function
	Private Sub Class_Initialize
		c = Array()
		names = Array()
	End Sub
	Private names
	Private c
End Class

Class CalibrationMixture
	Public Sub AddComponent(Name, Val)
		ReDim Preserve Gases(UBound(gases) + 1)
		Gases(UBound(Gases)) = Name
		ReDim Preserve Values(UBound(Values) + 1)
		Values(UBound(Values)) = CDbl(Val)
	End Sub
	Public Function GetValue(n)
		Dim i : For i = 0 To UBound(Gases)
			If UCase(Gases(i)) = UCase(n) Then
				GetValue = Values(i)
				Exit Function
			End If
		Next
		GetValue = CDbl(0)
	End Function
	Public Function GetValues
		Dim v : VectorToVector Values, v
		GetValues = v
	End Function
	Public Function GetGasNames
		Dim v : VectorToVector Gases, v
		GetGasNames = v
	End Function
	Public Name
	Private Gases
	Private Values
	Private Sub Class_Initialize
		Gases = Array()
		Values = Array()
	End Sub
End Class

Class CalibrationMixtureCollection
	Public Sub AddMixture(ByVal mix)
		ReDim Preserve c(UBound(c) + 1)
		Set c(UBound(c)) = mix
	End Sub
	Public Function GetValue(mname, cname)
		Dim m : For Each m In c
			If UCase(m.Name) = UCase(mname) Then
				GetValue = m.GetValue(cname)
				Exit Function
			End If
		Next
		OutLog "������������� ����� "  & mname & " �� �������"
		GetValue = CDbl(0)
	End Function
	Public Function GetValues(mname)
		Dim m : For Each m In c
			If UCase(m.Name) = UCase(mname) Then
				GetValues = m.GetValues
				Exit Function
			End If
		Next
		OutLog "������������� ����� "  & mname & " �� �������"
		ReDim mm(MaxGases - 1)
		GetValues = mm
	End Function
	Public Function GetGasNames(mname)
		Dim m : For Each m In c
			If UCase(m.Name) = UCase(mname) Then
				GetGasNames = m.GetGasNames
				Exit Function
			End If
		Next
		OutLog "������������� ����� "  & mname & " �� �������"
		ReDim mm(MaxGases - 1)
		GetGasNames = mm
	End Function
	Private c
	Private Sub Class_Initialize
		c = Array()
	End Sub
End Class

Sub WriteCalibrationMatrix(Name)

	Dim fso, fl
	Set fso = CreateObject("Scripting.FileSystemObject")

	Dim i, j
	Dim Size
	Dim names
	Dim Str
	Dim ItemFrameSize : ItemFrameSize = 13
	Dim NumOfSpaces : NumOfSpaces = 1

	names = Array("")
	ReDim names(MaxGases)
	Size = UBound(names) - 1
	names(0) = "N/N"
	Dim ns : ns = AttributesOfGases.GetAllNames
	For i = 0 To Size
		names(i + 1) = CStr(ns(i))
	Next

	Dim fname : fname = SaveDir & "CalibrationMatrix_" & Name & ".save"
	Set fl = fso.OpenTextFile(fname, ForWriting, True)
	Str = ""
	For i = 0 To Size + 1
		NumOfSpaces = ItemFrameSize - Len(names(i)) + 1
		Str = Str & names(i) & Space(NumOfSpaces)
	Next
	fl.WriteLine Str

	Dim cm : cm = CalibrationMatrixes.GetCalibrationMatrix(Name)
	For i = 0 To Size
		NumOfSpaces = ItemFrameSize - Len(names(i + 1)) + 1
		Str = names(i + 1) & Space(NumOfSpaces)
		For j = 0 To Size
			Dim Item : Item = Format(cm(i, j), 7, 4)
			NumOfSpaces = ItemFrameSize - Len(Item) + 1
			Str = Str & Item & Space(NumOfSpaces)
		Next
		fl.WriteLine Str
	Next

	fl.Close
	Set fso = Nothing

End Sub

Sub ReadCalibrationMatrix(Name)
	Dim fso : Set fso = CreateObject("Scripting.FileSystemObject")

	Dim fname : fname = SaveDir & "CalibrationMatrix_" & Name & ".save"
	If fso.FileExists(fname) = False Then
		If Name <> "������" And Name <> "�������" Then
			OutLog "���������� ��������� ������������� ������� ��� " & Name
		End If
		Exit Sub
	End If
	Dim fl : Set fl = fso.OpenTextFile(fname, ForReading)

	Dim Str
	Str = fl.ReadLine
	
	'skip N/N element
	Dim pos : pos = InStr(Str, " ")
	Str = Trim( Right(Str, Len(Str) - pos) )
	
	Dim names
	ReDim names(MaxGases - 1)
	Dim i
	For i = 0 To MaxGases - 1
		pos = InStr(Str, " ")
		If pos <> 0 Then
			names(i) = Trim( Left(Str, pos - 1) )
			Str = Trim( Right(Str, Len(Str) - pos) )
		Else
			'�������� ���
			If i <> MaxGases - 1 Then		'����� ����� �������, ����� ���������� ������ ���� �������
				OutLog "������ ������ ����� ���� ����� " & (i + 1) & " � ������� ��� " & Name
				Exit Sub
			End If
			names(i) = Trim(Str)
		End If
	Next

	ReDim cm(MaxGases - 1, MaxGases - 1)
	Dim j
	For i = 0 To MaxGases - 1
		Str = fl.ReadLine
		'skip gas name in row
		pos = InStr(Str, " ")
		Str = Trim( Right(Str, Len(Str) - pos) )
		For j = 0 To MaxGases - 1
			pos = InStr(Str, " ")
			If pos <> 0 Then
				cm(GetGasIndex(names(i)), GetGasIndex(names(j))) = CDbl( Trim( Left(Str, pos - 1) ) )
				Str = Trim( Right(Str, Len(Str) - pos) )
			Else
				'�������� ���
				If j <> MaxGases - 1 Then		'����� ����� ����������, ����� ���������� ������ ���� �������
					OutLog "������ ������ �������� (" & (i + 1) & "," & (j + 1) & ") � ������� ��� "  & Name
					Exit Sub
				End If
				cm(GetGasIndex(names(i)), GetGasIndex(names(j))) = CDbl( Trim( Str ) )
			End If
		Next
	Next
'OutMatrix cm
	CalibrationMatrixes.AddCalibrationMatrix Name, cm
	fl.Close
	Set fso = Nothing
End Sub

Public CalibrationMatrixes
Public CalibrationMixtures
Public Fones
Public CalibrationCoefficients
Public CalibrationTime

Public AveragingCyclesAir			'����� ���������� �� ������� ���� ����� ������������ ������� �� ������� ��� ��� ����� ����
Public AveragingCyclesGas			'����� ���������� �� ������� ���� ����� ������������ ������� ����� ������� ������������� �����

Public InCalibrations

Sub InitializeCalibration
	Set CalibrationMatrixes = New CalibrationMatrixCollection
	Set CalibrationMixtures = New CalibrationMixtureCollection
	Set Fones = New ArraysCollection
	Fones.Name = "Fone"
	Set CalibrationCoefficients = New ArraysCollection
	CalibrationCoefficients.Name = "CalibrationCoefficients"
	Set InCalibrations = New FlagsCollection
	Dim ggnames : ggnames = RawData.GetGasGroupNames
	Dim n : For Each n In ggnames
''		WriteCalibrationMatrix n
		ReadCalibrationMatrix n
''		Fones.WriteArray n
		Fones.ReadArray n
''		CalibrationCoefficients.WriteArray n
		CalibrationCoefficients.ReadArray n
	Next
	' ��������� ���������� � ������������� ������
	With Form.IniFile
		.Read IniDir & "Calibration.ini"
		.Section = "Mixtures"
		If .NumberOfParameters = 0 Then
			MsgBox "�� ������� �� ����� ������������� �����." _
					& " ��������� ����� �������.", vbOKOnly, "������"
		End If
		Dim parnames : parnames = .AllParameters
		Dim par : For Each par In parnames
			Dim TempMixture : Set TempMixture = New CalibrationMixture
			TempMixture.Name = par
			.Parameter(par)
			Dim swnames : swnames = .AllSwitches
			Dim sw : For Each sw In swnames
				.Switch(sw)
				TempMixture.AddComponent sw, .SwitchValue
			Next
			CalibrationMixtures.AddMixture TempMixture
		Next
		.Section = "Parameters"
		.Parameter = "CalibrationTime"
		If .ParameterValue = "" Then
			CalibrationTime = CLng(30)
		Else
			CalibrationTime = CLng(.ParameterValue)
		End If
		.Parameter = "CalibrationTime"
		If .ParameterValue = "" Then
			CalibrationTime = CLng(30)
		Else
			CalibrationTime = CLng(.ParameterValue)
		End If
		.Parameter = "AveragingCyclesAir"
		If .ParameterValue = "" Then
			AveragingCyclesAir = CLng(1)
		Else
			AveragingCyclesAir = CLng(.ParameterValue)
		End If
		.Parameter = "AveragingCyclesGas"
		If .ParameterValue = "" Then
			AveragingCyclesGas = CLng(1)
		Else
			AveragingCyclesGas = CLng(.ParameterValue)
		End If
		'OutLog AveragingCyclesAir & " " & AveragingCyclesGas
	End With
End Sub
