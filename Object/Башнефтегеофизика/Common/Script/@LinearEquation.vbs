' ������ ������ - ������, ������ - �������

' �������� ����� �������
Sub VectorToVector(A, ByRef B)
	If IsArray(B) Then
		ReDim Preserve B(UBound(A))
	Else
		ReDim B(UBound(A))
	End If
	Dim i
	For i = 0 To UBound(A)
		B(i) = A(i)
	Next
End Sub

' �������� ����� �������
Sub MatrixToMatrix(A, ByRef B)
	If IsArray(B) Then
		If UBound(A, 1) > UBound(B, 1) Or UBound(A, 2) > UBound(B, 2) Then
			ReDim B(UBound(A, 1), UBound(A, 2))
		End If
	Else
		ReDim B(UBound(A, 1), UBound(A, 2))
	End If
	Dim row, column
	For row = 0 To UBound(A, 1)
		For column = 0 To UBound(A, 2)
			B(row, column) = A(row, column)
		Next
	Next
End Sub

' �������� ��������
Sub AddVectorToVector(A, B, ByRef X)
	Dim i
	If IsArray(X) Then
		ReDim Preserve X(UBound(A))
	Else
		ReDim X(UBound(A))
	End If
	For i = 0 To UBound(A)
		X(i) = A(i) + B(i)
	Next
End Sub

' ��������� ��������
Sub SubtractVectorFromVector(A, B, ByRef X)
	Dim i
	If IsArray(X) Then
		ReDim Preserve X(UBound(A))
	Else
		ReDim X(UBound(A))
	End If
	For i = 0 To UBound(A)
		X(i) = A(i) - B(i)
	Next
End Sub

' ��������� ��������
Sub MultiplyVectorOnVector(A, B, ByRef X)
	Dim i
	If IsArray(X) Then
		ReDim Preserve X(UBound(A))
	Else
		ReDim X(UBound(A))
	End If
	For i = 0 To UBound(A)
		X(i) = A(i) * B(i)
	Next
End Sub

' ������� ��������
Function DivideVectorOnVector(A, B, ByRef X)
	DivideVectorOnVector = True
	Dim i
	If IsArray(X) Then
		ReDim Preserve X(UBound(A))
	Else
		ReDim X(UBound(A))
	End If
	For i = 0 To UBound(A)
		If B(i) = 0 Then
			OutLog "������� �� ���� � ������� DevideVectorOnVector, ������ = " & ( i + 1)
			B(i) = 1
			DivideVectorOnVector = False
		End If
		X(i) = A(i) / B(i)
	Next
End Function

' ��������� ������� �� �����
Sub MultiplyVectorOnNumber(A, B, ByRef X)
	Dim i
	If IsArray(X) Then
		ReDim Preserve X(UBound(A))
	Else
		ReDim X(UBound(A))
	End If
	For i = 0 To UBound(A)
		X(i) = A(i) * B
	Next
End Sub

' �������� ������������� �������� � �������
Sub RemoveNegativeValuesFromVector(ByRef A)
	If Not IsArray(A) Then Exit Sub
	Dim i
	For i = 0 To UBound(A)
		If A(i) < 0.0 Then
			A(i) = 0.0
		End If
	Next
End Sub

' �������� �� ������� �������� � �������
Function TestZeroValuesInVector(ByRef A)
	If Not IsArray(A) Then
		TestZeroValuesInVector = True
		Exit Function
	End If
	Dim i
	For i = 0 To UBound(A)
		If A(i) = 0.0 Then
			TestZeroValuesInVector = True
			Exit Function
		End If
	Next
	TestZeroValuesInVector = False
End Function

' ��������� ������� �� ������
Sub MultiplyMatrixOnVector(A, B, ByRef X)
	Dim row, column
	For row = 0 To UBound(A, 1)
		X(row) = 0
		For column = 0 To UBound(A, 2)
			X(row) = X(row) + A(row, column) * B(column)
		Next
	Next
End Sub

' ��������� ������� �� �������
Sub MultiplyMatrixOnMatrix(A, B, ByRef X)
	ReDim X(UBound(A, 1), UBound(A, 2))
	Dim row, column, i
	For row = 0 To UBound(A, 1)
		For column = 0 To UBound(A, 2)
			X(row, column) = 0.0
			For i = 0 To UBound(A, 1)
				X(row, column) = X(row, column) + A(row, i) * B(i, column)
			Next
		Next
	Next
End Sub

' ���������� ������������ �������
Function Determinant(A)
	Dim rows : rows = UBound(A, 1)
	Dim T
	ReDim T(rows, rows + 1)
	MatrixToMatrix A, T
	Determinant = 1

	Dim iOK : iOK = 0
	Dim tmp
	Dim r, i, j
	For r = 0 To rows
		If T(r, r) = 0 Then
			For i = r + 1 To rows
				If T(i, r) <> 0 Then
					iOK = i
					Exit For
				Else
					If iOK = 0 Then
						Determinant = 0
						Exit Function
					End If
				End If
			Next
			For j = r To rows
				tmp = T(iOK, j)
				T(iOK, j) = T(r, j)
				T(r, j) = tmp
			Next
			Determinant = -Determinant
		End If
		For i = r + 1 To rows
			If T(r, r) = 0 Then
				OutMatrix T
			End If
			tmp = T(i, r) / T(r, r)
			For j = r To rows
				T(i, j) = T(i, j) - T(r, j) * tmp
			Next
		Next
		Determinant = Determinant * T(r, r)
	Next
End Function

' �������� ��������� �������
Sub MakeIdentityMatrix(ByRef A, n)
	ReDim A(n - 1, n - 1)
	Dim row, column
	For row = 0 To UBound(A, 1)
		For column = 0 To UBound(A, 2)
			If row = column Then
				A(row, column) = CDbl(1.0)
			Else
				A(row, column) = CDbl(0.0)
			End If
		Next
	Next
End Sub

' ������� ������� �������
Sub Cramer(A, B, ByRef X)
	' x(i) = delta(i) / delta(A), ��� delta(i) - ������������ �������, ����������
	' �� �������� ���� ��������� i-�� ������� �������� ��������� ������

	' ���������� ����� � ��������, ��� ������� ����� ������� ����������
	Dim n : n = UBound(A, 1)
	' ��������� ������������ �������
	Dim det : det = Determinant(A)
	ReDim X(UBound(A))
	If det = 0 Then
		OutLog "������������ ������� ����� ����"
		Exit Sub
	End If
	Dim col
	For col = 0 To n
		Dim T
		MatrixToMatrix A, T
		' �������� ������� � �������� col �� ������� ��������� ������
		Dim row
		For row = 0 To n
			T(row, col) = B(row)
		Next
		Dim delta : delta = Determinant(T)
		X(col) = delta / det
	Next
End Sub

' ���������� �������� ������� ������� ������-�������
Sub GaussJordan(A, ByRef U)
	Dim B
	MatrixToMatrix A, B
	MakeIdentityMatrix U, UBound(B, 1) + 1
	Dim n : n = UBound(B, 1)
	Dim i, j, q
	' ������ ���
	For q = 0 To n
		' ����� �� ������� ������� ������������ ������� ������
		For j = 0 To n
			U(q, j) = U(q, j) / B(q, q)
		Next
		For j = q + 1 To n
			B(q, j) = B(q, j) / B(q, q)
		Next
		B(q, q) = 1.0
		' ��������� ������������ ��������� �����, ������� ���� �������
		For i = q + 1 To n
			For j = 0 To n
				U(i, j) = U(i, j) - B(i, q) * U(q, j)
			Next
			For j = q + 1 To n
				B(i, j) = B(i, j) - B(i, q) * B(q, j)
			Next
			B(i, q) = 0.0
		Next
	Next
	' �������� ���
	For q = n To 0 Step -1
		For i = q - 1 To 0 Step -1
			For j = 0 To n
				U(i, j) = U(i, j) - B(i, q) * U(q, j)
			Next
			For j = q + 1 To n
				B(i, j) = B(i, j) - B(i, q) * B(q, j)
			Next
			B(i, q) = 0.0
		Next
	Next
'debug.text.text = ""
'OutMatrix U
'
'debug.text.text = debug.text.text & vbNewLine & vbNewLine
'OutMatrix A
'dim res
'MultiplyMatrixOnMatrix A, U, res
'OutMatrix res
End Sub
