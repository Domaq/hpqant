Private ArchiveFileName : ArchiveFileName = ""

Sub WriteArchive(Name)

	Dim p : p = Percents.GetValues(Name)

	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
	ReDim a(19)
	Dim i
	For i = 2 To 19
		a(i) = CSng(0)
	Next
	a(0) = CSng((Form.GTime.GetYear(CurrentDateTime) - 2000) * 10000.0 _
		+ Form.GTime.GetMonth(CurrentDateTime) * 100.0 _
		+ Form.GTime.GetDay(CurrentDateTime))
'    a(1) = CSng(Form.GTime.GetHour(CurrentDateTime) _
'            + Form.GTime.GetMinute(CurrentDateTime) / 100.0 _
'            + Form.GTime.GetSecond(CurrentDateTime) / 10000.0)
	a(1) = CSng((Form.GTime.GetHour(CurrentDateTime) * 10000.0 _
		+ Form.GTime.GetMinute(CurrentDateTime) * 100.0 _
		+ Form.GTime.GetSecond(CurrentDateTime)) / 10000.0)
	a(2) = CSng(p(GetGasIndex("CH4")))
	a(3) = CSng(p(GetGasIndex("C2H6")))
	a(4) = CSng(p(GetGasIndex("C3H8")))
	a(5) = CSng(p(GetGasIndex("C4H10")))
	a(6) = CSng(p(GetGasIndex("C5H12")))
	a(7) = CSng(p(GetGasIndex("C6H14")))
	a(8) = CSng(0) ' H2O
	a(9) = CSng(p(GetGasIndex("CO2")))
	a(10) = CSng(p(GetGasIndex("O2")))
	a(11) = CSng(0) ' N2
	a(13) = CSng(p(GetGasIndex("Ar")))

	Dim fso, fl
	Set fso = CreateObject("Scripting.FileSystemObject")
	Dim text

	If ArchiveFileName = "" Then
		ArchiveFileName = ArchiveDir & _
		Right("0" & Form.GTime.GetMonth(CurrentDateTime), 2) & _
		Right("0" & Form.GTime.GetDay(CurrentDateTime), 2) & _
		Right("0" & Form.GTime.GetHour(CurrentDateTime), 2) & _
		Right("0" & Form.GTime.GetMinute(CurrentDateTime), 2) & _
		".emg"
	End If
	If fso.FileExists(ArchiveFileName) = False Then
		Set fl = fso.OpenTextFile(ArchiveFileName, ForWriting, True)
	Else
		Set fl = fso.OpenTextFile(ArchiveFileName, ForAppending)
	End If
	For i = 0 To 19
		text = Form.SingleRepresentationToString(a(i))
		fl.Write text
	Next
	fl.Close
	Set fl = Nothing

'	Dim fname: fname = ArchiveFileName & ".txt"
'	If fso.FileExists(fname) = False Then
'		Set fl = fso.OpenTextFile(fname, ForWriting, True)
'	Else
'		Set fl = fso.OpenTextFile(fname, ForAppending)
'	End If
'	fl.Write Form.GTime.FormatDate(CurrentDateTime) & " " & Form.GTime.FormatTime(CurrentDateTime) & "   "
'	For i = 1 To 19
'		text = a(i) & " "
'		fl.Write text
'	Next
'	fl.WriteLine ""
'	fl.Close
'	Set fl = Nothing

	Set fso = Nothing
End Sub