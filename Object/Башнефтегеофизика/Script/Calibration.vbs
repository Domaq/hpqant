Private CalibrationCounter
Private CalibrationAccelCounter
Private AveragingCycles					'����� ���������� �� ������� ���� ����� ������������ �������
Private AveragingTime					'����� ���������� � ��������
Private AveragedData					'������ ����� ����������
Private CurrentMixture
Private CalibrationCurrentCyclogram
Private CalibrationRegime: CalibrationRegime = "Wait"   'Wait, CalibFone, CalibMatrix, CalibMain

Sub OnAnyCalibrationStart
	ReleaseButton "���������"
	SetTuneOutputConfiguration "�������"
	InCalibrations.SetFlag "�������", True
	Threads.SetCurrentSub CalibrationCurrentCyclogram, ""
	Cyclograms.StopAllCyclograms
	Cyclograms.StartCyclogram CalibrationCurrentCyclogram
	DisableAllButtons
	If IsAutoCalibration Then
		EnableButton "�������"
	Else
		If CalibrationCurrentCyclogram <> "���������� ������������" Then
			EnableButton CalibrationCurrentCyclogram
		End If
	End If
	JsumSensor = 0
End Sub


Sub OnCalibrationStart
    CalibrationRegime = "Wait"
	CalibrationCurrentCyclogram = "����������"
	OnAnyCalibrationStart
End Sub


Sub OnCalibrationStop
	Cyclograms.StopSelectedCyclograms "����������"
	Cyclograms.StartCyclogram "�������� ���������"
	Threads.SetCurrentSub CalibrationCurrentCyclogram, ""
	EnableAllButtons
	InCalibrations.SetFlag "�������", False
	SetWorkOutputConfiguration "�������"
	ClearTextValues "�������"
	CalibrationRegime = "Wait"
	SetRegime "��������" '��������2
End Sub


Sub OnCalibrationEnd
	If Not IsAutoCalibration Then
		ReleaseSelectedButtons "����������"
		WriteLog "���������� ���������"
	Else
		Cyclograms.StopSelectedCyclograms "����������"
		Threads.SetCurrentSub CalibrationCurrentCyclogram, ""
		InCalibrations.SetFlag "�������", False
		SetWorkOutputConfiguration "�������"
		ClearTextValues "�������"
		CalibrationRegime = "Wait"
		IsAutoCalibration = False
		If NeedReturnToAuto = True Then
			OutLog "���������� ���������, ������������ � �������������� ����� ������"
			SetRegime "������"
			Cyclograms.StartCyclogram "������"
		End If
	End If
End Sub


Sub OnCalibrationAirStart
	OutLog "���������� �� ������� ������"
	CalibrationCurrentCyclogram = "���������� �� �������"
	CurrentMixture = "������"
	SetRegime "������"
	OnAnyCalibrationStart
End Sub


Sub OnWorkGasHiCalibrationStart
	CalibrationCurrentCyclogram = "���������� ������� ��"
	CurrentMixture = "������������� ��� 1"
	SetRegime "�������_��"
	OnAnyCalibrationStart
End Sub

Sub OnDatNullsStart
	CalibrationCurrentCyclogram = "���� ��������"
	OnAnyCalibrationStart
End Sub

Sub OnDatNullsCalibration
	Dim ext : ext = 0
	Dim DDM_corrector:DDM_corrector = -11.0 
	Dim DR_corrector:DR_corrector = -11.0
	OutLog "������� ���� ��������..."
	Dim s : For Each s In VCU.Sensors
		If s.Title = "���" Then
			DDM_corrector =  s.Value
			'OutLog " ������� �������� ��� ��� = " & DDM_corrector
		ElseIf s.Title = "��" Then
			DR_corrector =  s.Value
			'OutLog " ������� �������� ��� �� = " & DR_corrector
		End If
	Next
	'-----------
	' ����� ����� ������������
	If DDM_corrector <> -11.0 And DR_corrector <> -11.0 Then
		With Form.IniFile
			.Read IniDir & "Sensors.ini"
			.Section = "CalibCorrectors"
			.Parameter = "DDM"
			If .ParameterValue = "" Then	' ���� ��� ��������� DDM ����� ���� �� �����
				ext = 1
			End If
			.Parameter = "DR"
			If .ParameterValue = "" Then	' ���� ��� ��������� DR ����� ���� �� �����
				ext = 1
			End If
			If ext = 1 Then					' ���� ���� �� �����, �� ������ ������� ����� � �������
				OutLog "�� ������ �������� 'DDM', ���� 'DR' � ������ 'CalibCorrectors'. ������������� ����� �������� ����������."
			Else							' ����� ����� ������������ � ���
				.Parameter = "DDM"
				.ParameterValue = DDM_corrector	 
				.Write
				OutLog " ����� ��������� ��� ��� = " & .ParameterValue
				.Parameter = "DR"
				.ParameterValue = DR_corrector
				.Write
				OutLog " ����� ��������� ��� �� = " & .ParameterValue
				End If
		End With
	'-----------
	' ��� �������� ������ �� ������������
	Else	
		OutLog "������� ��� � ��  �����������."	
	End If
	ReleaseButton "���� ��������"
End Sub

Sub OnDatNullsStop
	Cyclograms.StopSelectedCyclograms "���� ��������"
	OnCalibrationStop
End Sub

Sub OnWorkGasLowCalibrationStart
	CalibrationCurrentCyclogram = "���������� ������� ��"
	CurrentMixture = "������������� ��� 2"
	SetRegime "�������_��"
	OnAnyCalibrationStart
End Sub


Sub OnFoneHiCalibrationStart
	OutLog "���������� ����(������� ����������������) ������"
	CalibrationCurrentCyclogram = "���������� ��� ��"
	SetRegime "�������_��"
	OnAnyCalibrationStart
End Sub


Sub OnFoneLowCalibrationStart
	OutLog "���������� ����(������ ����������������) ������"
	CalibrationCurrentCyclogram = "���������� ��� ��"
	SetRegime "�������_��"
	OnAnyCalibrationStart
End Sub


Sub OnCalibrationHiVoltStart
	CalibrationCurrentCyclogram = "���������� ����������"	
	DisableAllButtons
	EnableButton "���������� ����������"
	CEM_State = CEMNoCalib
	SetRegime "������"
	OnAnyCalibrationStart
End Sub

Sub OnCalibrationHiVoltStop
	OnCalibrationStop
	'RgaMultOff
End Sub

Sub OnCalibrationHiVoltEnd
	ReleaseButton "���������� ����������"
End Sub

Sub OnCalibrationElectrometerStart
	OutMess "���������� ������������ ��������. ����� ���������� 2 ���. �� ���������� ����������!"	
	CalibrationCurrentCyclogram = "���������� ������������"	
	DisableAllButtons
	CEM_State = CEMNoCalib
	OnAnyCalibrationStart
End Sub

Sub OnCalibrationElectrometerStop
	Elect_Started = False
	Elect_CL_snded = False
	OnCalibrationStop
End Sub

Sub OnCalibrationHiVoltStopCycle
	Cyclograms.ToNextFrame CalibrationCurrentCyclogram
	Threads.SetCurrentSub CalibrationCurrentCyclogram, ""
End Sub

Sub OnCalibrationCEM
	CEM_Ready = True
End Sub

Sub OnCalibrationElectrometer
	Elect_Started = True
	'OutMess "OnCalibrationElectrometerStart: Elect_Started = " & Elect_Started
	If Elect_CL_snded = False Then
		Elect_CL_snded = True
		RGAsendCommand "CL", 0, 100
	End If

End Sub

Sub OnCalcMatrix
    CalibrationRegime = "CalibMatrix"
	OnCalibration
End Sub

Sub OnMainCalibration
    CalibrationRegime = "CalibMain"
	OnCalibration
End Sub

Sub OnFoneCalibration
    CalibrationRegime = "CalibFone"
	OnCalibration
End Sub

Sub OnCalibration
	CalibrationCounter = 0
	CalibrationAccelCounter = 0
	Dim NumberOfGases: NumberOfGases = RawData.GetGasGroupSize(RegimeName)
	
	If CalibrationRegime = "CalibFone" Then
		OutLog "���� ���� ��� ������ " & RegimeName
	ElseIf CalibrationRegime = "CalibMatrix" Then
		OutLog "������ ������������� ������������� ������� ��� ������ " & RegimeName
	ElseIf CalibrationRegime = "CalibMain" Then
		OutLog "��������� " & CurrentMixture & ". ����������� ������������� ������������ ��� ������ " & RegimeName
	End If
	
	If CalibrationRegime = "CalibMain" And RegimeName <> "������" Then
		AveragingCycles = AveragingCyclesGas
	Else
		AveragingCycles = AveragingCyclesAir
	End If
	
	AveragingTime = AveragingCycles * NumberOfGases
	OutLog("����� ����������: " & AveragingCycles)
	
	Redim AveragedData(MaxGases - 1)
	Dim i: For i = 0 To MaxGases - 1
		AveragedData(i) = 0
	Next
	
	Threads.SetCurrentSub CalibrationCurrentCyclogram, "CalibrationCycle"
End Sub

Sub CalibrationCycle
	If IsAveragingDataInProcess = True Then
		Exit Sub
	End If
	
	If CalibrationRegime = "CalibFone" Then
		CalibrationFone
	ElseIf CalibrationRegime = "CalibMatrix" Then
		CalculationMatrix
	ElseIf CalibrationRegime = "CalibMain" Then
		CalibrationMain
	Else
		OutLog "����������� ������������� ����� " & CalibrationRegime & ". ������ �����������."
	End If
	
	Cyclograms.ToNextFrame CalibrationCurrentCyclogram
	Threads.SetCurrentSub CalibrationCurrentCyclogram, ""
End Sub


'������ ������������� sl
Sub CalibrationMain
	Dim DataWithoutFone : Redim DataWithoutFone(UBound(AveragedData))
	Dim Y : Redim Y(UBound(AveragedData))
	If RegimeName = "�������_��" Or RegimeName = "�������_��" Then
		SubtractVectorFromVector AveragedData, Fones.GetArray(RegimeName), DataWithoutFone
		Dim cm : cm = CalibrationMatrixes.GetCalibrationMatrix(RegimeName)
		Dim icm
		GaussJordan cm, icm
		MultiplyMatrixOnVector icm, DataWithoutFone, Y
		'Cramer cm, DataWithoutFone, Y
	ElseIf RegimeName = "������" Then
		DataWithoutFone = AveragedData
		Y = AveragedData
	Else
		OutLog "����������� ����� " & RegimeName
	End If
	
	Dim sl: sl = CalibrationCoefficients.GetArray(RegimeName)
	
	Dim N2component: N2component = CalibrationMixtures.GetValue(CurrentMixture, "N2")
	If N2component = 0 Then
		OutLog "���������� ��������. �� ������ ���������� N2 � ������� ��� ����� " & CurrentMixture
		Exit Sub
	End If
	
	RawDataWithoutFone.SetValues "�������", DataWithoutFone
	ProcessedData.SetValues "�������", Y
	Dump "DataWithoutFone"
	Dump "ProcessedData"
	
	Dim slN2: slN2 = Y(GetGasIndex("N2")) / N2component
	'OutLog "slN2 = " & slN2
	Dim gasnames: gasnames = CalibrationMixtures.GetGasNames(CurrentMixture)
	
	Dim GasIndex: GasIndex = GetGasIndex("CH4")
	If RegimeName = "������" Then
		Dim fone: Redim fone(UBound(Y))
		Dim j: For j = 0 To UBound(Y)
			fone(j) = 0
		Next
		fone(GasIndex) = Y(GasIndex)
		OutLog "��� ��� CH4 ����� " & fone(GasIndex)
		Fones.AddArray RegimeName, fone
		Fones.WriteArray RegimeName
	End If
	
	Dim val, gname
	Dim K: K = 1
	For i = 0 To UBound(gasnames)
		gname = gasnames(i)
		val = CalibrationMixtures.GetValue(CurrentMixture, gname)
		GasIndex = GetGasIndex(gname)
		
		If RegimeName = "�������_��" Then
			If gname = "nC6H14" Then
				K = 1.8
			ElseIf gname = "nC5H12" Then
				K = 1.2
			ElseIf gname = "iC5H12" Then
				K = 1.1
			ElseIf gname = "nC4H10" Then
				K = 1.06
			ElseIf gname = "iC4H10" Then
				K = 1.3
			End If
		End If
		
		If gname = "N2" Then
			sl(GasIndex) = 1
		ElseIf RegimeName = "������"  And  gname = "CH4" Then
			'Do Nothing
		ElseIf RegimeName = "�������_��" And (gname = "O2" Or gname = "Ar" Or gname = "H2S" Or gname = "H2") Then
			'Do Nothing
		ElseIf RegimeName = "�������_��" And (gname = "O2" Or gname = "Ar" Or gname = "H2S" Or gname = "H2") Then
			'Do Nothing
		ElseIf val <> 0 Then
			Dim SlValue: SlValue = (Y(GasIndex) / val) / slN2
			SlValue = K * SlValue
			If SlValue <> 0 Then
				sl(GasIndex) = SlValue
			Else
				'OutLog "������ ����������. �������� sl ��� ���� " & gname & " ����� 0. ����������� �� ����� ���������"
			End If
				
		Else
			OutLog "�������� ���������� " & gname & " � ����� " & CurrentMixture & " ����� 0. ���������� ��������� sl"
		End If
	Next

	OutLog "�������� ������: " & FormatVector(AveragedData)
	OutLog "����������� ������ ��� ����: " & FormatVector(DataWithoutFone)
	OutLog "������������ ������: " & FormatVector(Y)
	OutLog "������������ sl: " & FormatVector(sl)
	
	'If RegimeName = "������" Then
	'	Dim sl2: sl2 = CalibrationCoefficients.GetArray("�������_��")
	'	For i = 0 To RawData.GetGasGroupSize("�������_��") - 1
	'		If i = GetGasIndex("O2") Or i = GetGasIndex("Ar") Then
	'			sl2(i) = sl(i)
	'		End If
	'	Next
	'	CalibrationCoefficients.AddArray "�������_��", sl2
	'	CalibrationCoefficients.WriteArray "�������_��"
	'End If
	
	CalibrationCoefficients.AddArray RegimeName, sl
	CalibrationCoefficients.WriteArray RegimeName
End Sub


'���� ����
Sub CalibrationFone
	If IsAveragingDataInProcess = True Then
		Exit Sub
	End If
	
	If RegimeName = "�������_��" Then
		'----------
		' ������ SL ��� O2,Ar
		Dim sl2: sl2 = CalibrationCoefficients.GetArray("�������_��")
		Dim N2component: N2component = CalibrationMixtures.GetValue("������", "N2")
		If N2component = 0 Then
			OutLog "���������� ��������. �� ������ ���������� N2 � ������� "
			Exit Sub
		End If
		Dim slN2: slN2 = AveragedData(GetGasIndex("N2")) / N2component
		If slN2 = 0 Then
			OutLog "���������� ��������. sl N2 = 0"
			Exit Sub
		End If
		'--
		Dim O2val, ARval
		O2val = CalibrationMixtures.GetValue("������", "O2")
		ARval = CalibrationMixtures.GetValue("������", "Ar")
		If O2val = 0 Or ARval = 0 Then
			OutLog "���������� ��������. � ����� ������ �� ������ ���������� O2 ��� Ar"
			Exit Sub
		End If
		'--------
		'sl ��� O2
		If AveragedData(GetGasIndex("O2")) <> 0 Then
			sl2(GetGasIndex("O2")) =  (AveragedData(GetGasIndex("O2")) / O2val) / slN2
		Else
			OutMess "��������! SL ��� ���� O2 �� ��� ��������, ������� ��������� = 0"
		End If
		'sl ��� Ar
		If AveragedData(GetGasIndex("Ar")) <> 0 Then
			sl2(GetGasIndex("Ar")) =  (AveragedData(GetGasIndex("Ar")) / ARval) / slN2
		Else
			OutMess "��������! SL ��� ���� Ar �� ��� ��������, ������� ��������� = 0"
		End If

		OutMess "====>>>> O2 sl = " & sl2(GetGasIndex("O2")) & " Ar sl = " & sl2(GetGasIndex("Ar"))
		CalibrationCoefficients.AddArray "�������_��", sl2
		CalibrationCoefficients.WriteArray "�������_��"
		'----------
		
		Dim reduceParam
		With Form.IniFile
			.Read IniDir & "Calibration.ini"
			.Section = "Parameters"
			.Parameter = "ReduceParameterHighVoltage"
			reduceParam = CDbl(.ParameterValue)
			OutMess "�������� ���������� = " & reduceParam
		End With
		Dim i: For i = 0 To MaxGases - 1
			If i = GetGasIndex("N2") Or i = GetGasIndex("O2") Or i = GetGasIndex("CO2") Or i = GetGasIndex("Ar") Or i = GetGasIndex("He") Then
				' ----------------
				'������ ������������ ������� ����������������
				If i = GetGasIndex("Ar") Then
					With Form.IniFile
						.Read IniDir & "Spectrometer.ini"
						.Section = "Mult"
						.Parameter = "ARnormal"
						OutMess "Ar ������� = " & AveragedData(i) & " Ar �������: ARnormal = " & .ParameterValue
						KSenseFall = 100.0 - ((.ParameterValue - AveragedData(i)) / .ParameterValue) 
						OutMess "����� ����������� ������� ����������������: KSenseFall = " & KSenseFall
						.Parameter = "KSenseFall"
						.ParameterValue = KSenseFall
						.Write
					End With
					If KSenseFall < 90 Or KSenseFall > 110 Then
						FireLamp "����������", DeviceStateBad
						OutMess "��������! ������� ���������������� ��������� ���������� ����������, ��������� ����������"
					Else
						FireLamp "����������", DeviceStateGood
					End If
				End If
				' ---------------- 
				AveragedData(i) = 0
			Else
				' ���������� �������� ��-�� ��������� ������������
				
				AveragedData(i) = AveragedData(i) * reduceParam
			End If
		Next
	ElseIf RegimeName <> "�������_��" Then
		OutLog "����������� �����  ������ ���� " & RegimeName
	End If

	OutLog "��� " & RegimeName & FormatVector(AveragedData)
	
	Fones.AddArray RegimeName, AveragedData
	Fones.WriteArray RegimeName
End Sub


'������ �������
Sub CalculationMatrix22
	If IsAveragingDataInProcess = True Then
		Exit Sub
	End If
	
	'������ ��� ������ ������ ��������������!
	If RegimeName <> "�������_��" Then
		OutLog "������ ������������� ������� �������� ������ ��� ������ ������ ����������������"
		Exit Sub
	End If
	
	Dim DataWithoutFone
	SubtractVectorFromVector AveragedData, Fones.GetArray(RegimeName), DataWithoutFone
	
	Dim cm : cm = CalibrationMatrixes.GetCalibrationMatrix(RegimeName)
	
	Dim iN2: iN2 = GetGasIndex("N2")
	Dim iCH4: iCH4 = GetGasIndex("CH4")
	Dim iC2H6: iC2H6 = GetGasIndex("C2H6")
	Dim iO2: iO2 = GetGasIndex("O2")
	Dim iH2S: iH2S = GetGasIndex("H2S")
	Dim iAr: iAr = GetGasIndex("Ar")
	Dim iCO2: iCO2 = GetGasIndex("CO2")
	Dim i_iC4H10: i_iC4H10 = GetGasIndex("iC4H10")
	
	cm(iCH4, iN2) = SafeDiv(DataWithoutFone(iCH4), DataWithoutFone(iN2))
	cm(iC2H6, iN2) = SafeDiv(DataWithoutFone(iC2H6), DataWithoutFone(iN2))
	cm(iH2S, iO2) = SafeDiv(DataWithoutFone(iH2S), DataWithoutFone(iO2))
	cm(iC3H8, iAr) = SafeDiv(DataWithoutFone(iC3H8), DataWithoutFone(iAr))
	'cm(i_iC4H10, iAr) = SafeDiv(DataWithoutFone(i_iC4H10), DataWithoutFone(iAr))
	
	OutLog "�������� ������: " & FormatVector(AveragedData)
	OutLog "����������� ������ ��� ����: " & FormatVector(DataWithoutFone)
	OutLog "������������ ������� ���������: N2_CH4 =  " & cm(iCH4, iN2) & ", N2_C2H6 =  " & cm(iC2H6, iN2) & ", O2_H2S =  " & cm(iH2S, iO2) & ", Ar_C3H8 =  " & cm(iC3H8, iAr)
	
	CalibrationMatrixes.AddCalibrationMatrix RegimeName, cm
	WriteCalibrationMatrix RegimeName
	
	Dim sl: sl = CalibrationCoefficients.GetArray(RegimeName)
	Dim N2component: N2component = CalibrationMixtures.GetValue("������", "N2")
	Dim slN2: slN2 = DataWithoutFone(iN2) / N2component
	sl(iAr) = (DataWithoutFone(iAr) / CalibrationMixtures.GetValue("������", "Ar")) / slN2
	sl(iO2) = (DataWithoutFone(iO2) / CalibrationMixtures.GetValue("������", "O2")) / slN2
	sl(iCO2) = (DataWithoutFone(iCO2) / CalibrationMixtures.GetValue("������", "CO2")) / slN2
	sl(iN2) = 1
	CalibrationCoefficients.AddArray RegimeName, sl
	CalibrationCoefficients.WriteArray RegimeName
End Sub

Sub CalculationMatrix
	If IsAveragingDataInProcess = True Then
		Exit Sub
	End If
	
	'������ ��� ������ ������ ��������������!
	If RegimeName <> "�������_��" Then
		OutLog "������ ������������� ������� �������� ������ ��� ������ ������ ����������������"
		Exit Sub
	End If
	
	
	Dim cm : cm = CalibrationMatrixes.GetCalibrationMatrix(RegimeName)
	
	Dim iN2: iN2 = GetGasIndex("N2")
	Dim iCH4: iCH4 = GetGasIndex("CH4")
	Dim iC2H6: iC2H6 = GetGasIndex("C2H6")
	Dim iO2: iO2 = GetGasIndex("O2")
	Dim iH2S: iH2S = GetGasIndex("H2S")
	Dim iAr: iAr = GetGasIndex("Ar")
	Dim iCO2: iCO2 = GetGasIndex("CO2")
	Dim iC3H8: iC3H8 = GetGasIndex("C3H8")
	Dim i_iC4H10: i_iC4H10 = GetGasIndex("iC4H10")
	
	cm(iCH4, iN2) = SafeDiv(AveragedData(iCH4), AveragedData(iN2))
	cm(iC2H6, iN2) = SafeDiv(AveragedData(iC2H6), AveragedData(iN2))
	cm(iH2S, iO2) = SafeDiv(AveragedData(iH2S), AveragedData(iO2))
	cm(iC3H8, iAr) = SafeDiv(AveragedData(iC3H8), AveragedData(iAr))
	'cm(i_iC4H10, iAr) = SafeDiv(AveragedData(i_iC4H10), AveragedData(iAr))
	
	OutLog "�������� ������: " & FormatVector(AveragedData)
	'OutLog "����������� ������ ��� ����: " & FormatVector(DataWithoutFone)
	OutLog "������������ ������� ���������: N2_CH4 =  " & cm(iCH4, iN2) & ", N2_C2H6 =  " & cm(iC2H6, iN2) & ", O2_H2S =  " & cm(iH2S, iO2) & ", Ar_C3H8 =  " & cm(iC3H8, iAr)
	
	CalibrationMatrixes.AddCalibrationMatrix RegimeName, cm
	WriteCalibrationMatrix RegimeName
	
	
	Dim sl: sl = CalibrationCoefficients.GetArray(RegimeName)
	Dim N2component: N2component = CalibrationMixtures.GetValue("������", "N2")
	Dim slN2: slN2 = AveragedData(iN2) / N2component
	sl(iAr) = (AveragedData(iAr) / CalibrationMixtures.GetValue("������", "Ar")) / slN2
	sl(iO2) = (AveragedData(iO2) / CalibrationMixtures.GetValue("������", "O2")) / slN2
	sl(iCO2) = (AveragedData(iCO2) / CalibrationMixtures.GetValue("������", "CO2")) / slN2
	sl(iN2) = 1
	CalibrationCoefficients.AddArray RegimeName, sl
	CalibrationCoefficients.WriteArray RegimeName
	
	Dim i: For i = 0 To MaxGases - 1
		If i = GetGasIndex("H2") Or i = GetGasIndex("He") Or i = GetGasIndex("iC4H10") Or i = GetGasIndex("nC4H10") Or i = GetGasIndex("iC5H12") Or i = GetGasIndex("nC5H12") Or i = GetGasIndex("nC6H14") Then
			AveragedData(i) = AveragedData(i)
		Else
			AveragedData(i) = 0
		End If
	Next
	OutLog "��� " & RegimeName & FormatVector(AveragedData)
	
	Fones.AddArray RegimeName, AveragedData
	Fones.WriteArray RegimeName
End Sub

Function SafeDiv(a, b)
	If b = 0 Then
		OutLog "������������� ������� �� 0, �� ��������� ����� ���� �� ����������"
		SafeDiv = a
	Else
		SafeDiv = a / b
	End If
End Function


Function IsAveragingDataInProcess2
	Dump "RawData"
	
	'OutLog CalibrationCounter & " " & AveragingTime
	If CalibrationCounter >= AveragingTime Then 
		IsAveragingDataInProcess = False
		Exit Function
	End If
	
	Dim NumberOfGases: NumberOfGases = RawData.GetGasGroupSize(RegimeName)
	Dim CurrentData: CurrentData = RawData.GetValues("�������")
	
	'������ RawData ��� ���� ������� � RawData("�������")
	'AveragedData array has size for Karotazh, CurrentData has sizeof currrent regime
	Dim CurGasIndex: CurGasIndex = CalibrationCounter Mod NumberOfGases		'Gas index for current regime (CalibrationCounter % NumberOfGases)
	Dim gasnames: gasnames = RawData.GetGasNames(RegimeName)				'Gas names for current regime
	Dim AvGasIndex: AvGasIndex = GetGasIndex(gasnames(CurGasIndex))			'Gas index for korotazh regime
	
	If RegimeName = "�������_��" Or RegimeName = "�������_��" Then
		AveragedData(AvGasIndex) = AveragedData(AvGasIndex) + CurrentData(AvGasIndex) / AveragingCycles
	Else
		AveragedData(AvGasIndex) = AveragedData(AvGasIndex) + (CurrentData(AvGasIndex) / (DDMSensor + 1)) / AveragingCycles
	End If
	
	CalibrationCounter = CalibrationCounter + 1
	'OutMess CStr(CalibrationCounter)
	If CalibrationCounter < AveragingTime Then 
		IsAveragingDataInProcess = True
	Else
		IsAveragingDataInProcess = False
	End If
End Function

Function IsAveragingDataInProcess
	Dump "RawData"

	'OutLog CalibrationCounter & " " & AveragingTime
	If CalibrationCounter >= AveragingTime Then 
		IsAveragingDataInProcess = False
		Exit Function
	End If
	If CalibrationAccelCounter >= AveragingCycles Then
		IsAveragingDataInProcess = False
		Exit Function
	End If
	
	CalibrationCounter = CalibrationCounter + 1

	
	Dim CurrentData: CurrentData = RawData.GetValues("�������")
	
	If onCalculateFlag Then
		onCalculateFlag = False
		
		CalibrationAccelCounter = CalibrationAccelCounter + 1
		'OutMess "InAveraging"
		If RegimeName = "�������_��" Then
			For i = 0 To MaxGases - 1
				AveragedData(i) = AveragedData(i) + CurrentData(i) / AveragingCycles
			Next
		Else
			For i = 0 To MaxGases - 1
				AveragedData(i) = AveragedData(i) + (CurrentData(i) / (DDMSensor + 1)) / AveragingCycles
			Next
		End If
	End If
	

	If CalibrationCounter < AveragingTime Then 
		IsAveragingDataInProcess = True
	Else
		IsAveragingDataInProcess = False
	End If
End Function
