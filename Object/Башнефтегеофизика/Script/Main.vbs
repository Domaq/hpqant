'Public WaitCycls : WaitCycls = 2		' ������� ���������� ��������� ������
'Const RegularWaitCycls = 2				' ������� ���������� ��������� ������
'Const FLWaitCycls = 2					' ���������� ��������� ������ ��� ������� FL
Public DataTransfer1 : DataTransfer1 = True

Sub StartWork
'	PressButton "���� �"
	If AutoStartRequired Then
		PressButton "�������"
	Else
		Cyclograms.StartCyclogram "�������� ���������"
	End If
	ReleaseButton "�������� ���. �������"				'�������� ������ ������
	' ������� ������ ������� � ���
	Dim v: For Each v In VCU.Valves
		If UCase(v.Title & v.TitleExt) = "�������" Then
		v.Visible = False	
		End If
	Next
End Sub

Dim ControlCounter : ControlCounter = CLng(1)
Dim CyclogramCounter : CyclogramCounter = CLng(1)
Dim ExchangeCounter : ExchangeCounter = CLng(1)

Sub InstantSub

	If isTestWithoutModbus = False Then
		rc = Form.GModBus1.modbusMastersUpdateConections
		If rc = -10 Then
			FireLamp "�������", DeviceStateBad
		End If
	End If
	'OutMess CStr(rc)
	' ������� ����� �������� !!!
	'OutLog "**** InstantSub ControlCounter = " & ControlCounter	'!!!!!!!
	
	
	If IsButtonPressed("���������") Then
		If SymulateNextLine = True Then
			Calculate
		Else
			'OutLog "��������� ������ ���������"
			'fl.Close
			'Set fso = Nothing
			'ReleaseSelectedButtons "���������"
			'RegimeName = "��������"
		End If
		Exit Sub
	End If
	
	

	
	ControlCounter = ControlCounter + CLng(1)
	
	'OutLog "WaitCycls = " & WaitCycls & " ControlCounter = " & ControlCounter		'!!!!!
	If ControlCounter > CLng(3) Then
		
		If DEMpacketReceived = True Or Cyclograms.IsCyclogramStarted("���������� ������") = True Then
			ControlCounter = CLng(0)
			If isTestWithoutModbus = False Then
				ControlValves
				'ReadFromExt1
			End If
		End If
		Dim cc
		If RGASended Then
			'^^cc = Form.rga1.GetAnswerBA
			'^^ProcessSpectrometer cc
			'^^RGASended = False
		End If
		CheckRGAStatus
		ControlSpectrometer
	End If
	'OutLog "**** InstantSub ExchangeCounter = " & ExchangeCounter	'!!!!!!!
	ExchangeCounter = ExchangeCounter + CLng(1)
	If ExchangeCounter > CLng(3) Then
		ExchangeCounter = CLng(0)
'		WaitCycls = RegularWaitCycls
'		OnDEMaction
'		OnDEMactionRGA
	End If
	CyclogramCounter = CyclogramCounter + CLng(1)
	If IsButtonPressed("���������� ��� ��") Or IsButtonPressed("���������� ��� ��") Or IsButtonPressed("���������� ������� ��") Or IsButtonPressed("���������� ������� ��") Or IsButtonPressed("���������� �� �������") Then
		If CyclogramCounter > CLng(6) Then
			If DEMpacketReceived = True Or Cyclograms.IsCyclogramStarted("���������� ������") = True Then
				CyclogramCounter = CLng(0)
				OnCyclogram
				Threads.Run
			End If
		End If
	Else
		If CyclogramCounter > CLng(15) Then
			If DEMpacketReceived = True Or Cyclograms.IsCyclogramStarted("���������� ������") = True Then
				CyclogramCounter = CLng(0)
				OnCyclogram
				Threads.Run
			End If
		End If
	End If

End Sub

Sub OnFinishPrepare
	ReleaseSelectedButtons ""
End Sub

Sub OnOffHighVoltage
	RGAhighVoltage = 0
	Threads.SetCurrentSub "���������� ������", "OffHighVoltageCycle"
	OffHighVoltageCycle
	debug.text.text = RGAhighVoltage
End Sub

Sub OffHighVoltageCycle
	If LampState("�������") <> DeviceStateOn Then Cyclograms.ToNextFrame "���������� ������"
End Sub

Sub OnOffFilament
	RGAfilament = 0
	Threads.SetCurrentSub "���������� ������", "OffFilamentCycle"
End Sub

Sub OffFilamentCycle
	RGAfilament = 0
	If LampState("�����") <> DeviceStateOn Then Cyclograms.ToNextFrame "���������� ������"
End Sub

Function FinishCriterion
	'If LampState("�������") = DeviceStateOff And LampState("�����") = DeviceStateOff _
'			Or LampState("�����������") <> DeviceStateOn Then
'		FinishCriterion = True
'	Else
'		FinishCriterion = False
'	End If
	If LampState("�����") = DeviceStateOff Or LampState("�����������") <> DeviceStateOn Then
		FinishCriterion = True
	Else
		FinishCriterion = False
	End If
End Function
