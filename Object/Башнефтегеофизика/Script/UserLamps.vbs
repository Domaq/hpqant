Sub InitializeUserLamps
	Set UserLamps = New NamesCollection
	UserLamps.AddName "���"
	UserLamps.AddName "�������"
	UserLamps.AddName "�������"
	UserLamps.AddName "�����"
	UserLamps.AddName "�����������"
	UserLamps.AddName "����������"
	LoadLamps
	FireLamp "���", DeviceStateUnknown
	FireLamp "�������", DeviceStateUnknown
	FireLamp "�������", DeviceStateUnknown
	FireLamp "�����", DeviceStateUnknown
	FireLamp "�����������", DeviceStateUnknown
	FireLamp "����������", DeviceStateUnknown
	'-------------------
	' ��������� ����������� ������ ����������������, ���� ���������� ����� ��� �� 10%, �� ���������� ������� ��������� ����������
	With Form.IniFile
		.Read IniDir & "Spectrometer.ini"
		.Section = "Mult"
		.Parameter = "KSenseFall"
		If .ParameterValue < 90 Or .ParameterValue > 110 Then
			FireLamp "����������", DeviceStateBad
			OutMess "��������! ������� ���������������� ��������� ���������� ����������, ��������� ����������"
		Else
			FireLamp "����������", DeviceStateGood
		End If
	End With
End Sub
