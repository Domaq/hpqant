Sub InitializeUserButtons
	Set UserButtons = New NamesCollection
	UserButtons.AddName "�������"
	UserButtons.AddName "�������� ���. �������"
	'UserButtons.AddName "������������� RGA"
	UserButtons.AddName "���������� ������������"
	UserButtons.AddName "���������� ����������"
	'UserButtons.AddName "���������� �� �������"
	'UserButtons.AddName "���������� ������� ��"
	'UserButtons.AddName "���������� ������� ��"
	UserButtons.AddName "���������� ��� ��"
	UserButtons.AddName "���������� ��� ��"
	UserButtons.AddName "����� ������� ��"
	UserButtons.AddName "����� ������� ��"
	UserButtons.AddName "����� ������"
	UserButtons.AddName "���������"
	UserButtons.AddName "���������"
	UserButtons.AddName "���� ��������"
	UserButtons.AddName "��������� �����"
	UserButtons.AddName "����������"
	LoadButtons
End Sub

Sub OnUserButtonPress(index)
	Dim i
	Dim Name : Name = UserButtons.GetName(index)
	'===============================
	' ������ ������ ������
	If UCase("�������� ���. �������") = UCase(Name) Then
		Dim ii
		For ii = 0 To UserButtons.GetSize - 1
			If UserButtons.GetName(ii) <> "�������� ���. �������" Then
				Form.chkUserButton(ii).Visible = True
			End If
		Next
		LocateButtons
	End If
	'===============================
	
	If CompareStringBegin("����������", Name) Then
		OnValvesManual
		Exit Sub
	End If
	If UCase("������������� RGA") = UCase(Name) Then
		OutLog "������������� RGA. 1 ����. ���������� ������������."
		PressButton "���������� ������������"
		DisableAllButtons
	End If
	If UCase("���� ��������") = UCase(Name) Then
		OutLog "�������� ��������� ����� ��������"
		OnDatNullsStart
	End If
	If UCase("���������") = UCase(Name) Then
		OnSymulationStart
	End If
	If UCase("���������� ������������") = UCase(Name) Then
		OnCalibrationElectrometerStart
	End If
	If UCase("���������� �� �������") = UCase(Name) Then
		OnCalibrationAirStart
	End If
	If UCase("���������� ������� ��") = UCase(Name) Then
		OutLog "���������� �� ����������� ���� � ������ ������� ���������������� ������"
		OnWorkGasHiCalibrationStart
	End If
	If UCase("���������� ������� ��") = UCase(Name) Then
		OutLog "���������� �� ����������� ���� � ������ ������ ���������������� ������"
		OnWorkGasLowCalibrationStart
	End If
	If UCase("���������� ����������") = UCase(Name) Then
		OutLog "���������� ���������� ������"
		OnCalibrationHiVoltStart
	End If
	If UCase("���������� ��� ��") = UCase(Name) Then
		OnFoneHiCalibrationStart
	End If
	If UCase("���������� ��� ��") = UCase(Name) Then
		OnFoneLowCalibrationStart
	End If
	If CompareStringBegin("���������", Name) Then
		SetTuneOutputConfiguration "�������"
	End If
	If CompareStringBegin("�������", Name) Then
		ReleaseButton "���������"
		SetRegime "������"
		OutLog "��������� ����� � �������������� ����� ������"
		OnAutoStart
	End If
	If CompareStringBegin("����� ������", Name) Then
		SetRegime "������"
		DisableAllButtons
		EnableButton "���������"
		EnableButton "����� ������"
		EnableButton "���������"
		Cyclograms.StopAllCyclograms
		Cyclograms.StartCyclogram "������"
	End If
	If CompareStringBegin("����� ������� ��", Name) Then
		SetRegime "�������_��"
		DisableAllButtons
		EnableButton "���������"
		EnableButton "����� ������� ��"
		EnableButton "���������"
		Cyclograms.StopAllCyclograms
		Cyclograms.StartCyclogram "������� ��"
	End If
	If CompareStringBegin("����� ������� ��", Name) Then
		SetRegime "�������_��"
		DisableAllButtons
		EnableButton "���������"
		EnableButton "����� ������� ��"
		EnableButton "���������"
		Cyclograms.StopAllCyclograms
		Cyclograms.StartCyclogram "������� ��"
	End If
End Sub

Sub OnUserButtonRelease(index)

	'Dim dteWait : dteWait = DateAdd("s", 1, Now())
	'Do Until (Now() > dteWait)
	'Loop

	If CompareStringBegin("����������", Name) Then
		OnValvesAuto
		Exit Sub
	End If
	Dim i
	Dim Name : Name = UserButtons.GetName(index)
	If CompareStringBegin("���� ��������", Name) Then
		OutLog "�������� ��������� ����� �������� ���������"
		OnDatNullsStop
	End If
	If CompareStringBegin("���������", Name) Then
		OutLog "��������� ������ ���������"
		fl.Close
		Set fso = Nothing
		SetWorkOutputConfiguration("�������")
		ReleaseSelectedButtons "�����"
		EnableAllButtons
		Cyclograms.StopAllCyclograms
		Cyclograms.StartCyclogram "�������� ���������"
		SetRegime "��������"
	End If
	If CompareStringBegin("���������� �� �������", Name) Then
		OutLog "���������� �� ������� ���������"
		OnCalibrationStop
	End If
	If CompareStringBegin("���������� ������������", Name) Then
		OutLog "���������� ������������ ���������"
		' --  ���� ���������� ������������ ���� � ������� ���������� RGA
		If IsButtonPressed("������������� RGA") Then	
			OutLog "������������� RGA. 2 ����. ���������� ����������."
			PressButton "���������� ����������"
		Else
			OnCalibrationElectrometerStop
		End If
		' --  
	End If
	If CompareStringBegin("���������� �������", Name) Then
		OutLog "���������� �� ����������� ���� (" & RegimeName & ") ���������"
		OnCalibrationStop
	End If
	If CompareStringBegin("���������� ����������", Name) Then
		OnCalibrationHiVoltStop
		CEM_Ready = False
		OutLog "���������� ���������� ���������"
		' --  ���� ���������� ���������� ���� � ������� ���������� RGA
		If IsButtonPressed("������������� RGA") Then	
			OutLog "������������� RGA ���������"
			ReleaseButton "������������� RGA"
		End If
		' --  
	End If
	If UCase("�������� ���. �������") = UCase(Name) Then
		Dim ii
		For ii = 0 To UserButtons.GetSize - 1
			If UserButtons.GetName(ii) <> "�������� ���. �������" Then
				Form.chkUserButton(ii).Visible = False
			End If
		Next
		LocateButtons
	End If
	If UCase("���������� ��� ��") = UCase(Name) Then
		OutLog "���������� ����(������� ����������������) ���������"
		OnCalibrationStop
	End If
	If UCase("���������� ��� ��") = UCase(Name) Then
		OutLog "���������� ����(������ ����������������) ���������"
		OnCalibrationStop
	End If
	If CompareStringBegin("���������", Name) Then
		SetWorkOutputConfiguration("�������")
		ClearTextValues "�������"
	End If
	If CompareStringBegin("�������", Name) Then
		OnAutoStop
		OutLog "��������� ����� �� ��������������� ������ ������"
	End If
	If CompareStringBegin("����� �������", Name) Or CompareStringBegin("����� ������", Name) Then
		EnableAllButtons
		Cyclograms.StopAllCyclograms
		Cyclograms.StartCyclogram "�������� ���������"
		SetRegime "��������" '��������2
	End If
End Sub
