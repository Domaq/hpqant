
Dim waitCntrRGA : waitCntrRGA = 0	' ������� ��������� �� ��������� �������� ������ �����������
Dim maxWaitRGA : maxWaitRGA = 10	' ������������ ���������� �������� �� ��������� �������� ������ �����������

Private GQLPprotocolVersionRGA : GQLPprotocolVersionRGA = "Q:2010.05.14" & Chr(0)

Const DEMoutPacketCounterRGA = 13
Const DEMoutPacketTypeRGA = 15
Const DEMoutPacketVacuummeterShiftRGA = 16
Const DEMoutPacketValvesShiftRGA = 19
Const DEMoutPacketSpectrometerShiftRGA = 23

Const DEMinPacketSensorsShiftRGA = 16
Const DEMinPacketDetectorsShiftRGA = 49
Const DEMinPacketVacuummeterShiftRGA = 52
Const DEMinPacketValvesShiftRGA = 54
Const DEMinPacketDeviceStateShiftRGA = 57
Const DEMinPacketSpectrometerShiftRGA = 60

Private DEMremotePortRGA: DEMremotePortRGA = 7778
Private DEMlocalPortRGA: DEMlocalPortRGA = 7777
Private DEMremoteHostRGA : DEMremoteHostRGA = ""
Public DEMoutDataRGA : DEMoutDataRGA = Array()
Public DEMstoredOutDataRGA : DEMstoredOutDataRGA = Array()

Const DEMaliveTimeoutRGA = 50
Private DEMaliveCounterRGA : DEMaliveCounterRGA = 0
Private DEMpacketCounterRGA : DEMpacketCounterRGA = CLng(0)
Public DEMpacketReceivedRGA : DEMpacketReceivedRGA = True

Sub InitializeExchangeRGA
	With Form.IniFile
		.Section = "Exchange:RGA"
		.Parameter = "HPQIP"
		If .ParameterValue = "" Then
			MsgBox "�� ������ �������� HPQIP � ������ Exchange:RGA. ��������� ����� �������.", _
					vbOKOnly, "������"
			Form.UnloadRequest = True
			Exit Sub
		Else
			DEMremoteHostRGA = CStr(.ParameterValue)
		End If
		
		.Parameter = "HPQPort"
		If .ParameterValue = "" Then
			MsgBox "�� ������ �������� HPQPort � ������ Exchange:RGA. ��������� ����� �������.", _
					vbOKOnly, "������"
			Form.UnloadRequest = True
			Exit Sub
		Else
			DEMremotePortRGA = CInt(.ParameterValue)
		End If
		
		.Parameter = "DEMRGA_LocalPort"
		If .ParameterValue = "" Then
			MsgBox "�� ������ �������� DEMRGA_LocalPort � ������ Exchange:RGA. ��������� ����� �������.", _
					vbOKOnly, "������"
			Form.UnloadRequest = True
			Exit Sub
		Else
			DEMlocalPortRGA = CInt(.ParameterValue)
		End If
		
	
		Form.DEMRGA.Close
		Form.DEMRGA.Protocol = 0 ' TCP
		Form.DEMRGA.LocalPort = DEMlocalPortRGA
		Form.DEMRGA.RemotePort = DEMremotePortRGA
		Form.DEMRGA.RemoteHost = DEMremoteHostRGA
		Form.DEMRGA.Connect
		
		
			'Form.DEMRA.Bind DEMlocalPortRGA
		' ������ �� 1 ������ ������������, ����� ���������� ���� ������������
		ReDim Preserve DEMoutDataRGA(DEMoutPacketSpectrometerShiftRGA)
		Dim i : For i = 0 To DEMoutPacketSpectrometerShiftRGA
			DEMoutDataRGA(i) = Chr(0)
		Next
		ReDim Preserve DEMstoredOutDataRGA(DEMoutPacketSpectrometerShiftRGA)
		For i = 0 To DEMoutPacketSpectrometerShiftRGA
			DEMstoredOutDataRGA(i) = Chr(0)
		Next
		FireLamp "���", DeviceStateUnknown
	End With
End Sub

Sub OnDEMactionRGA
	DEMaliveCounterRGA = DEMaliveCounterRGA + 1
	'OutMess "DEMaliveCounterRGA=" & DEMaliveCounterRGA
	If DEMaliveCounterRGA >= DEMaliveTimeoutRGA Then
		DEMaliveCounterRGA = 0
		FireLamp "���", DeviceStateBad
		FireLamp "�����������", DeviceStateUnknown
		FireLamp "�������", DeviceStateUnknown
		FireLamp "�����", DeviceStateUnknown
		FireLamp "�������", DeviceStateUnknown
		Exit Sub
	End If
	Form.DEMRGA.SendData "Sensors" & vbCr
	
	'' ����������� ���������
	'Dim i : For i = 0 To Len(GQLPprotocolVersionRGA) - 1
	'	DEMoutDataRGA(i) = Mid(GQLPprotocolVersionRGA, i + 1, 1)
	'Next
	'DEMpacketCounterRGA = DEMpacketCounterRGA + 1
	'If DEMpacketCounterRGA > CLng(65535) Then DEMpacketCounterRGA = 0
	'DEMoutDataRGA(DEMoutPacketCounterRGA) = Chr(GetByte(DEMpacketCounterRGA, 2, 2))
	'DEMoutDataRGA(DEMoutPacketCounterRGA + 1) = Chr(GetByte(DEMpacketCounterRGA, 2, 1))
	'Dim PacketType : PacketType = 0
	'DEMoutDataRGA(DEMoutPacketTypeRGA) = Chr(PacketType)
	'
	'On Error Resume Next
	'' ������� ������
	'Form.DEMRGA.RemoteHost = DEMremoteHostRGA
	'Form.DEMRGA.RemotePort = DEMremotePortRGA
	'If DEMpacketReceivedRGA = True Then ' ����� �������
	'	VectorToVector DEMoutDataRGA, DEMstoredOutDataRGA
	'	waitCntrRGA = 0										
	'Else
	'	If waitCntrRGA <= maxWaitRGA Then
	'		waitCntrRGA = waitCntrRGA + 1
	'		Exit Sub
	'	End If
	'	waitCntrRGA = 0
	'	DEMpacketCounterRGA = DEMpacketCounterRGA + 1
	'	If DEMpacketCounterRGA > CLng(65535) Then DEMpacketCounterRGA = 0
	'	DEMstoredOutDataRGA(DEMoutPacketCounterRGA) = Chr(GetByte(DEMpacketCounterRGA, 2, 2))
	'	DEMstoredOutDataRGA(DEMoutPacketCounterRGA + 1) = Chr(GetByte(DEMpacketCounterRGA, 2, 1))
	'End If
	'Form.DEMRGA.SendData Form.ArrayToBytes(DEMstoredOutDataRGA)
	'
	'' ������� ������ ���������� �������
	'' ... ����� ������ �����������
	'DEMoutDataRGA(DEMoutPacketVacuummeterShiftRGA) = Chr(0)
	'' ... ���������� ���������
	'DEMoutDataRGA(DEMoutPacketValvesShiftRGA) = Chr(0)
	'' ... ������ ��� ����-������������
	'DEMoutDataRGA(DEMoutPacketSpectrometerShiftRGA) = Chr(0)
	'DEMpacketReceivedRGA = False
	''TestArrivalRGA
End Sub

Sub CheckRGAStatus
	DemState = Form.DEMRGA.State
	If DemState = 9 Or DemState = 0 Or DemState = 6 Then
		'If DemState = 9 Then OutMess "socket state: error 9"
		'If DemState = 0 Then OutMess "socket state: error 0"
		'If DemState = 6 Then OutMess "socket state: error 6"
		FireLamp "�����������", DeviceStateOff
		Form.DEMRGA.Close
		Form.DEMRGA.Protocol = 0 ' TCP
		Form.DEMRGA.LocalPort = DEMlocalPortRGA
		Form.DEMRGA.RemotePort = DEMremotePortRGA
		Form.DEMRGA.RemoteHost = DEMremoteHostRGA
		Form.DEMRGA.Connect
	End If
End Sub


Sub DEMCommandHPQ (str)
	On Error Resume Next
	Err.Clear
	'OutMess "output: " & str
	'OutLog "output: " & str
	Form.DEMRGA.SendData str
	If Err.Number <> 0 Then 
		'OutMess "RGA send error."
		FireLamp "�����������", DeviceStateOff
		Form.DEMRGA.Close
		Form.DEMRGA.Protocol = 0 ' TCP
		Form.DEMRGA.LocalPort = DEMlocalPortRGA
		Form.DEMRGA.RemotePort = DEMremotePortRGA
		Form.DEMRGA.RemoteHost = DEMremoteHostRGA
		Form.DEMRGA.Connect
		Err.Clear
	End If
	On Error Goto 0
End Sub


Private TestMassCounterRGA : TestMassCounterRGA = 0
Private TestOddPointRGA : TestOddPointRGA = False

Sub TestArrivalRGA
	' �����������
	Dim testInFone : testInFone = False
	Dim v: For Each v In VCU.Valves
		If v.Title = "�1" And v.ButtonChecked = True Then
			testInFone = True
			Exit For
		End If
	Next

'	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
'debug.text.text = DEMaliveCounterRGA
'	Dim val: val = cdbl(Form.GTime.GetSecond(CurrentDateTime))
'	If Int(val / 2) * 2 = val Then
'		val = val + 10
'	Else
'		val = val - 10
'	End If
	Dim val
	If TestMassCounterRGA = 0 Then
		If TestOddPointRGA = True Then
			TestOddPointRGA = False
		Else
			TestOddPointRGA = True
		End If
	End If
	If TestOddPointRGA = True Then
		val = 10.0
	Else
		val = -10.0
	End If
	If testInFone = True Then val = val - 15.0
	Dim value : value = CDbl((TestMassCounterRGA + 1) * 50.0 + val / 1.0)
	Dim gasnames : gasnames = AttributesOfGases.GetAllNames
	Dim mass : mass = RawData.GetChannel(RegimeName, gasnames(TestMassCounterRGA))
	RawData.SetValue mass, value
	Debug.Data(TestMassCounterRGA).Title = Format(TestMassCounterRGA + 1, 2, 0)
	Debug.Data(TestMassCounterRGA).Format = "%.2f"
	Debug.Data(TestMassCounterRGA).Value = value
	If TestMassCounterRGA = MaxGases - 1 Then
		Calculate
	End If
	TestMassCounterRGA = TestMassCounterRGA + 1
	If TestMassCounterRGA >= MaxGases Then
		TestMassCounterRGA = 0
	End If

	' ���������� �������
	ReDim data(NumberOfSensors - 1)
	Dim i : For i = 0 To UBound(data)
		data(i) = CInt((i + 1) * 1000)
	Next
	ProcessSensors data

	DEMpacketReceivedRGA = True
End Sub

Sub OnDEMRGAdataArrival(bytesTotal)
'On Error Resume Next
	If Form.DEMRGA.State = sckError Then
		WriteLog "��� ������"
		Exit Sub
	End If
	Dim arrival
	Form.DEMRGA.GetData arrival, vbArray + vbByte
	Form.RepackArray arrival
	If UBound(arrival) < 2 Then
		WriteLog "����� �����"
		' ���������� ����� ������
		Exit Sub
	End If
	ProcessSpectrometer arrival
	
	'data = Form.SubArray(arrival, DEMinPacketSpectrometerShiftRGA, _
	'		1 + UBound(arrival) - DEMinPacketSpectrometerShiftRGA)
	''ProcessSpectrometer data
End Sub
