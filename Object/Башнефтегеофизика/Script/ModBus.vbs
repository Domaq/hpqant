Private ip : ip = Cstr("192.168.137.202")
'Private ip : ip = Cstr("192.168.127.254")


Function GetIntegerFromLong(value, index)
	If index = 0 Then
		GetIntegerFromLong = (value And &HFFFF0000&) / (2 ^ 16)
		If GetIntegerFromLong > 32767 Then GetIntegerFromLong = GetIntegerFromLong - (2 ^ 16)
		Exit Function
	End If
	GetIntegerFromLong = value And &H0000FFFF&
	If GetIntegerFromLong > 32767 Then GetIntegerFromLong = GetIntegerFromLong - (2 ^ 16)
End Function


Function GetByteFromLong(value, index)
	If index = 0 Then
		GetByteFromLong = (value And &HFF000000&) / (2 ^ 24)
		If GetByteFromLong < 0 Then GetByteFromLong = GetByteFromLong + 256
		Exit Function
	End If
	If index = 1 Then
		GetByteFromLong = (value And &H00FF0000&) / (2 ^ 16)
		If GetByteFromLong < 0 Then GetByteFromLong = GetByteFromLong + 256
		Exit Function
	End If
	If index = 2 Then
		GetByteFromLong = (value And &H0000FF00&) / (2 ^ 8)
		If GetByteFromLong < 0 Then GetByteFromLong = GetByteFromLong + 256
		Exit Function
	End If
	GetByteFromLong = value And &H000000FF&
	If GetByteFromLong < 0 Then GetByteFromLong = GetByteFromLong + 256
End Function


Function GetLongFromBytesNew(byte1, byte2, byte3, byte4)
	GetLongFromBytesNew = 0
	If byte1 > 127 Then
       GetLongFromBytesNew = ((byte1 And &H7F&) * &H1000000 Or &H80000000) Or  _
              (byte2) * &H10000 Or _
              (byte3 * &H100&) Or byte4
	Else
       GetLongFromBytesNew = (byte1  * &H1000000) Or  _
              (byte2 * &H10000) Or _
              (byte3 * &H100&) Or byte4
	End If
End Function


Function GetLongFromIntegersNew(int1, int2)
	GetLongFromIntegersNew = 0
	If int1 < 0 Then
       GetLongFromIntegersNew = ((int1 And &H7FFF&) * &H10000 Or &H80000000) Or int2
	Else
       GetLongFromIntegersNew = (int1 And &H0000FFFF&) * (2 ^ 16) + (int2 And &H0000FFFF&)
	End If
End Function







Sub InitializeModBus
	'Form.GModbus1.stopSlave()
	'Form.GModbus2.stopSlave()
	'Form.GModbus3.stopSlave()
	'Form.GModbus4.stopSlave()
	
	If isTestWithoutModbus = False Then
		ConnectTo1  ' 6598   8000
	Else
		OutMess "Modbus disabled"
	End If
	'GetMatFrom1
	'GetMatFrom2
	'GetFoneFrom1
	'GetFoneFrom2
	
	'SendFoneTo1
	'SendCoefTo1
	'SendFoneToReset1 31000
	'SendCoefToReset1 0.04110
	'SendFoneToReset2 0
	'SendCoefToReset2 0.0385000
End Sub

Private FI_Type
Private FI_Ip
Private FI_Port
Private FI_Address
Private FI_Localport
Private FI_DevicePort
Private FI_BaudRate


Sub ReadModbusIni(section)
	With Form.IniFile
		.Read IniDir & "Exchange.ini"
		.Section = section
		.Parameter = "Type"
		FI_Type = CStr(.ParameterValue)
		If FI_Type = "TCP" Then
			.Parameter = "Ip"
			FI_Ip = CStr(.ParameterValue)
			.Parameter = "Port"
			FI_Port = CInt(.ParameterValue)
			.Parameter = "Address"
			FI_Address = CInt(.ParameterValue)
			.Parameter = "Localport"
			FI_Localport = CInt(.ParameterValue)
		ElseIf FI_Type = "RTU" Then
			.Parameter = "Address"
			FI_Address = CInt(.ParameterValue)
			.Parameter = "DevicePort"
			FI_DevicePort = CInt(.ParameterValue)
			.Parameter = "BaudRate"
			FI_BaudRate = CInt(.ParameterValue)
			.Parameter = "Localport"
			FI_Localport = CInt(.ParameterValue)
		Else
			OutMess "Wrong modbus type"
		End If
	End With
End Sub


Sub ConnectTo1
	'Init Slave 1
	OutMess  "ModBus Slave 1 initialization"
	ReadModbusIni "Valves:1"
	With Form.GModbus1
		localIp = CStr("127.0.0.1")'CStr("192.168.1.88")'
		localPort = FI_Localport
		OutMess  "Local Slave: ip = " & CStr(localIp) & " port = " & CStr(localPort)
		'rc = .createMap (400)
		'If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
		'	res = .getConnectionStatusInfo()
		'	OutMess res & ". createMap error: " & CStr(rc)
		'End If
		rc = .initConnectionAsSlave (localIp,localPort)
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
			res = .getConnectionStatusInfo()
			OutMess res & ". Local Slave error: " & CStr(rc)
		End If
		rc = .modbusRemoteMastersInitialization(1)
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
			res = .getConnectionStatusInfo()
			OutMess res & ". Remote Master Initialization error: " & CStr(rc)
		End If
		If FI_Type = "RTU" Then
			device = "\\.\COM" & CStr(FI_DevicePort)
			rc = .modbusAddMasterToRemoteSlaveRTU(device, FI_Address, FI_BaudRate)
			OutMess "Remote Slave: type = RTU address = " & CStr(FI_Address) & " device= " & device & " baudrate = " & CStr(FI_BaudRate)
		Else
			rc = .modbusAddMasterToRemoteSlaveTCP(FI_Ip, FI_Port, FI_Address)
			OutMess "Remote Slave: type = TCP address = " & CStr(FI_Address) & " ip= " & FI_Ip & " port = " & CStr(FI_Port)
		End If
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
			res = .getConnectionStatusInfo()
			OutMess res & ". Remote Slave Addition error: " & CStr(rc)
		End If
		
	End With
	If (rc <> 1) Then
		DataTransfer1 = False
	End If
	OutMess "ModBus Slave 1 initialization Done"
	'--------
	'SendFoneTo2
End Sub

