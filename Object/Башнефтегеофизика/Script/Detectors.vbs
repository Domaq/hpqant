Const NumberOfDetectors = 16

Sub ProcessDetectors(data)
	Dim d : For Each d In VCU.Detectors
		Dim Name : Name = d.Title & d.TitleExt
		Dim code : code = DetectorsMap.GetCode(Name)
		'MsgBox "Name = " & Name & " code = " & code
		If code > 0 Then
			Dim tmp : tmp = Int(data / (2 ^ (code - 1)))
			tmp = tmp - Int(tmp / 2) * 2
			If (tmp = 0) Then 
				d.ButtonChecked = False
			Else
				d.ButtonChecked = True
			End If
		End If
	Next
	For i = 0 To NumberOfDetectors - 1
		Dim checked : checked = Int(data / (2 ^ i))
		checked = checked - Int(checked / 2) * 2
		If (checked = 0) Then 
			Debug.Detectors(i).LampColor = RGB(0, 0, 255)
		Else
			Debug.Detectors(i).LampColor = RGB(0, 255, 0)
		End If

		'��������� �������� �����
		'code = DetectorsMap.GetCode("����")
		'If i = code - 1 Then
		'	If checked Then
		'		Form.AlarmFlag = True
		'		FireLamp "����", DeviceStateBad
		'	Else
		'		Form.AlarmFlag = False
		'		FireLamp "����", DeviceStateOn
		'	End If
		'End If

		'��������� ������� ����
		'code = DetectorsMap.GetCode("��")
		'If i = code - 1 Then
		'	If checked Then
		'		FireLamp "������ ����", DeviceStateBad
		'	Else
		'		FireLamp "������ ����", DeviceStateOn
		'	End If
		'End If		
		
		''��������� ��������� ������� ������ 1
		'code = DetectorsMap.GetCode("������ �� ���1")
		'If i = code - 1 Then
		'	If checked Then
		'		'Form.AlarmFlag = False
		'		FireLamp "������ �� ���1", DeviceStateOn
		'	Else
		'		'Form.AlarmFlag = True
		'		FireLamp "������ �� ���1", DeviceStateBad
		'	End If
		'End If
		
		'��������� ��������� ������� ������ 2
		'code = DetectorsMap.GetCode("������ �� ���2")
		'If i = code - 1 Then
		'	If checked Then
		'		'Form.AlarmFlag = False
		'		FireLamp "������ �� ���2", DeviceStateOn
		'	Else
		'		'Form.AlarmFlag = True
		'		FireLamp "������ �� ���2", DeviceStateBad
		'	End If
		'End If
	Next
End Sub
