
Dim waitCntr : waitCntr = 0	' ������� ��������� �� ��������� �������� ������ �����������
Dim maxWait : maxWait = 10	' ������������ ���������� �������� �� ��������� �������� ������ �����������

Private GQLPprotocolVersion : GQLPprotocolVersion = "Q:2010.05.14" & Chr(0)

Const DEMoutPacketCounter = 13
Const DEMoutPacketType = 15
Const DEMoutPacketVacuummeterShift = 16
Const DEMoutPacketValvesShift = 19
Const DEMoutPacketSpectrometerShift = 23

Const DEMinPacketSensorsShift = 16
Const DEMinPacketDetectorsShift = 49
Const DEMinPacketVacuummeterShift = 52
Const DEMinPacketValvesShift = 54
Const DEMinPacketDeviceStateShift = 57
Const DEMinPacketSpectrometerShift = 60

Const DEMremotePort = 7778
Const DEMlocalPort = 7777
Private DEMremoteHost : DEMremoteHost = ""
Public DEMoutData : DEMoutData = Array()
Public DEMstoredOutData : DEMstoredOutData = Array()

Const DEMaliveTimeout = 50
Private DEMaliveCounter : DEMaliveCounter = 0
Private DEMpacketCounter : DEMpacketCounter = CLng(0)
Public DEMpacketReceived : DEMpacketReceived = True

Sub InitializeExchangeGQLP
	With Form.IniFile
		.Section = "Exchange:GQLP"
		.Parameter = "ForeignIP"
		If .ParameterValue = "" Then
			MsgBox "�� ������ �������� ForeignIP � ������ GQLP. ��������� ����� �������.", _
					vbOKOnly, "������"
			Form.UnloadRequest = True
			Exit Sub
		Else
			DEMremoteHost = CStr(.ParameterValue)
			Form.DEM.Protocol = 1 ' UDP
			Form.DEM.LocalPort = DEMlocalPort
			Form.DEM.RemotePort = DEMremotePort
			Form.DEM.RemoteHost = DEMremoteHost
			Form.DEM.Bind DEMlocalPort
		End If
		' ������ �� 1 ������ ������������, ����� ���������� ���� ������������
		ReDim Preserve DEMoutData(DEMoutPacketSpectrometerShift)
		Dim i : For i = 0 To DEMoutPacketSpectrometerShift
			DEMoutData(i) = Chr(0)
		Next
		ReDim Preserve DEMstoredOutData(DEMoutPacketSpectrometerShift)
		For i = 0 To DEMoutPacketSpectrometerShift
			DEMstoredOutData(i) = Chr(0)
		Next
		FireLamp "���", DeviceStateUnknown
	End With
End Sub

Sub OnDEMaction
	DEMaliveCounter = DEMaliveCounter + 1
	'OutMess "DEMaliveCounter=" & DEMaliveCounter
	If DEMaliveCounter >= DEMaliveTimeout Then
		DEMaliveCounter = 0
		FireLamp "���", DeviceStateBad
		FireLamp "�����������", DeviceStateUnknown
		FireLamp "�������", DeviceStateUnknown
		FireLamp "�����", DeviceStateUnknown
		FireLamp "�������", DeviceStateUnknown
		Exit Sub
	End If
	' ����������� ���������
	Dim i : For i = 0 To Len(GQLPprotocolVersion) - 1
		DEMoutData(i) = Mid(GQLPprotocolVersion, i + 1, 1)
	Next
	DEMpacketCounter = DEMpacketCounter + 1
	If DEMpacketCounter > CLng(65535) Then DEMpacketCounter = 0
	DEMoutData(DEMoutPacketCounter) = Chr(GetByte(DEMpacketCounter, 2, 2))
	DEMoutData(DEMoutPacketCounter + 1) = Chr(GetByte(DEMpacketCounter, 2, 1))
	Dim PacketType : PacketType = 0
	DEMoutData(DEMoutPacketType) = Chr(PacketType)

On Error Resume Next
	' ������� ������
	Form.DEM.RemoteHost = DEMremoteHost
	Form.DEM.RemotePort = DEMremotePort
	If DEMpacketReceived = True Then ' ����� �������
		VectorToVector DEMoutData, DEMstoredOutData
		waitCntr = 0										
	Else
		If waitCntr <= maxWait Then
			waitCntr = waitCntr + 1
			Exit Sub
		End If
		waitCntr = 0
		DEMpacketCounter = DEMpacketCounter + 1
		If DEMpacketCounter > CLng(65535) Then DEMpacketCounter = 0
		DEMstoredOutData(DEMoutPacketCounter) = Chr(GetByte(DEMpacketCounter, 2, 2))
		DEMstoredOutData(DEMoutPacketCounter + 1) = Chr(GetByte(DEMpacketCounter, 2, 1))
	End If
	Form.DEM.SendData Form.ArrayToBytes(DEMstoredOutData)

	' ������� ������ ���������� �������
	' ... ����� ������ �����������
	DEMoutData(DEMoutPacketVacuummeterShift) = Chr(0)
	' ... ���������� ���������
	DEMoutData(DEMoutPacketValvesShift) = Chr(0)
	' ... ������ ��� ����-������������
	DEMoutData(DEMoutPacketSpectrometerShift) = Chr(0)
	DEMpacketReceived = False
	'TestArrival
End Sub

Private TestMassCounter : TestMassCounter = 0
Private TestOddPoint : TestOddPoint = False

Sub TestArrival
	' �����������
	Dim testInFone : testInFone = False
	Dim v: For Each v In VCU.Valves
		If v.Title = "�1" And v.ButtonChecked = True Then
			testInFone = True
			Exit For
		End If
	Next

'	Dim CurrentDateTime : CurrentDateTime = Form.GTime.GetCurrentDATE
'debug.text.text = DEMaliveCounter
'	Dim val: val = cdbl(Form.GTime.GetSecond(CurrentDateTime))
'	If Int(val / 2) * 2 = val Then
'		val = val + 10
'	Else
'		val = val - 10
'	End If
	Dim val
	If TestMassCounter = 0 Then
		If TestOddPoint = True Then
			TestOddPoint = False
		Else
			TestOddPoint = True
		End If
	End If
	If TestOddPoint = True Then
		val = 10.0
	Else
		val = -10.0
	End If
	If testInFone = True Then val = val - 15.0
	Dim value : value = CDbl((TestMassCounter + 1) * 50.0 + val / 1.0)
	Dim gasnames : gasnames = AttributesOfGases.GetAllNames
	Dim mass : mass = RawData.GetChannel(RegimeName, gasnames(TestMassCounter))
	RawData.SetValue mass, value
	Debug.Data(TestMassCounter).Title = Format(TestMassCounter + 1, 2, 0)
	Debug.Data(TestMassCounter).Format = "%.2f"
	Debug.Data(TestMassCounter).Value = value
	If TestMassCounter = MaxGases - 1 Then
		Calculate
	End If
	TestMassCounter = TestMassCounter + 1
	If TestMassCounter >= MaxGases Then
		TestMassCounter = 0
	End If

	' ���������� �������
	ReDim data(NumberOfSensors - 1)
	Dim i : For i = 0 To UBound(data)
		data(i) = CInt((i + 1) * 1000)
	Next
	ProcessSensors data

	DEMpacketReceived = True
End Sub

Sub OnDEMdataArrival(bytesTotal)
	Exit Sub
'On Error Resume Next
	If Form.DEM.State = sckError Then
		WriteLog "��� ������"
		Exit Sub
	End If
	Dim arrival
	Form.DEM.GetData arrival, vbArray + vbByte
	Form.RepackArray arrival
	If UBound(arrival) < 12 Then
		WriteLog "����� �����"
		' ���������� ����� ������
		Exit Sub
	End If
	Dim ver : ver = ""
	Dim i : For i = 0 To 12
		ver = ver & Chr(arrival(i))
	Next
	If ver <> GQLPprotocolVersion Then
		WriteLog "������ ���������: " & ver
		Exit Sub
	End If
	DEMaliveCounter = 0
	DEMpacketReceived = True
	FireLamp "���", DeviceStateOn
	Dim extractor : Set extractor = New ExtractorOfNumericals
	extractor.SetSource arrival
	extractor.SetIsIntelOrder False
	Dim data
	' ������ ��������
	extractor.SetShift DEMinPacketSensorsShift + 1
	extractor.SetAmount NumberOfSensors
	extractor.SetLength 2
	extractor.SetIsSigned False
	data = extractor.GetArray
	ProcessSensors data
	' ������ ����������
	extractor.SetShift DEMinPacketDetectorsShift + 1
	extractor.SetLength 2
	extractor.SetIsSigned False
	data = extractor.GetNumerical
	ProcessDetectors data
	' ������� ��������� ��������
	extractor.SetShift DEMinPacketValvesShift + 1
	extractor.SetLength 2
	extractor.SetIsSigned False
	data = extractor.GetNumerical
	ProcessValves data
	If UBound(arrival) < DEMinPacketSpectrometerShift + 3 Then Exit Sub
	data = Form.SubArray(arrival, DEMinPacketSpectrometerShift, _
			1 + UBound(arrival) - DEMinPacketSpectrometerShift)
	'ProcessSpectrometer data
End Sub
