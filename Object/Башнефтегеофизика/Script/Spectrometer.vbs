Const RGAcomplete = 0						' ��������� RGAcommandState
Const RGApending = 1						' ��������� RGAcommandState
Const RGAerror = 3							' ��������� RGAcommandState

Const CEMNoCalib = 0						' ��������� ��� ���������� ��
Const CEMPrepare = 1						' ��������� ���������� ���������� ��
Const CEMCalib = 2							' ��������� ���� ���������� ��
Const CEMCalibSave = 3						' ��������� ������ ���������� � ������
Const CEMCalibEnded = 4						' ��������� ���������� �� ��������

'--
' ������������� ����������
Public Elect_Started:Elect_Started = False		' ���� ���������� ����������� ��������, �����
Public Elect_CL_snded:Elect_CL_snded = False	' ���� ��� ���������� ���������� ������� ������� CL

Private CEM_State : CEM_State = CEMNoCalib		' ��� ��������� ���������� ��
Public CEM_READY: CEM_Ready = False
Private CEM_Exist : CEM_Exist = 0				' ���� �� � ������� ��
Private CEM_MemoryGain : CEM_MemoryGain = 0		' ����������� �������� �� ������ 2,111, � �������
Private CEM_CalibGain : CEM_CalibGain = 2029	' ����������� ��������, ��� ������� ���� ��������� ���������� 2100 � �������
Private CEM_MemoryVolt : CEM_MemoryVolt = 0		' ������� ��������� ���������� �� ������(������?)
Private CEM_ScanSpeed : CEM_ScanSpeed = 7		' �������� ������������ ��� ���������� 0..7 (�������� � �������� 1)
Private CEM_HV : CEM_HV = 1000					' ������� ��������� ���������� 0...2490
Const CEM_HV_A_Init = 300
Const CEM_HV_B_Init = 2348
Private CEM_HV_A   								' Min ��������� ���������� 0...2490
Private CEM_HV_B								' Max ��������� ���������� 0...2490
Private CEM_ScanCounter : CEM_ScanCounter = 0	' ������� ������ ������������
Private CEM_ScanNum : CEM_ScanNum = 10			' ���������� ������ ������������ ��� ����������
Private CEM_CalibMASS : CEM_CalibMASS = 29		' �����, �� ������� ����������� ����������
Private CEM_HV0Mid : CEM_HV0Mid = 0				' ��������� �� CEM_CalibMASS ����� ��� ���������� �� CEM_ScanNum ��������
Private CEM_Razn : CEM_Razn = 0					' ������� ��������, ������� ����� ������ � ����� ��-�� ��������� ��� ������ ��
Private CEM_ARnormal : CEM_ARnormal = 0			' ������� �������� ������ ��� ������������� ��������� CH4

'--
Public RGASended: RGASended = False
Public RegimeChanged: RegimeChanged = False
Const RGAaliveTimeout = 20
Private RGAaliveCounter : RGAaliveCounter = 0
Private RGACycleAliveCntr : RGACycleAliveCntr = 0
Private RGACycleAliveMAX : RGACycleAliveMAX = 10

Const RGAnoAnswer = 255
Private RGAcommandPending : RGAcommandPending = ""
Private RGAsubcommandPending : RGAsubcommandPending = ""
Private RGAincomingData : RGAincomingData = ""
Private RGAbyteData : RGAbyteData = Array(0, 0, 0, 0)
Private RGAcommandState : RGAcommandState = RGAcomplete			' ��������� �������(�������, ��=����� ��������� ������, ����� - ������)
Public RGArequestedCommand : RGArequestedCommand = ""


Public RGAhighVoltageDefault : RGAhighVoltageDefault = 0
Public RGAhighVoltage : RGAhighVoltage = 0						' �������� � ��������� ���������� ����������
Public RGAhighVoltageSaved : RGAhighVoltageSaved = 0			' ����������� RGAhighVoltage ��� ���������� ����������
Public RGAactualHighVoltage : RGAactualHighVoltage = 9999		' ���������� �� ������  ���������� ����������
Const RGAHighVoltageDeviation = 30								' ����������� ���������� ������� ����� ������������ � �������� ����������� ����������

Public RGAfilament : RGAfilament = 0							' �������� � ��������� ���������� ������
Public RGAactualFilament : RGAactualFilament = 9999				' ���������� �� ������ ���������� ������
Public RGAfilamentSaved : RGAfilamentSaved = 0					' ����������� RgaFilament ��� ���������� ������

Public RGAGain : RGAGain = 0									' ����������� �������� ��
Public RGAGainFromIniFile : RGAGainFromIniFile = 0				' ����������� �������� �� �� ��� �����
Public RGAnoiseFloor : RGAnoiseFloor = -1
Public HPQAccuracy : HPQAccuracy = -1


Private isTestWithoutFilament : isTestWithoutFilament = False
Public isTestWithoutModbus : isTestWithoutModbus = True

'=====================================================

'=====================================================

Private RGACommandsNames
Private RGAnormalCommandsNamesGood
Private RGAnormalCommandsNamesBad
Private RGAflCommandsNamesGood
Private RGAflCommandsNamesBad
Private RGAhvCommandsNamesGood
Private RGAhvCommandsNamesBad
Private RGAidCommandsNamesGood
Private RGAidCommandsNamesBad
Private RGAHiVoltCalibCommandsNamesGood
Private RGAHiVoltCalibCommandsNamesBad
Private RGAexCommandsNamesGood
Private RGAexCommandsNamesBad

'========================================================================================
' �������������, ������ ���������� �� ���, �������� ������� ������ ����������
Sub InitializeSpectrometer
	With Form.IniFile
		Dim value
		.Read IniDir & "Spectrometer.ini"
		
		.Section = "Mult"
		.Parameter = "HighVoltage"
		If SetHighVoltage(.ParameterValue) = False Then
			InformAboutParameterError "HighVoltage", "Spectrometer"
			Form.UnloadRequest = True
			Exit Sub
		End If
		RGAhighVoltageDefault = RGAhighVoltage
		
		RGAhighVoltageSaved = RGAhighVoltage
		RGAhighVoltage = 0
		RegimeName = "��������"
		
		'.Parameter = "Gain"
		'If .ParameterValue > 0 And .ParameterValue < 10000 Then
		'	RGAGain = .ParameterValue
		'End If
		RGAGain = 1000
		RGAGainFromIniFile = RGAGain
		
		.Parameter = "CalibMass"
		If .ParameterValue > 0 And .ParameterValue < 100 Then
			CEM_CalibMASS = .ParameterValue
		End If	
		
		.Section = "Spectrometer"
		.Parameter = "NoiseFloor"
		RGAnoiseFloor = CLng(.ParameterValue)
		OutMess "RGAnoiseFloor = " & RGAnoiseFloor				'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		If RGAnoiseFloor = -1 Then
			InformAboutParameterError "NoiseFloor", "Spectrometer"
			Form.UnloadRequest = True
			Exit Sub
		Else
			If RGAnoiseFloor < 0 Then RGAnoiseFloor = 0
			If RGAnoiseFloor > 7 Then RGAnoiseFloor = 7
		End If

		.Parameter = "FaradayOn"
		If .ParameterValue <> "False" Then
			SetHighVoltage(0)
		End If
		
		.Parameter = "Filament"
		If SetFilament(.ParameterValue) = False Then
			InformAboutParameterError "Filament", "Spectrometer"
			Form.UnloadRequest = True
			Exit Sub
		End If
		
		.Section = "HPQ"
		.Parameter = "Accuracy"
		HPQAccuracy = CLng(.ParameterValue)
		OutMess "HPQAccuracy = " & HPQAccuracy				'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		If HPQAccuracy = -1 Then
			InformAboutParameterError "Accuracy", "HPQ"
			Form.UnloadRequest = True
			Exit Sub
		Else
			If HPQAccuracy < 0 Then HPQAccuracy = 0
			If HPQAccuracy > 8 Then HPQAccuracy = 8
		End If
	End With
		
	'RGACommandsNames =           	Array("69",  "ID",  "HV",  "FL",  "NF",  "MR",  "CEM")		'������ ���� ��������� ������
	'RGAnormalCommandsNamesGood = 	Array("ID",  "HV",  "FL",  "NF",  "MR",  "MR",  "MR")
	'RGAnormalCommandsNamesBad =  	Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	'RGAhvCommandsNamesGood =     	Array("HV",  "HV",  "HV",  "HV",  "HV",  "HV",  "HV")
	'RGAhvCommandsNamesBad =      	Array("HV",  "HV",  "HV",  "HV",  "HV",  "HV",  "HV")
	'RGAflCommandsNamesGood =     	Array("FL",  "FL",  "FL",  "FL",  "FL",  "FL",  "FL")
	'RGAflCommandsNamesBad =      	Array("FL",  "FL",  "FL",  "FL",  "FL",  "FL",  "FL")
	'RGAidCommandsNamesGood =     	Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	'RGAidCommandsNamesBad =      	Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	'RGAHiVoltCalibCommandsNamesGood=Array("CEM", "CEM", "CEM", "CEM", "CEM", "CEM", "CEM")
	'RGAHiVoltCalibCommandsNamesBad =Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	
	RGACommandsNames =           	Array("69",  "ID",  "PJ",  "FL",  "SA",  "SS",  "EX",  "CEM")		'������ ���� ��������� ������
	RGAnormalCommandsNamesGood = 	Array("ID",  "PJ",  "FL",  "SA",  "SS",  "FL",  "FL",  "FL")
	RGAnormalCommandsNamesBad =  	Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	RGAhvCommandsNamesGood =     	Array("HV",  "HV",  "HV",  "HV",  "HV",  "HV",  "HV",  "HV")
	RGAhvCommandsNamesBad =      	Array("HV",  "HV",  "HV",  "HV",  "HV",  "HV",  "HV",  "HV")
	RGAflCommandsNamesGood =     	Array("FL",  "FL",  "FL",  "FL",  "FL",  "FL",  "FL",  "FL")
	RGAflCommandsNamesBad =      	Array("FL",  "FL",  "FL",  "FL",  "FL",  "FL",  "FL",  "FL")
	RGAidCommandsNamesGood =     	Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	RGAidCommandsNamesBad =      	Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	RGAHiVoltCalibCommandsNamesGood=Array("CEM", "CEM", "CEM", "CEM", "CEM", "CEM", "CEM", "CEM")
	RGAHiVoltCalibCommandsNamesBad =Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	
	RGAexCommandsNamesGood=         Array("EX",  "EX",  "EX",  "EX",  "EX",  "EX",  "EX",  "EX")
	RGAexCommandsNamesBad =         Array("ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID",  "ID")
	
	DisableButton "���������� ����������"
	MultCalibrationDisabled = True
	
	ReDim data(NumberOfSensors - 1)
End Sub

'========================================================================================
' ������ ������� ���������� ��
Function SetHighVoltage(value)
	If Not IsNumeric(value) Then
		SetHighVoltage = False
		Exit Function
	End If
	value = CLng(value)
	If value < 0 Then value = 0
	If value > 2500 Then value = 2500
	RGAhighVoltage = value
	Debug.Parameters(0).Title = "�������"
	Debug.tbParameters(0).Text = RGAhighVoltage
	SetHighVoltage = True
End Function

'========================================================================================
' ���/���� �����
Function SetFilament(value)
	If Not IsNumeric(value) Then
		SetFilament = False
		Exit Function
	End If
	value = CDbl(value)
	If value < 0.00 Then value = 0.00
	If value > 3.50 Then value = 3.50
	RGAfilament = value
	Debug.Parameters(1).Title = "�����"
	Debug.tbParameters(1).Text = RGAfilament
	SetFilament = True
End Function

'========================================================================================
' �������� �������� ���������� ��
Function TestHighVoltage
	If Not IsNumeric(RGAactualHighVoltage) Then
		TestHighVoltage = False
		Exit Function
	End If
	RGAactualHighVoltage = CLng(RGAactualHighVoltage)
	If RGAactualHighVoltage > RGAhighVoltage - RGAHighVoltageDeviation _
			And RGAactualHighVoltage < RGAhighVoltage + RGAHighVoltageDeviation Then
		TestHighVoltage = True
		Exit Function
	End If
	TestHighVoltage = False
End Function

'========================================================================================
' �������� �������� ���������� ������
Function TestFilament
	If Not IsNumeric(RGAactualFilament) Then
		TestFilament = False
		Exit Function
	End If
	RGAactualFilament = Cdbl(RGAactualFilament)
	If RGAactualFilament > RGAFilament - 0.1 _
			And RGAactualFilament < RGAfilament + 0.1 Then
		TestFilament = True
		Exit Function
	End If
	TestFilament = False
End Function

'========================================================================================
' �������� ������
Private filamenttimer : filamenttimer = Now()
Private filamenttimerstarted: filamenttimerstarted = False
Sub StartFilamentTimer
	filamenttimer = DateAdd("s", 80, Now())
	filamenttimerstarted = True
End Sub

Sub StopFilamentTime
	filamenttimerstarted = False
End Sub

Function TestFilamentTime
	If filamenttimerstarted = True And (Now() > filamenttimer) Then 
		TestFilamentTime = True
		OutMess "Time for filament test"
		Exit Function
	End If
	TestFilamentTime = False
End Function

'========================================================================================
' �������� �����
Private scantimer : scantimer = Now()
Private scantimerstarted: scantimerstarted = False
Sub StartScanTimer
	scantimer = DateAdd("s", 8, Now())
	scantimerstarted = True
End Sub

Sub StopScanTime
	scantimerstarted = False
End Sub

Function TestScanTime
	If scantimerstarted = True And (Now() > scantimer) Then 
		TestScanTime = True
		OutMess "scan timeout"
		Exit Function
	End If
	TestScanTime = False
End Function


'========================================================================================
' �������� ����������
Function RgaMultOn
	RgaMultOn = False
	If IsAccident Then
		OutLog "������ ������. �������� ���������� ���������"
		Exit Function
	End If
	
	If RGAhighVoltageSaved <> 0 Then
		If LampState("�����������") <> DeviceStateOn Or LampState("�����������") <> DeviceStateOn Then
			OutLog "���������� �������� ����������. ��� ����� �� ������������� ��� �������� �����"
			Exit Function
		End If
		RGAcommandsNamesGood = RGAidCommandsNamesGood
		RGAhighVoltage = RGAhighVoltageSaved
		RGAhighVoltageSaved = 0
		RgaCommandPending = "HV"
		RgaMultOn = True
		OutLog "���������� �������"
	ElseIf RGAhighVoltage = 0 Then
		OutLog "����������� ������: ���������� ��������� ����������"
	Else
		' RGAhighVoltageSaved = 0 And RGAhighVoltage <> 0 -> Mult already on, no error
		RgaMultOn = True
	End If
End Function


'========================================================================================
' ��������� ����������
Function RgaMultOff
	'RgaCommandPending = "NF"
	'RGAsubcommandPending = ""
	'RgaMultOff = True
	'Exit Function
	If RGAhighVoltage <> 0 Then
		If LampState("�����������") <> DeviceStateOn Then
			OutLog "���������� ��������� ����������. ��� ����� �� �������������"
			RgaMultOff = False
			Exit Function
		End If
		RGAhighVoltageSaved = RGAhighVoltage
		RGAhighVoltage = 0
		'RgaCommandPending = "69"
		RgaCommandPending = "HV"
		'RgaCommandPending = "NF"
		'RGAsubcommandPending = ""
		
		OutLog "���������� ��������"
	End If
	RgaMultOff = True
End Function

'========================================================================================
' �������� ��������, ������ ��� �� ����� ����
Sub OnParameterChange(index)
	Select Case index
		Case 0 SetHighVoltage Debug.tbParameters(index).Text
		Case 1 SetFilament Debug.tbParameters(index).Text
	End Select
End Sub

Dim MultCalibrationDisabled: MultCalibrationDisabled = True 		'���� ������� ����������� ����������. 
																	'����� ��� ����, ����� ������ ������ "���������� ����������" ��������/�� ��������
Dim IsAccident: IsAccident = False									'������ �������
Dim TimeAfterStart: TimeAfterStart = 0								'����� ����� ������� ��������� (��� ������� ������ �������)


'========================================================================================
' �������� ������ �������
Sub TurbineAccidentCheck
	Dim TurbineFreq, PumpCurrent
	
	Dim s : For Each s In VCU.Sensors
		If s.Title = "��" Then
			TurbineFreq = s.Value
		ElseIf s.Title = "���" Then
			PumpCurrent = s.Value
		End If
	Next
	
	If TimeAfrerStart < 5 Then
		TimeAfterStart = TimeAfterStart + 1
	End If
	
	If (TurbineFreq < 1490 Or PumpCurrent > 10) And IsAccident = False Then
		If TimeAfterStart < 5 Then Exit Sub
		OutLog "��������� ���������� � �����. ������� ������� " & TurbineFreq & ", ��� ������ " & PumpCurrent
		IsAccident = True
	
		ReleaseSelectedButtons("")
		DisableAllButtons
		MultCalibrationDisabled = False
		
		If RgaFilament <> 0 Then
			RgaFilamentSaved = RgaFilament
			RgaFilament = 0
		End If
		If RgaHighVoltage <> 0 Then
			RgaHighVoltageSaved = RgaHighVoltage
			RgaHighVoltage = 0
		End If
		RegimeName = "��������"
		
		RgaCommandPending = "69"
	ElseIf (TurbineFreq > 1490 And PumpCurrent < 10) And IsAccident = True Then
		OutLog "�������� ����� �������. ������� ������� " & TurbineFreq & ", ��� ������ " & PumpCurrent
		IsAccident = False
		EnableAllButtons
		DisableButton "���������� ����������"
		MultCalibrationDisabled = True
		
		RgaFilament = RgaFilamentSaved
		RgaCommandPending = "69"
	End If
End Sub


'========================================================================================
' ���������� ��������� ������ � �����������
Sub ControlSpectrometer
	'TurbineAccidentCheck
	'If IsButtonPressed("���������") And IsAccident = False Then
	'	Exit Sub
	'End If
    '
	'If IsButtonPressed("���������� ������������") Then
	'	Exit Sub
	'End If
	
	If RegimeName = "��������2" Then
		'Calculate
		Exit Sub
	End If
	
	
	Dim RGAcommandsNamesGood
	Dim RGAcommandsNamesBad
	'-----------------
	' ����� ������ ������������������ ������
	' ���� ������ ���������� ����������


	If IsButtonPressed("���������� ����������") And CEM_Ready = True Then
		'If LampState("�����������") <> DeviceStateOn Then
		'	OutLog "ControlSpectrometer: ��������! ���������� ���������� �����������, ��� ����� �� �������������"
		'	Exit Sub
		'End If
		RGAcommandsNamesGood = RGAHiVoltCalibCommandsNamesGood
		RGAcommandsNamesBad = RGAHiVoltCalibCommandsNamesBad
		If CEM_State = CEMNoCalib Then
			RGAcommandPending = ""
			CEM_ScanCounter = 0
			CEM_HV0Mid = 0
			OutLog "====================================================================="
			OutLog "������ ���������� ����������"
		End If
		'OutLog "ControlSpectrometer: RGAcommandsNames... = RGAHiVoltCalibCommands... RGAcommandPending=" & RGAcommandPending
	'--
	ElseIf IsButtonPressed("��������� �����") Then
		'If LampState("�����������") <> DeviceStateOn Then
		'	OutLog "ControlSpectrometer: ��������! ���������� ���������� �����������, ��� ����� �� �������������"
		'	Exit Sub
		'End If
		RGAcommandsNamesGood = RGAexCommandsNamesGood
		RGAcommandsNamesBad = RGAexCommandsNamesBad
		'OutMess "666666"
		'OutLog "ControlSpectrometer: RGAcommandsNames... = RGAHiVoltCalibCommands... RGAcommandPending=" & RGAcommandPending
	'--
	' ���� ����������� ��������� ����� �� �������������
	ElseIf LampState("�����������") = DeviceStateOff Then
		RGAcommandPending = ""
		RGASended = False
		
		FireLamp "�����������", DeviceStateUnknown
		RGAcommandsNamesGood = RGAnormalCommandsNamesGood
		RGAcommandsNamesBad = RGAnormalCommandsNamesBad
		'OutMess "555555"
	ElseIf LampState("�����������") <> DeviceStateOn And RGAcommandPending <> "ID" Then
		RGAcommandsNamesGood = RGAidCommandsNamesGood
		RGAcommandsNamesBad = RGAidCommandsNamesBad
		'OutMess "444444"
		'OutLog "ControlSpectrometer: DisableButton ���������� ����������"
	'--
	' ���� �������� �������� ���������� �� �� ��������, �� ������������� ��� ���� ��
	'ElseIf TestHighVoltage = False Then
	'	RGAcommandsNamesGood = RGAhvCommandsNamesGood
	'	RGAcommandsNamesBad = RGAhvCommandsNamesBad
	''--
	'' ���� ���� �������� �������� ���������� ������ �� ��������, �� ������������� ��� ���� ��
	ElseIf TestFilamentTime = True Then
		RGAcommandsNamesGood = RGAflCommandsNamesGood
		RGAcommandsNamesBad = RGAflCommandsNamesBad
		RGAcommandPending = RGAcomplete
		'StopFilamentTime
		'StopScanTime
		'OutMess "333333"
	'--
	ElseIf TestScanTime = True Then
		RGAcommandsNamesGood = RGAflCommandsNamesGood
		RGAcommandsNamesBad = RGAflCommandsNamesBad
		RGAcommandPending = RGAcomplete
		'StopFilamentTime
		'StopScanTime
		'OutMess "333333"
	'--
	ElseIf RegimeChanged = True Then
		RGAcommandsNamesGood = RGAflCommandsNamesGood
		RGAcommandsNamesBad = RGAflCommandsNamesBad
		'OutMess "222222"
	'' ����� ���������� ������������������
	Else
		RGAcommandsNamesGood = RGAnormalCommandsNamesGood
		RGAcommandsNamesBad = RGAnormalCommandsNamesBad
		'OutMess "111111"
		' ���������� ������ � ���� ����������� ���������� ����������, ����� ����� �� ������� ����
		If MultCalibrationDisabled = True Then
			EnableButton "���������� ����������"
			MultCalibrationDisabled = False
			'OutLog "ControlSpectrometer: EnableButton ���������� ����������"
			CEM_State = CEMNoCalib
		End If
	End If
	'-----------------
	If RGAcommandPending = "" Then
		RGAcommandPending = "69"
		
	End If
	Dim found : found = False
	Dim i
	' ���������� ������� � ����� ��������� �������
	For i = 0 To UBound(RGACommandsNames)
		If UCase(RGAcommandPending) = UCase(RGACommandsNames(i)) Then
			'OutLog "UCase = UCase: RGAcommandPending = " & RGAcommandPending
			RGAcommandProcess RGACommandsNamesGood(i), RGACommandsNamesBad(i)
			found = True
			Exit For
		End If
	Next
	If Not found Then
		'OutLog "Not found RGAcommandPending = " & RGAcommandPending
		RGAcommandPending = "69"
		RGAcommandProcess RGACommandsNamesGood(0), RGACommandsNamesBad(0)
	End If
	RGAincomingData = ""
	RGAaliveCounter = RGAaliveCounter + 1
End Sub

'========================================================================================
' ������� ������ ����� �-�� � ����������� �� RGAcommandPending � RGAcommandState
Sub RGAcommandProcess(nameGood, nameBad)
	'OutMess "==>> +Start RGAcommandProcess: nameGood=" & nameGood & " nameBad=" & nameBad
	'OutLog "==>> +Start RGAcommandProcess: nameGood=" & nameGood & " nameBad=" & nameBad
	'OutLog "==>> RGAcommandProcess: SC.Eval RGAcommand_" & RGAcommandPending & "(False)"
	
	If IsButtonPressed("���������� ������������") Then
		Exit Sub
	End If
	'If RGAcommandPending <> "NF" Then OutMess RGAcommandPending & " norm"
	'OutMess RGAcommandPending
	SC.Eval "RGAcommand_" & RGAcommandPending & "(False)"
	If RGAcommandState = RGAcomplete Then
		'OutLog "==>> RGAcommandProcess: RGAcommandState = RGAcomplete"
		'If nameGood <> "MR" Then OutMess nameGood
		SC.Eval("RGAcommand_" & nameGood & "(True)")
		Exit Sub
	End If
	If RGAcommandState = RGAerror Then
		'OutLog "==>> RGAcommandProcess: RGAcommandState = RGAerror"
		SC.Eval("RGAcommand_" & nameBad & "(True)")
		' !!!!!!!!!!!! ����� �� ������ �� ����� ���������� !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		Exit Sub
	End If
End Sub

'========================================================================================
Function RGAcommand_CEM(start)
	' ���������� ���������� � ���� 14 (��������� ����������), �.�. � ���� 1 �� �����-�� �������� �� ��������...
	
	'OutLog ":::: +Start RGAcommand_CEM: start=" & start & " CEM_State=" & CEM_State
	'OutLog ":::: +Start RGAcommand_CEM: RGAcommandPending=" & RGAcommandPending & " RGAsubcommandPending=" & RGAsubcommandPending 
	'-------------------------------------------------------
	' ���� ������� ��������� CEM_State = CEMCalibEnded
	If CEM_State = CEMCalibEnded Then
		RGAcommandState = RGAcomplete
		RGAcommandPending = ""
		If IsButtonPressed("���������� ����������") Then
			OutLog "����� ���������� ����������"
			OutLog "====================================================================="
		End If
		ReleaseButton "���������� ����������"
		EnableAllButtons
		CEM_State = CEMCalibEnded
		Exit Function
	End If
	'-------------------------------------------------------
	'-------------------------------------------------------
	'-------------------------------------------------------
	'-------------------------------------------------------
	' CEM_State = CEMPrepare
	'-------------------------------------------------------
	' 1 ���� ������ ������, �� ������������� ������, ���� MR0
	If start = True Then
		RGAcommandPending = "CEM"
		CEM_State = CEMCalib'CEMPrepare
		RGAsendCommand "MR0", RGAnoAnswer, 200
		RGAsendER
		RGAsubcommandPending = "FLansw"'"MR0"
		RGAcommandState = RGApending
		'OutLog "---- RGAcommand_CEM: CEM_State =" & CEM_State & " CEM_MemoryGain=" & CEM_MemoryGain
		'OutLog "---- RGAcommand_CEM: start = True"
		'OutLog "---- RGAcommand_CEM: RGAsendCommand MR0, RGAnoAnswer, 10"
		Exit Function
	End If
	'-------------------------------------------------------
	' 999 ���� RGAsubcommandPending = "" ������ ��� ����� ����������
	If RGAsubcommandPending = "" Then
		RGAcommandState = RGAcomplete
		RGAcommandPending = ""
		'ReleaseButton "���������� ����������"
		'EnableAllButtons
		OnCalibrationHiVoltStopCycle
		CEM_State = CEMCalibEnded
		'OutLog "---- RGAcommand_CEM: ����� ����������! ReleaseButton EnableAllButtons"
		OutLog "====================================================================="
		' !!!!!!!!!!!!!!!!!!!!!! �������� �������� ���� ������? !!!!!!!!!!!!!!!
		Exit Function
	End If
	'-------------------------------------------------------
	' 2 ���� ������, �������� �� ��
	If RGAsubcommandPending = "MR0" And CEM_State = CEMPrepare Then
		RGAsendCommand "MO?", 0, 2000
		OutLog "2) ���������� ������� ��..."
		RGAsubcommandPending = "MO?"
		RGAcommandState = RGApending
		Exit Function
	End If
	'-------------------------------------------------------
	' 3 ������������ �����, �������� �� ��
	If RGAsubcommandPending = "MO?" And CEM_State = CEMPrepare Then
		If Len(RGAincomingData) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			CEM_Exist = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If CEM_Exist = 0 Then
				OutLog "3) ������. �� �����������!"
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "MOansw"
				OutLog "3) ��. �� ��������. CEM_Exist=" & CEM_Exist
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = MO? ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If
	'-------------------------------------------------------
	' 4 ���� ������, ����� ����� ����������� �������� � ������ �������?
	If RGAsubcommandPending = "MOansw" And CEM_State = CEMPrepare Then
		RGAsendCommand "MG?", 0, 500
		OutLog "4) ���������� ������� ����������� �������� ��..."
		RGAsubcommandPending = "MG?"
		RGAcommandState = RGApending
		Exit Function
	End If
	'-------------------------------------------------------
	' 5 ������������ �����, ����������� �������� ��
	If RGAsubcommandPending = "MG?" And CEM_State = CEMPrepare Then
		If Len(RGAincomingData) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			CEM_MemoryGain = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If CEM_MemoryGain <= 0 Or CEM_MemoryGain > 1999	Then
				OutLog "5) ������. ����������� �������� = "& CEM_MemoryGain
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "MGansw"
				OutLog "5) ��. ����������� �������� �� =" & CEM_MemoryGain * 1000
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = MG? ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If	
	'-------------------------------------------------------
	' 6 ���� ������, ����� ����� ���������� �� � ������ �������?
	If RGAsubcommandPending = "MGansw" And CEM_State = CEMPrepare Then
		RGAsendCommand "MV?", 0, 400
		OutLog "6) ���������� ���������� �� � ������ �������..."
		RGAsubcommandPending = "MV?"
		RGAcommandState = RGApending
		Exit Function
	End If	
	'-------------------------------------------------------
	' 7 ������������ �����, ���������� �� � ������ �������
	If RGAsubcommandPending = "MV?" And CEM_State = CEMPrepare Then
		If Len(RGAincomingData) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			CEM_MemoryVolt = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If CEM_MemoryVolt < 1 Or CEM_MemoryVolt > 2490	Then
				OutLog "7) ������. ���������� �� � ������ ������� = "& CEM_MemoryVolt
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "MVansw"
				OutLog "7) ��. ���������� �� � ������ ������� =" & CEM_MemoryVolt
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = MV? ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If	
	'-------------------------------------------------------
	'-------------------------------------------------------
	'-------------------------------------------------------
	'-------------------------------------------------------
	' CEM_State = CEMCalib
	'-------------------------------------------------------
	' 8,9 ������ ������ �������� ����������. ���������� �������� ������������ = 1
	If RGAsubcommandPending = "MVansw" And CEM_State = CEMPrepare Then
		CEM_State = CEMCalib
		OutLog "........������ �������� ����������........"
		OutLog "9) ���������� �������� ������������ = 1"
		RGAsendCommand "NF0", RGAnoAnswer, 0
		RGAsendER
		RGAsubcommandPending = "NF0"
		RGAcommandState = RGApending
		Exit Function
	End If
	'-------------------------------------------------------
	' 10 ������ ������� �������� ������������ � ������ ����������
	If RGAsubcommandPending = "NF0" And CEM_State = CEMCalib Then
		RGAsendCommand "NF?", 0, 100
		OutLog "10) ������ ������� �������� ������������..."
		RGAsubcommandPending = "NF?"
		RGAcommandState = RGApending
		Exit Function
	End If
	'-------------------------------------------------------
	' 11 ������������ �����, ������� �������� ������������
	If RGAsubcommandPending = "NF?" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			CEM_ScanSpeed = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If CEM_ScanSpeed <> 0 Then
				OutLog "11) ������. �������� �������� ������������ = " & CEM_ScanSpeed
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "NFansw"
				OutLog "11) ��. C������� ������������ = " & CEM_ScanSpeed
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = NF? ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If
	'-------------------------------------------------------
	' 12 ������ ������� �� �����
	If RGAsubcommandPending = "NFansw" And CEM_State = CEMCalib Then
		RGAsendCommand "FL?", 8, 5000
		OutLog "12) ������ ������� �� �����..."
		RGAsubcommandPending = "FL?"
		RGAcommandState = RGApending
		Exit Function
	End If	
	'-------------------------------------------------------
	' 13 ������������ �����, ������� �� �����
	If RGAsubcommandPending = "FL?" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			RGAactualFilament = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If (Not TestFilament) Or RGAfilament = 0.0 Then
				OutLog "13) ������. ��� ������ ��������� = "& RGAactualFilament
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "FLansw"
				OutLog "13) ��. ��� ������ = " & RGAactualFilament
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = FL? ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If	
	'-------------------------------------------------------
	' 14 ��������� ����������
	If RGAsubcommandPending = "FLansw" And CEM_State = CEMCalib Then
		RGAsendCommand "HV0", 3, 20000'4000
		OutLog "��������� ����������..."
		RGAsubcommandPending = "HV0"
		RGAcommandState = RGApending
		Exit Function
	End If		
	'-------------------------------------------------------
	 '15 ������, �������� �� ����������
	If RGAsubcommandPending = "HV0" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		RGAsendCommand "HV?", 6, 1000
		RGAsubcommandPending = "HV?"
		RGAcommandState = RGApending
		Exit Function
		'RGAsendCommand "HV?", 0, 400
		'OutLog "15) ������, �������� �� ����������..."
		'RGAsubcommandPending = "HV?"
		'RGAcommandState = RGApending
		'Exit Function
	End If
	'-------------------------------------------------------
	' 15 ������������ �����, �������� �� ����������
	If RGAsubcommandPending = "HV?" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		Dim statusHV : statusHV = 0
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			statusHV = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If statusHV <> 0 Then
				OutLog "������.  �� �������� ����������. STAUS = "& statusHV
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "HV0answ"
				OutLog "��. ���������� ��������"
				FireLamp "�������", DeviceStateOff
				CEM_HV = 0
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = HV0 ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If		
	'-------------------------------------------------------
	' 16, 17 ��������� ����� CEM_CalibMASS, CEM_ScanNum ������, CEM_ScanCounter - ������� ������
	'-------------------------------------------------------
	' 16 ��������� ����� CEM_CalibMASS
	If (RGAsubcommandPending = "HV0answ" Or RGAsubcommandPending = "MR0_28answ") And CEM_State = CEMCalib Then
		RGAsendCommand "MR" & CEM_CalibMASS, 4, 5000
		CEM_ScanCounter = CEM_ScanCounter + 1			' ������� ��������
		RGAsubcommandPending = "MR0_28"
		RGAcommandState = RGApending
		Exit Function
	End If			
	'-------------------------------------------------------
	' 17 �������� �����, ����� CEM_CalibMASS
	If RGAsubcommandPending = "MR0_28" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 4 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		Dim value : value = 0.0
		Dim extractor : Set extractor = New ExtractorOfNumericals
		extractor.SetSource RGAbyteData
		extractor.SetLength 4
		extractor.SetIsSigned True
		extractor.SetIsIntelOrder True
		value = extractor.GetNumerical
		'value = CDbl(value) * 1e-2
		'If LampState("�������") = DeviceStateOn Then
		'	value = value / 2000
		'End If
		If value <= 0.0 Then value = 1e-20
		CEM_HV0Mid = CEM_HV0Mid + value / CEM_ScanNum	' ������� �������		
		OutLog "...�������� = " & CEM_ScanCounter & ". ��������� ����� = " & CEM_CalibMASS & ", ��������� = " & value
		If CEM_ScanCounter >= CEM_ScanNum Then
		OutLog "������� �������� ��� ���������� = " & CEM_HV0Mid
			RGAsubcommandPending = "MR0_28answEnd"	'MR0_28answEnd - ��������� ��������� ����� ��������, ��� �������� ������
			RGAcommandState = RGApending
			Exit Function
		End If
		RGAsubcommandPending = "MR0_28answ"
		RGAcommandState = RGApending
		Exit Function
	End If
	'-------------------------------------------------------
	' 18, 19 20,21 ��������� ���������� ���������� ��� �������� �������� ��������
	'-------------------------------------------------------
	' 18 �������� ���������� ���������� = CEM_HV
	If (RGAsubcommandPending = "MR0_28answEnd" Or RGAsubcommandPending = "MR1_28answ") And CEM_State = CEMCalib Then
		If RGAsubcommandPending = "MR0_28answEnd" Then
			CEM_CalibGain = RGAGainFromIniFile
			If CEM_CalibGain > 8000 Or CEM_CalibGain <= 0 Then
				OutLog "������, ������� ������� ����������� �������� = " & CEM_CalibGain
				RGAsubcommandPending = ""
				CEM_State = CEMCalibEnded
				RGAcommandState = RGAerror			
			End If
			
			CEM_HV_A = CEM_HV_A_Init
			CEM_HV_B = CEM_HV_B_Init
			
			If CEM_HV_A < 0 Then
				CEM_HV_A = 0
			End If
			If CEM_HV_B > 2500 Then
				CEM_HV_B = 2500
			End If
			CEM_HV = Fix( (CEM_HV_A + CEM_HV_B) / 2 )
			OutLog "Start CEM_HV_A = " & CEM_HV_A & " CEM_HV_B = " & CEM_HV_B & " CEM_HV = " & CEM_HV
		End If
		RGAsendCommand "HV" & CEM_HV, 3, 20000								'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		RGAsubcommandPending = "HVi"
		RGAcommandState = RGApending
		Exit Function
	End If
	'-------------------------------------------------------
	' 19 ������������ �����, ���������� �� ���������� ����������
	If RGAsubcommandPending = "HVi" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		statusHV = 0
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			statusHV = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If statusHV <> 1 And statusHV <> 0 Then
				OutLog "������. ���������� ���������� �� ������. STAUS = "& statusHV
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				RGAsubcommandPending = "HViAnsw"
				RGAcommandState = RGApending
				Exit Function				
			End If
		End If
		OutLog ">>>>>>>>>>>>>>>>>>> � RGAsubcommandPending = HVi ���-�� ����� �� ���... <<<<<<<<<<<<<<"
		RGAsubcommandPending = ""
		CEM_State = CEMCalibEnded
		RGAcommandState = RGAerror
		Exit Function
	End If		
	'-------------------------------------------------------
	' 20 ��������� ����� CEM_CalibMASS
	If (RGAsubcommandPending = "HViAnsw" Or RGAsubcommandPending = "MR1_28answ") And CEM_State = CEMCalib Then
		RGAsendCommand "MR" & CEM_CalibMASS, 4, 5000
		RGAsubcommandPending = "MR1_28"
		RGAcommandState = RGApending
		Exit Function
	End If			
	'-------------------------------------------------------
	' 21 �������� �����, ����� CEM_CalibMASS
	If RGAsubcommandPending = "MR1_28" And CEM_State = CEMCalib Then
		If Len(RGAincomingData) < 4 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		value = 0.0
		Set extractor = New ExtractorOfNumericals
		extractor.SetSource RGAbyteData
		extractor.SetLength 4
		extractor.SetIsSigned True
		extractor.SetIsIntelOrder True
		value = extractor.GetNumerical
		'value = CDbl(value) * 1e-2
		'If LampState("�������") = DeviceStateOn Then
		'	value = value / 2000
		'End If
		If value <= 0.0 Then value = 1e-20
		CEM_Razn = value/CEM_CalibGain - CEM_HV0Mid
		
		'If CEM_ScanCounter >= CEM_ScanNum Then
		'OutLog "17) ������� �������� ��� ���������� = " & CEM_HV0Mid
		'	RGAsubcommandPending = "MR1_28answEnd"	'MR0_28answEnd - ��������� ��������� ����� ��������, ��� �������� ������, �������� ���������
		'	Exit Function
		'End If
		If (CEM_Razn < 50 And CEM_Razn > -50) Or ( CEM_HV_B-CEM_HV_A <= 1 ) Then
			CEM_CalibGain = Fix(value/CEM_HV0Mid)
			OutLog "���������: ����� ���������� ���������� = " & Fix( CEM_HV ) & ". ������ ����������� ��������(������) = " & CEM_CalibGain
			RGAsubcommandPending = "MR1_28answEnd"
			RGAcommandState = RGApending
			CEM_State = CEMCalibSave
			Exit Function
		End If
		
		If CEM_Razn < 0 Then
			CEM_HV_A = Fix((CEM_HV_A + CEM_HV_B) / 2)
		Else
			CEM_HV_B = Fix((CEM_HV_A + CEM_HV_B) / 2)
		End If
		CEM_HV = Fix((CEM_HV_A + CEM_HV_B) / 2)
		OutLog "���������� ���������� " & CEM_HV & ", M" & CEM_CalibMASS & "/" & CEM_CalibGain & " = " & Fix(value/CEM_CalibGain)
		CEM_ARnormal = Fix(value/CEM_CalibGain)			' ���������� ��������� ������� �������� �� ������, ��� ������������� ��������� �� CH4
		RGAsubcommandPending = "MR1_28answ"
		RGAcommandState = RGApending
		Exit Function
	End If	
	'-------------------------------------------------------
	'-------------------------------------------------------
	'--------------  CEM_State = CEMCalibSave. ���������� �����������
	' 22 ���� ����� ����������� �������� � ������ �������
	If RGAsubcommandPending = "MR1_28answEnd" And CEM_State = CEMCalibSave Then
		RGAsendCommand "MG" & (CEM_CalibGain/1000), RGAnoAnswer, 0
		RGAsendER
		OutLog "���������� ����� ����������� �������� " & CEM_CalibGain/1000 & " � ������ �������..."
		RGAsubcommandPending = "MGNew"
		RGAcommandState = RGApending
		Exit Function
	End If

	'-------------------------------------------------------
	' 23 ���� ����� ���������� �� � ������ �������
	If RGAsubcommandPending = "MGNew" And CEM_State = CEMCalibSave Then
		RGAsendCommand "MV" & Fix(CEM_HV), RGAnoAnswer, 0
		RGAsendER
		OutLog "���������� ����� ���������� �� " & Fix(CEM_HV) & " � ������ �������..."
		RGAsubcommandPending = "MVNew"
		RGAcommandState = RGApending
		Exit Function
	End If	

	'-------------------------------------------------------
	' 24 ������������ �����, ���������� �� � ������ �������
	If RGAsubcommandPending = "MVNew" And CEM_State = CEMCalibSave Then
		'OutLog "24) ���������� ����� ��������� � ��� ����..."
		RGAGain = CEM_CalibGain
		SetHighVoltage(CEM_HV)
		With Form.IniFile
			.Read IniDir & "Spectrometer.ini"
			.Section = "Mult"
			.Parameter = "HighVoltage"
			.ParameterValue = RGAhighVoltage
			.Parameter = "Gain"
			.ParameterValue = CEM_CalibGain
			.Parameter = "ARnormal"
			.ParameterValue = CEM_ARnormal
			.Write
		End With
		OutLog "� ini ���� �������� ����������� ����������: HighVoltage =" & RGAhighVoltage
		OutLog "� ini ���� �������  ������ ����������� ��������: Gain = " & CEM_CalibGain
		OutLog "� ini ���� �������� ������� �������� �� ������: ARnormal = " & CEM_ARnormal
		RGAsubcommandPending = ""
		RGAcommandState = RGAcomplete
		CEM_State = CEMCalibEnded
		Exit Function
	End If	
	'---------	
	RGAcommandState = RGAerror
End Function

'========================================================================================
Function RGAcommand_69(start)
	RGAcommandState = RGAcomplete
End Function

'========================================================================================
Function RGAcommand_ID(start) 'control sensors
	StopFilamentTime
	StopScanTime
	If start = True Then
		RGAcommandPending = "ID"
		RGAsendCommandHPQ "Release"
		RGAsubcommandPending = "Release"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Or IsButtonPressed("��������� �����") Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RegimeChanged = True Then
		RegimeChanged = False
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "Release" Then
		If Len(RGAincomingData) < 9 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		RGAsendCommandHPQ "Sensors"
		RGAsubcommandPending = "Sensors"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "Sensors" Then
		If Len(RGAincomingData) < 25 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) >= 7 Then
			OutMess "State: " & rrr(5)
			OutMess "Serial Number: " & rrr(6)
			OutMess "Name: " & rrr(7)
		End If
		
		RGAsendCommandHPQ "control Ant 1.0"	
		RGAsubcommandPending = "control"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "control" Then
		If Len(RGAincomingData) < 25 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		'If Left(RGAincomingData, 6) <> "SRSRGA" Then
		'	RGAcommandState = RGAerror
		'	Exit Function
		'End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "CONTROL" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot get control"
			RGAsendCommandHPQ "control Ant 1"	
			RGAsubcommandPending = "control"
			RGAcommandState = RGApending
			Exit Function
		End If
		OutMess "Control OK"
		FireLamp "�����������", DeviceStateOn
		
		Form.lblDeviceID.Caption = "�������� �����: " & rrr(3)
		RGAsubcommandPending = ""
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	RGAcommandState = RGAerror
End Function

'========================================================================================
Private RGAtestHighVoltageCounter : RGAtestHighVoltageCounter = 0

'========================================================================================
Function RGAcommand_HV(start)
	'OutMess "HV"
	If start = True Then
		RGAcommandPending = "HV"
		'Dim dteWait : dteWait = DateAdd("s", 2, Now())
		'Do Until (Now() > dteWait)
		'Loop
		RGAsendCommand "MR0", RGAnoAnswer, 10
		RGAsendER
		RGAsubcommandPending = "MR0"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "MR0" Then
		'OutLog "���������� ���������� ����������: " & RGAhighVoltage
		RGAsendCommand "HV" & RGAhighVoltage, 3, 20000'1400					' ���� � ����� � 2 �������� ��� 2000
		RGAsubcommandPending = "HVs"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "HVs" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		RGAsendCommand "HV?", 6, 1000
		RGAsubcommandPending = "HV?"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "HV?" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			RGAactualHighVoltage = Left(RGAincomingData, Len(RGAincomingData) - 2)
			'OutLog "������� ���������� ����������: " & RGAactualHighVoltage
			If TestHighVoltage = True Then
				LampHighVoltage
				RGAsubcommandPending = ""
				RGAcommandState = RGAcomplete
				Exit Function
			Else
				If RGAtestHighVoltageCounter < 5 Then
					RGAtestHighVoltageCounter = RGAtestHighVoltageCounter + 1
				Else
					'���� ���������� ������ ����� ����� ���������� ������, ��� ����� �������, ����� �� ���� ������������ �����
					RGAtestHighVoltageCounter = 0
					LampHighVoltage
					RGAsubcommandPending = ""
					RGAcommandState = RGAcomplete
					OutLog "������� ����� �������� � ������������ ��������� ���������� ���������� ������ " & RGAHighVoltageDeviation
					Exit Function
				End If
			End If
		End If
		FireLamp "�������", DeviceStateBad
		RGAcommandState = RGAerror
		Exit Function
	End If
	RGAcommandState = RGAerror
End Function


Dim hpqRegConter: hpqRegConter = 0
Dim lastmass: lastmass = 2
Function RGAcommand_PJ(start)
	StopFilamentTime
	StopScanTime
	If start = True Then
		RGAcommandPending = "PJ"
		RGAsendCommandHPQ "ScanStop"
		RGAsubcommandPending = "ScanStop"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Or IsButtonPressed("��������� �����") Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RegimeChanged = True Then
		RegimeChanged = False
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "ScanStop" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "SCANSTOP" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot ScanStop"
			RGAsendCommandHPQ "ScanStop"
			RGAsubcommandPending = "ScanStop"
			RGAcommandState = RGApending
			Exit Function
		End If
		OutMess "ScanStop OK"
		RGAsendCommandHPQ "MeasurementremoveAll"
		RGAsubcommandPending = "MeasurementremoveAll"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "MeasurementremoveAll" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "MEASUREMENTREMOVEALL" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot MeasurementremoveAll"
			RGAsendCommandHPQ "MeasurementremoveAll"
			RGAsubcommandPending = "MeasurementremoveAll"
			RGAcommandState = RGApending
			Exit Function
		End If
		OutMess "MeasurementremoveAll OK"
		RGAsendCommandHPQ "AddPeakJump air PeakCenter " & CStr(HPQAccuracy) & " 0 0 0"
		RGAsubcommandPending = "AddPeakJumpAir"
		hpqRegConter = -1
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "AddPeakJumpAir" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If hpqRegConter = -1 Then 'AddPeakJump
			If UCase(rrr(0)) <> "ADDPEAKJUMP" Or UCase(rrr(1)) <> "OK" Then
				OutMess "Cannot ADDPEAKJUMP"
				RGAsendCommandHPQ "MeasurementremoveAll"
				RGAsubcommandPending = "MeasurementremoveAll"
				RGAcommandState = RGApending
				Exit Function
			End If
			hpqRegConter = 0
		Else 'MeasurementAddMass
			If UCase(rrr(0)) <> "MEASUREMENTADDMASS" Or UCase(rrr(1)) <> "OK" Then
				OutMess "Cannot MEASUREMENTADDMASS"
				RGAsendCommandHPQ "MeasurementremoveAll"
				RGAsubcommandPending = "MeasurementremoveAll"
				RGAcommandState = RGApending
				Exit Function
			End If
		End If
	
		reg = "������"
		GasCount = RawData.GetGasGroupSize(reg)
		If hpqRegConter >= GasCount Then
			OutMess "Measurement PeakJump air OK"
			RGAsendCommandHPQ "AddPeakJump karohv PeakCenter " & CStr(HPQAccuracy) & " 0 0 0"
			RGAsubcommandPending = "AddPeakJumpKaroHV"
			hpqRegConter = -1
			RGAcommandState = RGApending
			Exit Function
		End If
		gasnames = RawData.GetGasNames(reg)
		mass = RawData.GetChannel(reg, gasnames(hpqRegConter))
		RGAsendCommandHPQ "MeasurementAddMass " & mass
		RGAsubcommandPending = "AddPeakJumpAir"
		hpqRegConter = hpqRegConter + 1
		RGAcommandState = RGApending
		Exit Function
	End If
	
	If RGAsubcommandPending = "AddPeakJumpKaroHV" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If hpqRegConter = -1 Then 'AddPeakJump
			If UCase(rrr(0)) <> "ADDPEAKJUMP" Or UCase(rrr(1)) <> "OK" Then
				OutMess "Cannot ADDPEAKJUMP"
				RGAsendCommandHPQ "MeasurementremoveAll"
				RGAsubcommandPending = "MeasurementremoveAll"
				RGAcommandState = RGApending
				Exit Function
			End If
			hpqRegConter = 0
		Else 'MeasurementAddMass
			If UCase(rrr(0)) <> "MEASUREMENTADDMASS" Or UCase(rrr(1)) <> "OK" Then
				OutMess "Cannot MEASUREMENTADDMASS"
				RGAsendCommandHPQ "MeasurementremoveAll"
				RGAsubcommandPending = "MeasurementremoveAll"
				RGAcommandState = RGApending
				Exit Function
			End If
		End If
	
		reg = "�������_��"
		GasCount = RawData.GetGasGroupSize(reg)
		If hpqRegConter >= GasCount Then
			OutMess "Measurement PeakJump karohv OK"
			RGAsendCommandHPQ "AddPeakJump karo PeakCenter " & CStr(HPQAccuracy) & " 0 0 0"
			RGAsubcommandPending = "AddPeakJumpKaro"
			hpqRegConter = -1
			RGAcommandState = RGApending
			Exit Function
		End If
		gasnames = RawData.GetGasNames(reg)
		mass = RawData.GetChannel(reg, gasnames(hpqRegConter))
		RGAsendCommandHPQ "MeasurementAddMass " & mass
		RGAsubcommandPending = "AddPeakJumpKaroHV"
		hpqRegConter = hpqRegConter + 1
		RGAcommandState = RGApending
		Exit Function
	End If
	
	If RGAsubcommandPending = "AddPeakJumpKaro" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If hpqRegConter = -1 Then 'AddPeakJump
			If UCase(rrr(0)) <> "ADDPEAKJUMP" Or UCase(rrr(1)) <> "OK" Then
				OutMess "Cannot ADDPEAKJUMP"
				RGAsendCommandHPQ "MeasurementremoveAll"
				RGAsubcommandPending = "MeasurementremoveAll"
				RGAcommandState = RGApending
				Exit Function
			End If
			hpqRegConter = 0
		Else 'MeasurementAddMass
			If UCase(rrr(0)) <> "MEASUREMENTADDMASS" Or UCase(rrr(1)) <> "OK" Then
				OutMess "Cannot MEASUREMENTADDMASS"
				RGAsendCommandHPQ "MeasurementremoveAll"
				RGAsubcommandPending = "MeasurementremoveAll"
				RGAcommandState = RGApending
				Exit Function
			End If
		End If
		
		reg = "�������"
		GasCount = RawData.GetGasGroupSize(reg)
		gasnames = RawData.GetGasNames(reg)
		If hpqRegConter >= GasCount Then
			OutMess "Measurement PeakJump karo OK"
			RGAsubcommandPending = ""
			RGAcommandState = RGAcomplete
			Exit Function
		End If
		mass = RawData.GetChannel(reg, gasnames(hpqRegConter))
		RGAsendCommandHPQ "MeasurementAddMass " & mass
		RGAsubcommandPending = "AddPeakJumpKaro"
		hpqRegConter = hpqRegConter + 1
		RGAcommandState = RGApending
		Exit Function
	End If
	
	RGAcommandState = RGAerror
End Function


Sub LampHighVoltage
	If RGAhighVoltage <> 0.0 Then
		FireLamp "�������", DeviceStateOn
	Else
		FireLamp "�������", DeviceStateOff
	End If
End Sub

''========================================================================================
'Function RGAcommand_FL(start)
'	If start = True Then
'		RGAcommandPending = "FL"
'		RGAsendCommand "FL" & RGAfilament, 3, 15000
'		WaitCycls = FLWaitCycls    											' !!!!!!!!!!!!
'		RGAsubcommandPending = "FLs"
'		RGAcommandState = RGApending
'		RGACycleAliveCntr = 0					' !!!!!!!!!!!!
'		Exit Function
'	End If
'	If RGAsubcommandPending = "" Then
'		RGAcommandState = RGAcomplete
'		Exit Function
'	End If
'	If RGAsubcommandPending = "FLs" Then
'		If Len(RGAincomingData) < 2 Then
'			RGAcommandState = RGApending
'			Exit Function
'		End If
'		'If RGACycleAliveCntr < RGACycleAliveMAX Then
'		'	RGACycleAliveCntr = RGACycleAliveCntr + 1
'		'	OutLog "RGAcommand_FL:Exit Wait RGACycleAliveCntr = " & RGACycleAliveCntr					' !!!!!!!!!!!!
'		'	Exit Function
'		'End If
'		RGAsendCommand "FL?", 8, 5000'5000
'		RGAsubcommandPending = "FLq"
'		RGAcommandState = RGApending
'		Exit Function
'	End If
'	If RGAsubcommandPending = "FLq" Then
'		If Len(RGAincomingData) < 2 Then
'			RGAcommandState = RGApending
'			Exit Function
'		End If
'		If Right(RGAincomingData, 2) = vbLf & vbCr Then
'			RGAactualFilament = Left(RGAincomingData, Len(RGAincomingData) - 2)
'			If TestFilament = True Then
'				If RGAfilament <> 0.0 Then
'					FireLamp "�����", DeviceStateOn
'				Else
'					FireLamp "�����", DeviceStateOff
'				End If
'				RGAsubcommandPending = ""
'				RGAcommandState = RGAcomplete
'				Exit Function
'			End If
'		End If
'		FireLamp "�����", DeviceStateBad
'		RGAcommandState = RGAerror
'		Exit Function
'	End If
'	RGAcommandState = RGAerror
'End Function

'========================================================================================
Function RGAcommand_FL(start)
	'RGAcommandPending = "FL"
	'RGAcommandState = RGAcomplete
	'OutMess "FILAMENT " & CStr(start)
	StopFilamentTime
	StopScanTime
	If start = True Then
		RGAcommandPending = "FL"
		RGAsendCommandHPQ "ScanStop"
		RGAsubcommandPending = "ScanStop"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Or IsButtonPressed("��������� �����") Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RegimeChanged = True Then
		RegimeChanged = False
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "ScanStop" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "SCANSTOP" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot ScanStop"
			RGAsendCommandHPQ "ScanStop"
			RGAsubcommandPending = "ScanStop"
			RGAcommandState = RGApending
			Exit Function
		End If
		OutMess "ScanStop OK"
	''!!!
		If isTestWithoutFilament = True Then
			OutMess "Test without Filament"
			FireLamp "�����", DeviceStateOn
			RGAcommandState = RGAcomplete
			Exit Function
		End If
	''!!!!
		RGAsendCommandHPQ "FilamentInfo"
		RGAsubcommandPending = "FilamentInfo"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "FilamentInfo" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 5 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(1)) <> "OK" Then
			FireLamp "�����", DeviceStateUnknown
			OutMess "FilamentInfo Error"
			RGAsendCommandHPQ "FilamentInfo"
			RGAsubcommandPending = "FilamentInfo"
			RGAcommandState = RGApending
			Exit Function
		End If
		OutMess "FilamentInfo" & ":" & rrr(5) & " " & rrr(3)
		If rrr(3) = "ON" Then
			FireLamp "�����", DeviceStateOn
			RGAsubcommandPending = ""
			RGAcommandState = RGAcomplete
			Exit Function
		End If
		If rrr(3) = "WARM-UP" Or rrr(3) = "COOL-DOWN" Then ' warm-up cool-down
			FireLamp "�����", DeviceStateUnknown
			RGAsendCommandHPQ "FilamentInfo"
			RGAsubcommandPending = "FilamentInfo"
			RGAcommandState = RGApending
			Exit Function
		End If
		
		If rrr(3) = "BAD-EMISSION" Then
			RGAsendCommandHPQ "FilamentInfo"
			RGAsubcommandPending = "FilamentInfo"
			RGAcommandState = RGApending
			Exit Function
		End If
		FireLamp "�����", DeviceStateOff
		RGAsendCommandHPQ "FilamentControl ON"
		RGAsubcommandPending = "FilamentControl"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "FilamentControl" Then
		If RGAincomingData = "C:FilamentON1" Or RGAincomingData = "C:FilamentON2" Then
			FireLamp "�����", DeviceStateOn
			RGAsubcommandPending = ""
			RGAcommandState = RGAcomplete
			Exit Function
		ElseIf RGAincomingData = "C:FilamentOFF1" Then
			FireLamp "�����", DeviceStateOff
			OutMess "Trying 2 filament"
			RGAsendCommandHPQ "FilamentSelect 2"
			RGAsubcommandPending = "FilamentSelect"
			RGAcommandState = RGApending
			Exit Function
		ElseIf RGAincomingData = "C:FilamentOFF2" Then
			FireLamp "�����", DeviceStateOff
			OutMess "Trying 1 filament"
			RGAsendCommandHPQ "FilamentSelect 1"
			RGAsubcommandPending = "FilamentSelect"
			RGAcommandState = RGApending
			Exit Function
		Else
			FireLamp "�����", DeviceStateUnknown
			RGAcommandState = RGApending
			Exit Function
		End If
	End If
	If RGAsubcommandPending = "FilamentSelect" Then
		If RGAincomingData = "C:FilamentON1" Or RGAincomingData = "C:FilamentON2" Then
			FireLamp "�����", DeviceStateOn
			RGAsubcommandPending = ""
			RGAcommandState = RGAcomplete
			Exit Function
		ElseIf RGAincomingData = "C:FilamentOFF1" Or RGAincomingData = "C:FilamentOFF2" Then
			
			FireLamp "�����", DeviceStateOff
			RGAsendCommandHPQ "FilamentControl ON"
			RGAsubcommandPending = "FilamentControl"
			RGAcommandState = RGApending
			Exit Function
		Else
			FireLamp "�����", DeviceStateUnknown
			RGAcommandState = RGApending
			Exit Function
		End If
	End If
	RGAcommandState = RGAerror
End Function

'========================================================================================
Function RGAcommand_SA(start)
	StopFilamentTime
	StopScanTime
	'OutMess "SA:" & CStr(start)
	If start = True Then
		RGAcommandPending = "SA"
		RGAsendCommandHPQ "ScanStop"
		RGAsubcommandPending = "ScanStop"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Or IsButtonPressed("��������� �����") Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	
	If RegimeChanged = True Then
		RegimeChanged = False
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	
	reg = RegimeName
	If reg = "��������" Then
		Exit Function
	End If
	If RGAsubcommandPending = "ScanStop" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "SCANSTOP" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot ScanStop"
			RGAsendCommandHPQ "ScanStop"
			RGAsubcommandPending = "ScanStop"
			RGAcommandState = RGApending
			Exit Function
		End If
		
		OutMess "ScanStop OK"
		If reg = "������" Then
			RGAsendCommandHPQ "ScanAdd air"
		ElseIf reg = "�������_��" Then
			RGAsendCommandHPQ "ScanAdd karohv"
		Else
			RGAsendCommandHPQ "ScanAdd karo"
		End If
		'reg = "�������"
		GasCount = RawData.GetGasGroupSize(reg)
		gasnames = RawData.GetGasNames(reg)
		lastmass = RawData.GetChannel(reg, gasnames(GasCount - 1))
		RGAsubcommandPending = "ScanAdd"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "ScanAdd" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "SCANADD" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot ScanAdd"
			If reg = "������" Then
				RGAsendCommandHPQ "ScanAdd air"
			ElseIf reg = "�������_��" Then
				RGAsendCommandHPQ "ScanAdd karohv"
			Else
				RGAsendCommandHPQ "ScanAdd karo"
			End If
			'reg = "�������"
			GasCount = RawData.GetGasGroupSize(reg)
			gasnames = RawData.GetGasNames(reg)
			lastmass = RawData.GetChannel(reg, gasnames(GasCount - 1))
			RGAsubcommandPending = "ScanAdd"
			RGAcommandState = RGApending
			Exit Function
		End If
		OutMess "ScanAdd OK"
		RGAsubcommandPending = ""
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	RGAcommandState = RGAerror
End Function

'========================================================================================


Dim scanString(20)
Dim scanCounter : scanCounter = 0


Function RGAcommand_SS(start)

	'OutMess "SS:" & CStr(start)
	If start = True Then
		RGAcommandPending = "SS"
		RGAsendCommandHPQ "ScanStart 1"
		StartFilamentTimer
		StartScanTimer
		RGAsubcommandPending = "ScanStart"
		RGAmassCounter = 0
		scanCounter = 0
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Or IsButtonPressed("��������� �����") Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RegimeChanged = True Then
		RegimeChanged = False
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	reg = RegimeName
	If reg = "��������" Then
		Exit Function
	End If
	If RGAsubcommandPending = "ScanStart" Then
		If RGAincomingData <> "C:Scan Ended, Need to Resume Scan" Then
			RGAcommandState = RGApending
			Exit Function
		End If
		For i = 0 To scanCounter
			rrr = SplitAnswer(scanString(i))
			
			'OutMess  scanString(i)
			If UBound(rrr) > 2 Then
				RawData.SetValue CInt(rrr(1)), CDbl(rrr(2)) * 100000000000000
				Debug.Data(i).Title = Format(CInt(rrr(1)), 2, 0)
				Debug.Data(i).Format = "%.2f"
				Debug.Data(i).Value = CDbl(rrr(2)) * 100000000000000
			End If
		Next
		
		
		
		Calculate
		RGAsendCommandHPQ "ScanResume 1"
		StartScanTimer
		RGAsubcommandPending = "ScanStart"
		RGAmassCounter = 0
		scanCounter = 0
		RGAcommandState = RGApending
		Exit Function
	End If
	RGAcommandState = RGAerror
End Function

'========================================================================================
Function RGAcommand_EX(start)
	StopFilamentTime
	StopScanTime
	'OutMess "EX:" & CStr(start)
	If start = True Then
		RGAcommandPending = "EX"
		RGAsendCommandHPQ "ScanStop"
		RGAsubcommandPending = "ScanStop"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Or Not IsButtonPressed("��������� �����") Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "ScanStop" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 1 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) <> "SCANSTOP" Or UCase(rrr(1)) <> "OK" Then
			OutMess "Cannot ScanStop"
			RGAsendCommandHPQ "ScanStop"
			RGAsubcommandPending = "ScanStop"
			RGAcommandState = RGApending
			Exit Function
		End If
		
		OutMess "ScanStop OK"
		RGAsendCommandHPQ "FilamentControl OFF"
		RGAsubcommandPending = "FilamentControl"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "FilamentControl" Then
		If Len(RGAincomingData) < 10 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If RGAincomingData = "C:FilamentOFF1" Or RGAincomingData = "C:FilamentOFF2" Then
			
			FireLamp "�����", DeviceStateOff
			OutMess "FilamentControl OFF OK"
			RGAsubcommandPending = "FilamentControl"
			RGAcommandState = RGApending
			Exit Function
		End If
		rrr = SplitAnswer(RGAincomingData)
		If UBound(rrr) < 3 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If UCase(rrr(0)) = "FILAMENTCONTROL" And UCase(rrr(1)) = "OK" And UCase(rrr(3)) = "OFF" Then
			FireLamp "�����", DeviceStateOff
			OutMess "Filament Already OFF"
			RGAsubcommandPending = "FilamentControl"
			RGAcommandState = RGApending
			Exit Function
		End If
		RGAcommandState = RGApending
		Exit Function
	End If
	RGAcommandState = RGAerror
End Function

'========================================================================================
Function RGAcommand_NF(start)
	If start = True Then
		RGAcommandPending = "NF"
		RGAsendCommand "NF" & CStr(RGAnoiseFloor), RGAnoAnswer, 0
		RGAsendER
		RGAsubcommandPending = "NFs"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "NFs" Then
'		If Len(RGAincomingData) < 2 Then
'			RGAcommandState = RGApending
'			Exit Function
'		End If
		RGAsendCommand "NF?", 3, 1500'800'400
		RGAsubcommandPending = "NFq"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "NFq" Then
		If Len(RGAincomingData) < 2 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		If Right(RGAincomingData, 2) = vbLf & vbCr Then
			Dim actualNoiseFloor
			actualNoiseFloor = Left(RGAincomingData, Len(RGAincomingData) - 2)
			If IsNumeric(actualNoiseFloor) Then
				actualNoiseFloor = CLng(actualNoiseFloor)
'Debug.Text.Text = Debug.Text.Text & " NF=" & actualNoiseFloor
				If actualNoiseFloor <> RGAnoiseFloor Then
					RGAcommandState = RGAerror
					Exit Function
				End If
			End If
			RGAsubcommandPending = ""
			RGAcommandState = RGAcomplete
			Exit Function
		End If
	End If
	RGAcommandState = RGAerror
End Function

Dim RGAmassCounter : RGAmassCounter = 0

'========================================================================================
Function RGAcommand_MR2(start)
	'OutMess "MR"
	Dim reg: reg = RegimeName
	If reg = "��������" Then
		reg = "������"
	End If
		
	Dim GasCount: GasCount = RawData.GetGasGroupSize(reg)
	
	If RGAmassCounter >= GasCount Then
		RGAmassCounter = 0
	End If
    
	Dim gasnames : gasnames = RawData.GetGasNames(reg)
	Dim mass : mass = RawData.GetChannel(reg, gasnames(RGAmassCounter))
	If start = True Then
		RGAcommandPending = "MR"
		RGAsendCommand "MR" & mass, 4, 5000 '800
		RGAsubcommandPending = "MR"
		RGAcommandState = RGApending
		Exit Function
	End If
	If RGAsubcommandPending = "" Then
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	If RGAsubcommandPending = "MR" Then
		'OutMess CStr(Len(RGAincomingData))
		If Len(RGAincomingData) < 4 Then
			RGAcommandState = RGApending
			Exit Function
		End If
		Dim value : value = 0.0
		Dim extractor : Set extractor = New ExtractorOfNumericals
		extractor.SetSource RGAbyteData
		extractor.SetLength 4
		extractor.SetIsSigned True
		extractor.SetIsIntelOrder True
		value = extractor.GetNumerical
		'OutLog "mass= " & mass & "value =" & value					'!!!!!!!!!!!!!!!!!!!!!!!!!
		'value = CDbl(value) * 1e-2
		If LampState("�������") = DeviceStateOn Then
			value = value / RGAGain
		End If
		If value <= 0.0 Then value = 1e-20
		RawData.SetValue mass, value
		
		'������ RawData ��� ���� ������� � RawData("�������")
		Dim massKar : massKar = RawData.GetChannel("�������", gasnames(RGAmassCounter))
		If massKar <> mass Then
			RawData.SetValue massKar, value
		End If
		
		' ����� � ����� ���������� ��������, ���� ������ � ��������� ������
		Debug.Data(RGAmassCounter).Title = Format(RGAmassCounter + 1, 2, 0)
		Debug.Data(RGAmassCounter).Format = "%.2f"
		Debug.Data(RGAmassCounter).Value = value
		'If RGAmassCounter = MaxGases - 1 Then
			Calculate
		'End If
		'OutLog "After mass= " & mass & "value =" & value					'!!!!!!!!!!!!!!!!!!!!!!!!!
		RGAmassCounter = RGAmassCounter + 1
		RGAsubcommandPending = ""
		RGAcommandState = RGAcomplete
		Exit Function
	End If
	RGAcommandState = RGAerror
End Function



Function RGAcommand_MR(start)
	'OutMess "MR"
	
	'If IsButtonPressed("���������� ��� ��") Or IsButtonPressed("���������� ��� ��") Or IsButtonPressed("���������� ������� ��") Or IsButtonPressed("���������� ������� ��") Or IsButtonPressed("���������� �� �������") Then
	'	RGAcommand_MR2(start)
	'	Exit Function
	'End If
	Dim reg: reg = RegimeName
	If reg = "��������" Then
		'RGAsendMR0ER
		Exit Function
		'reg = "������"
	End If
	RGAcommandPending = "MR"
	Dim GasCount: GasCount = RawData.GetGasGroupSize(reg)
	

    
	Dim gasnames : gasnames = RawData.GetGasNames(reg)
	Dim mass
	Dim value
	Dim extractor : Set extractor = New ExtractorOfNumericals
	Dim massKar
	For i = 0 To 4
		If RGAmassCounter >= GasCount Then
			RGAmassCounter = 0
		End If
	
	
		mass = RawData.GetChannel(reg, gasnames(RGAmassCounter))
		RGAsendCommand "MR" & mass, 4, 5000 '1100
		Dim cc
		If RGASended Then
			cc = Form.rga1.GetAnswerBA
			ProcessSpectrometer cc
			RGASended = False
		End If
		If Len(RGAincomingData) < 4 Then
			RGAcommandState = RGAerror
			Exit Function
		End If
		value = 0.0
		extractor.SetSource RGAbyteData
		extractor.SetLength 4
		extractor.SetIsSigned True
		extractor.SetIsIntelOrder True
		value = extractor.GetNumerical
		'OutMess CStr(value)
		
		If LampState("�������") = DeviceStateOn Then
			value = value / RGAGain
		End If
		If value <= 0.0 Then value = 1e-20
		RawData.SetValue mass, value
		
		'������ RawData ��� ���� ������� � RawData("�������")
		massKar = RawData.GetChannel("�������", gasnames(RGAmassCounter))
		If massKar <> mass Then
			RawData.SetValue massKar, value
		End If
		
		' ����� � ����� ���������� ��������, ���� ������ � ��������� ������
		Debug.Data(RGAmassCounter).Title = Format(RGAmassCounter + 1, 2, 0)
		Debug.Data(RGAmassCounter).Format = "%.2f"
		Debug.Data(RGAmassCounter).Value = value
		
		'If RGAmassCounter = MaxGases - 1 Then
		'	Calculate
		'End If
		If RGAmassCounter = GasCount - 1 Then
			Calculate
		End If
		RGAmassCounter = RGAmassCounter + 1
	Next
	'Calculate
	RGAsubcommandPending = ""
	RGAcommandState = RGAcomplete
	Exit Function

End Function



'========================================================================================
' ������� �������� ������� � ����������
Sub RGAsendCommand(data, retLen, timeout)

	'Dim query : query = data & vbLf & vbCr
	Dim query : query = data & vbCr
	If RGASended = True Then
		OutMess "ERROR: RGASended = True"
		Exit Sub
	End If
	If retLen = RGAnoAnswer Then
		isizePending = 0
		Form.rga1.SendNoAns query, timeout
	Else
		isizePending = retLen
		If retLen = 0 Then retLen = 25
		'timeout = timeout * 2
		
		Form.rga1.SendWithAns query, timeout, retLen
	RGASended = True
	End If
End Sub

'========================================================================================
' ������� �������� ������� � ����������
Sub RGAsendCommandHPQ(data)

	Dim query : query = data & vbCr
	If RGASended = True Then
		OutMess "ERROR: RGASended = True"
		'Exit Sub
	End If
		
	DEMCommandHPQ query
	RGASended = True
End Sub

Sub RGAsendER()
	Dim query : query = "ER?" & vbCr
	If RGASended = True Then
		OutMess "ERROR: RGASended = True"
		Exit Sub
	End If
	Form.rga1.SendWithAns query, 5000, 3
	Form.rga1.GetAnswerBA
End Sub

Sub RGAsendMR0ER()

	Dim query : query = "MR0" & vbCr
	If RGASended = True Then
		OutMess "ERROR: RGASended = True"
		Exit Sub
	End If
	Form.rga1.SendNoAns query, timeout
	
	query = "ER?" & vbCr
	If RGASended = True Then
		OutMess "ERROR: RGASended = True"
		Exit Sub
	End If
	Form.rga1.SendWithAns query, 5000, 3
	Form.rga1.GetAnswerBA
End Sub

Sub RGAsendIN()
	Dim query : query = "IN" & vbCr
	If RGASended = True Then
		OutMess "ERROR: RGASended = True"
		Exit Sub
	End If
	Form.rga1.SendWithAns query, 5000, 3
	Form.rga1.GetAnswerBA
End Sub

'========================================================================================
Private isizePending : isizePending = 0

'========================================================================================
' ������� �������������� ������ � ����������� ��� �� ������������(����������)
Sub ProcessSpectrometer(data)
	'Dim n : n = data(0)
	'If (n = 0) and Not IsButtonPressed("���������� ����������") Then			'!!!!!!!!!!!!!!!!!!!!!!!!
	'	If RGAaliveCounter >= RGAaliveTimeout Or timeTail = 0 Then
'	'		OutLog "======================================================="
'	'		OutLog "======================================================="
'	'		OutLog "======================================================="
	'		'OutLog "======================================================="
	'		'OutLog "--ProcessSpectrometer: BAD RGAaliveCounter = " & RGAaliveCounter & " timeTail=" & timeTail
	'		'OutLog "RGAaliveTimeout = " & RGAaliveTimeout
	'		RGAcommandPending = ""
	'		FireLamp "�����������", DeviceStateBad
	'		FireLamp "�������", DeviceStateUnknown
	'		FireLamp "�����", DeviceStateUnknown
	'		RGAaliveCounter = 0
	'		'OutMess "�������� ������� ��������� ������ �� ������������"
	'	Else
	'		RGAaliveCounter = RGAaliveCounter + 1
	'		Exit Sub
	'	End If
	'End If
	
	
	RGASended = False
	RGAaliveCounter = 0
	RGAincomingData = ""
	
	If (RGAcommandPending = "MR" Or RGAsubcommandPending = "MR0_28" Or RGAsubcommandPending = "MR1_28") And UBound(data) = 4 Then
		'OutLog "RGAbyteData(0) = data"
		RGAbyteData(0) = data(1)
		RGAbyteData(1) = data(2)
		RGAbyteData(2) = data(3)
		RGAbyteData(3) = data(4)
		For i = 0 To 3
			If RGAbyteData(i) < 0 Then RGAbyteData(i) = RGAbyteData(i) + 255
		Next
		'OutMess CStr(RGAbyteData(0)) & " " & CStr(RGAbyteData(1)) & " " & CStr(RGAbyteData(2)) & " " & CStr(RGAbyteData(3))
		For i = 0 To 3
			RGAincomingData = RGAincomingData & Chr(RGAbyteData(i))
		Next
	Else
		
		For i = 0 To UBound(data) - 1
			If data(i) < 0 Then data(i) = data(i) + 256
			RGAincomingData = RGAincomingData & Chr(data(i))
		Next
		'OutMess "Answer: " & RGAincomingData
		'OutLog "Answer: " & RGAincomingData
		If Mid(RGAincomingData, 1, 11) = "MassReading" Then
			scanString(scanCounter) = RGAincomingData
			If (Mid(RGAincomingData, 13, 2) = CStr(lastmass)) Then
				RGAincomingData = "C:Scan Ended, Need to Resume Scan"
			Else
				scanCounter = scanCounter + 1
			End If
			'rrr = SplitAnswer(RGAincomingData)
			'If UBound(rrr) > 2 Then
			'	RawData.SetValue CInt(rrr(1)), CDbl(rrr(2))
			'	
			'	
			'	Debug.Data(RGAmassCounter).Title = Format(RGAmassCounter + 1, 2, 0)
			'	Debug.Data(RGAmassCounter).Format = "%.2f"
			'	Debug.Data(RGAmassCounter).Value = CDbl(rrr(2))
			'	RGAmassCounter = RGAmassCounter + 1
			'End If
			'If (Mid(RGAincomingData, 13, 2) = CStr(lastmass)) Then
			'	RGAincomingData = "C:Scan Ended, Need to Resume Scan"
			'	RGAmassCounter = 0
			'End If
			
		End If
		
		If Mid(RGAincomingData, 1, 14) = "FilamentStatus" Then
			
			rrr = SplitAnswer(RGAincomingData)
			If UBound(rrr) > 2 Then
				OutMess "FilamentStatus" & ":" & rrr(1) & " " & rrr(2)
				If rrr(1) = "1" Then
					If UCase(rrr(2)) = "OFF" Then RGAincomingData = "C:FilamentOFF1"
					If UCase(rrr(2)) = "ON" Then RGAincomingData = "C:FilamentON1"
				ElseIf rrr(1) = "2" Then
					If UCase(rrr(2)) = "OFF" Then RGAincomingData = "C:FilamentOFF2"
					If UCase(rrr(2)) = "ON" Then RGAincomingData = "C:FilamentON2"				
				End If
			End If
		End If
		
	End If
	
	
End Sub


Function SplitArgs2(sstr)
   sArgs = ""
   sChar = ""
   nCount = 0
   bQuotes = False
   lastchar = ""
   For nCount = 1 To Len(sstr)
      sChar = Mid(sstr, nCount, 1)
	  If sChar = vbCr Then sChar = Chr(32)
	  If sChar = vbLf Then sChar = Chr(32)
      If sChar = Chr(34) Then
         bQuotes = Not bQuotes
      End If
      If sChar = Chr(32) Then
         If bQuotes Then
            sArgs = sArgs & sChar
         ElseIf lastchar <> Chr(32) Then
            sArgs = sArgs & Chr(0)
         End If
      Else
         sArgs = sArgs & sChar
      End If
	  lastchar = sChar
   Next
   SplitArgs2 = sArgs
End Function

Function SplitAnswer(sstr)
   SplitAnswer = Split(SplitArgs2(sstr), Chr(0))
End Function