Public InAnalysis
Public AutoStartRequired
Public AutoRoutes

Private AnalysisCounter
Public CH4Limit1
Public CH4Limit2

Public JSUMAir00
Public JSUMGasLow00
Public JSUMGasHi00

Private CalibrationAirPeriod		'������������� ���������� �� ������� ��� ���������� CH4
Private FoneHiPeriod	 			'������������� ����� ���� � ������ ������� ����������������
Dim CalibrationAirCounter: CalibrationAirCounter = 0
Dim FoneHiCounter: FoneHiCounter = 0

Public IsAutoCalibration			'���� ���������� � �������������� ������ ��� ���������� CH4
Public NeedReturnToAuto				'���� ������������� �������� � ��������� (������������ �� �����, ���� ������ "������" ������)

Public RegimeName: RegimeName = "��������"		'"��������", "������", "�������_��", "�������_��"

Dim StabilizationTime

Sub InitializeAuto
	Set InAnalysis = New FlagsCollection
	Set AutoRoutes = New NamesCollection
	CurrentCyclogramIndex = -1
	IsAutoCalibration = False
	InAnalysis.SetFlag "����", False
	With Form.IniFile
		.Read IniDir & "Auto.ini"
		.Section = "Auto"
		.Parameter = "StartOnLoad"
		If .ParameterValue = UCase("True") Then
			AutoStartRequired = True
		Else
			AutoStartRequired = False
		End If
		.Parameter = "Routes"
		If .ParameterValue = "" Then
			AutoRoutes.AddName "������"
		Else
			Dim swnames : swnames = .AllSwitches
			Dim sw : For Each sw In swnames
				.Switch(sw)
				AutoRoutes.AddName sw
			Next
		End If
		.Parameter = "CH4Limit1"
		If .ParameterValue = "" Then
			OutLog "CH4Limit1 �� �����"
			CH4Limit1 = CDbl(0.1)
		Else
			CH4Limit1 = CDbl(.ParameterValue)
		End If
		.Parameter = "CH4Limit2"
		If .ParameterValue = "" Then
			OutLog "CH4Limit2 �� �����"
			CH4Limit2 = CDbl(0.2)
		Else
			CH4Limit2 = CDbl(.ParameterValue)
		End If
		.Parameter = "StabilizationTime"
		If .ParameterValue = "" Then
			OutLog "StabilizationTime �� �����"
			StabilizationTime = CDbl(0.2)
		Else
			StabilizationTime = CDbl(.ParameterValue)
		End If
		If (CH4Limit2 < CH4Limit1) Then
			OutLog "CH4Limit2 ������ ���� ������ CH4Limit1"
		End If
		.Parameter = "JSUMAir00"
		If .ParameterValue = "" Then
			OutLog "JSUMAir00 �� �����"
			JSUMAir00 = CDbl(0.1)
		Else
			JSUMAir00 = CDbl(.ParameterValue)
		End If
		.Parameter = "JSUMGasLow00"
		If .ParameterValue = "" Then
			OutLog "JSUMGasLow00 �� �����"
			JSUMGasLow00 = CDbl(0.1)
		Else
			JSUMGasLow00 = CDbl(.ParameterValue)
		End If
		.Parameter = "JSUMGasHi00"
		If .ParameterValue = "" Then
			OutLog "JSUMGasHi00 �� �����"
			JSUMGasHi00 = CDbl(0.1)
		Else
			JSUMGasHi00 = CDbl(.ParameterValue)
		End If
		.Parameter = "CalibrationAirPeriod"
		If .ParameterValue = "" Then
			OutLog "CalibrationAirPeriod �� �����"
			CalibrationAirPeriod = CLng(10000)
		Else
			CalibrationAirPeriod = CLng(.ParameterValue)
		End If
		.Parameter = "FoneHiPeriod"
		If .ParameterValue = "" Then
			OutLog "FoneHiPeriod �� �����"
			FoneHiPeriod = CLng(30000)
		Else
			FoneHiPeriod = CLng(.ParameterValue)
		End If
	End With
End Sub

Sub OnAutoStart
	InAnalysis.SetSelectedFlags "����", False
	NeedReturnToAuto = True
	CalibrationAirCounter = 0
	FoneHiCounter = 0
	DisableAllButtons
	Cyclograms.StopAllCyclograms
	Cyclograms.StartCyclogram "������"
	EnableButton "�������"
	Threads.SetCurrentSub "������", ""
End Sub

Sub OnAutoStop
	If Cyclograms.IsCyclogramStarted("���������� ��� ��") Or Cyclograms.IsCyclogramStarted("���������� �� �������")  Then
		NeedReturnToAuto = False
		OnCalibrationEnd
	End If
	Threads.SetCurrentSub "������", ""
	Cyclograms.StopAllCyclograms
	Cyclograms.StartCyclogram "�������� ���������"
'	VCU.rbtManual.Value = True
	InAnalysis.SetSelectedFlags "����", False
	EnableAllButtons
	SetRegime "��������" '��������2
End Sub

Private InFoneCalibration

Sub OnAnalysisRoute
	AnalysisCounter = 0
	InFoneCalibration = False
	InAnalysis.SetFlag "����", True
	Threads.SetCurrentSub "������", "AnalysisGasCycle"
End Sub

Sub AnalysisGasCycle
	If IsAutoCalibration Then
		Exit Sub
	End If

	If AnalysisCounter < StabilizationTime Then
		AnalysisCounter = AnalysisCounter + 1
		Exit Sub
	End If
	
	Dim perc: perc = Percents.GetValues("�������")
	Dim units: units = AttributesOfGases.GetAllUnits
	Dim iCH4: iCH4 = GetGasIndex("CH4")
	'Dim iCH4: iCH4 = GetGasIndex("O2")
	
	Dim CH4
	If units(iCH4) = "%" Then
		CH4 = perc(iCH4) * 10000
	Else
		CH4 = perc(iCH4)
	End If
	
'	If RegimeName = "������" And CH4 >= CH4Limit1 Then
'		SetRegime "�������_��"'SetRegime "�������_��"
'		AnalysisCounter = 0
''	ElseIf RegimeName = "�������_��" And CH4 < CH4Limit1 Then
'	ElseIf RegimeName = "�������_��" And CH4 < CH4Limit1 Then
'		SetRegime "������"
'		AnalysisCounter = 0
''	ElseIf RegimeName = "�������_��" And CH4 >= CH4Limit2 Then
'	ElseIf RegimeName = "�������_��" And CH4 >= CH4Limit2 Then
'		SetRegime "�������_��"
'		AnalysisCounter = 0
''	ElseIf RegimeName = "�������_��" And CH4 < CH4Limit2 Then
'	ElseIf RegimeName = "�������_��" And CH4 < CH4Limit2 Then
'		SetRegime "�������_��"
'		AnalysisCounter = 0
'	End If
	
	If RegimeName = "������" And CH4 >= CH4Limit2 Then
		SetRegime "�������_��"
		AnalysisCounter = 0
	ElseIf RegimeName = "�������_��" And CH4 < CH4Limit1 Then
		SetRegime "������"
		AnalysisCounter = 0
	End If
	
	If RegimeName = "������" Then
		If CalibrationAirCounter < CalibrationAirPeriod Then
			CalibrationAirCounter = CalibrationAirCounter' + 1
		Else
			OutLog "CH4 �� ��������� �������� ���������� �������, �������� ���������� �� �������"
			CalibrationAirCounter = 0
			IsAutoCalibration = True
			OnCalibrationAirStart
		End If
		If FoneHiCounter < FoneHiPeriod Then
			FoneHiCounter = FoneHiCounter + 1
		Else
			OutLog "CH4 �� ��������� �������� ���������� �������, �������� ���� ���� � ������ ������� ����������������"
			FoneHiCounter = 0
			IsAutoCalibration = True
			OnFoneHiCalibrationStart
		End If
	Else
		CalibrationAirCounter = 0
		FoneHiCounter = 0
	End If
End Sub 


Sub SetRegime (newreg)
	If newreg = "��������2" Then
		'�������� ��� ������ ��� �������� (�.� ���� ������������ ������ ��� ���� �������)
		Dim rd : rd = RawData.GetValues("�������")
		Dim i : For i = 0 To RawData.GetGasGroupSize("�������") - 1
			rd(i) = 0
		Next
		RawData.SetValues "�������", rd
	
		RegimeName = newreg
		OutLog "������� ����� " & RegimeName
		Exit Sub
	End If
	If RegimeName = newreg Then
		Exit Sub
	End If
	
	If newreg = "������" Or newreg = "�������_��" Then
	'	RgaMultOn
	ElseIf newreg = "�������_��" Or newreg = "��������" Then
	'	RgaMultOff
	Else
		OutLog "������. ������� ����������� ����������� ����� " & newreg
		Exit Sub
	End If
	
	

	
	
	'�������� ��� ������ ��� �������� (�.� ���� ������������ ������ ��� ���� �������)
	rd = RawData.GetValues("�������")
	For i = 0 To RawData.GetGasGroupSize("�������") - 1
		rd(i) = 0
	Next
	RawData.SetValues "�������", rd
	
	

	
	RegimeName = newreg
	OutLog "������� ����� " & RegimeName
	RegimeChanged = True
End Sub
