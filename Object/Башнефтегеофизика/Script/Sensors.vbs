Const NumberOfSensors = 14
Public JsumSensor: JsumSensor = 0
Public DDMSensor: DDMSensor = 0

Sub ProcessSensors(data)
	Dim s : For Each s In VCU.Sensors
		Dim sname : sname = s.Title
		If sname = "��������" Then
			s.Value = JsumSensor
		Else
			Dim value : value = data(SensorsMap.GetCode(sname) - 1)
			Dim formula : formula = SensorsFormula.GetText(sname)
			s.Value = ProcessFormula(formula, value)
		End If
		
		If sname = "��" Then
			s.Format = "%.0f"
		ElseIf sname = "���" or sname = "���" Then
			s.Format = "%.3f"
		Else
			s.Format = "%.1f"
		End If
		
		If Not IsButtonPressed("���� ��������") Then
			With Form.IniFile
				.Read IniDir & "Sensors.ini"
				.Section = "CalibCorrectors"
				'--
				If sname = "���" Then
					.Parameter = "DDM"
					If .ParameterValue = "" Then	' ���� ��� ������ ��������� � ��������
						OutLog "������! ����������� �������� 'DDM' � [CalibCorrectors] � Sensors.ini"
						Exit Sub
					End If
					s.Value = s.Value - .ParameterValue
				'--
				ElseIf sname = "��" Then
					.Parameter = "DR"
					If .ParameterValue = "" Then	' ���� ��� ������ ��������� � ��������
						OutLog "������! ����������� �������� 'DR' � [CalibCorrectors] � Sensors.ini"
						Exit Sub
					End If
					s.Value = s.Value - .ParameterValue
				End If
				'--
			End With
		End If
		If sname = "���" Then
			DDMSensor = s.Value
			If DDMSensor = -1 Then
				OutLog "��������. �������� ������� ��� ����� -1. � ��������� ������ ������� �� 0 � �������� ������������ -0.999"
				DDMSensor = -0.999
			End If
		End If
	Next
	
	For i = 0 To NumberOfSensors - 1
		Debug.Sensors(i).Title = Format(i + 1, 2, 0)
		Debug.Sensors(i).Format = "%.0f"
		Debug.Sensors(i).Value = data(i)
	Next
	
	Dump "Sensors"
	Dump "DebugSensors"
End Sub
