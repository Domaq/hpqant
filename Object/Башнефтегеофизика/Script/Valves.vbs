Const NumberOfValves = 127
Public OutValvesState(3) : OutValvesState(0) = CLng(0)

Dim vvs0Prev : vvs0Prev = 777
Dim vvs1Prev : vvs1Prev = 777

Private K2K15S1 : K2K15S1 = False
Private K2K15S2 : K2K15S2 = False
Private K3K4S1 : K3K4S1 = False
Private K3K4S2 : K3K4S2 = False

Private firstValves(3)

Sub ControlValves
	Dim index : index = 0
	Dim stepen : stepen = 0
	'---------------
	Dim vvs(3)
	vvs(0) = CLng(0)
	vvs(1) = CLng(0)
	vvs(2) = CLng(0)
	vvs(3) = CLng(0)
	Debug.Text.Text =  ""				'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	Dim v : For Each v In VCU.Valves

		'Debug.Text.Text = Debug.Text.Text + UCase(v.Title & v.TitleExt) & "=" & ValvesMap.GetCode(UCase(v.Title & v.TitleExt)) & "    ||||    "
		If v.ButtonChecked = True Then		'���� �������� �� ������ �� ���
		
			Dim code : code = ValvesMap.GetCode(UCase(v.Title & v.TitleExt))
			
			'OutMess "�12.���2 = " & code
			index = Int(code/32)
			'OutMess "Index = " & index
			stepen = code - 1
			while stepen > 31
				stepen = stepen - 32
			Wend
			vvs(index) = vvs(index) + 2 ^ stepen
			
			
			
'			Debug.Text.Text = " code=" & code & " index=" & index & " stepen=" & stepen & "vvs(index)=" & Hex(vvs(index))'!!!!!!!!!!!
		End If															
		
		
	Next
	'---------------
	Dim dvs(3) 
	dvs(0) = CLng(0)
	dvs(1) = CLng(0)
	dvs(2) = CLng(0)
	dvs(3) = CLng(0)
	For i = 0 To NumberOfValves - 1
		index = Int(i/32)
		stepen = i
		while stepen > 31
			stepen = stepen - 32
		Wend
		If Debug.btnValves(i).Value = 1 Then	' ������ �������� � ����� ����
			dvs(index) = dvs(index) + 2 ^ stepen
			Debug.Text.Text = Debug.Text.Text +  " i=" & i & " index=" & index & " stepen=" & stepen & "dvs(index)=" & Hex(dvs(index))'!!!!!!!!!!!
		End If
	Next
	'----------------
	For i = 0 To 3
		OutValvesState(i) = vvs(i) Or dvs(i) 	
	Next
'	OutMess "OutValvesState(0) = " & OutValvesState(0)	'16897	
'	OutMess "OutValvesState(1) = " & OutValvesState(1)		
'	OutMess "OutValvesState(2) = " & OutValvesState(2)		
'	OutMess "OutValvesState(3) = " & OutValvesState(3)
	If vvs0Prev <> OutValvesState(0) Then
		vvs0Prev = OutValvesState(0)
		'OutMess "vvs0Prev = " & vvs0Prev
		WriteToExt1
	End If
End Sub



	

Private modbustimer : modbustimer = Now()


Sub WriteToExt1
	If (Now() > DateAdd("s", 2, modbustimer)) Then DataTransfer1 = True
	'OutMess CStr(DataTransfer1)
	If DataTransfer1 Then
		modbustimer = Now()
		DataTransfer1 = False
		'OutMess "WriteToExt1"
		'-------------Work with map-----------------------------
		'Write to local map
		Dim arrTemp(0)
		arrTemp(0) = OutValvesState(0)
		
		addr = 470	'����� ��������, � �������� �������� ������
		nb = 2 		'���������� ����
		ft = 16 	'FunctionType (Write_Multiple_Registers = 16)
		bo = 2		'ByteOrder (ABCD = 0, DCBA = 1)
	
		rc = Form.WriteRegistrsWithLongTypeToMap1(arrTemp,addr, nb, ft, bo)
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
			res = Form.InfoModbus1()
			OutMess res & ". WriteToExt1 ��� ������: " & CStr(rc)
		End If
		'Write to remote slave
		index = 0	'������ ���������� (� ����� ������ ��� ����, ������� ������ 0)
		
		rc = Form.GModbus1.modbusWriteRegistersToRemoteSlave(addr, nb, index)
		
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
			res = Form.InfoModbus1()
			'OutMess res & ". WriteToExt1 ��� ������: " & CStr(rc)
		End If
		'----
		DataTransfer1 = True
	End If
End Sub

Sub ReadFromExt1

	If DataTransfer1 Then
		DataTransfer1 = False
		'Read data form remote slave and write to local slave
		addr = 468	'����� ��������, � �������� �������� ������
		nb = 2 		'���������� ���� ( 1 �������� �� 2 ����� = 2)
		index = 0	'������ ���������� (� ����� ������ ��� ����, ������� ������ 0)
		bo = 2		'ByteOrder (ABCD = 0)
		dt = 3 		'��� ������ ������������� �������� (LONGS)
		ft = 3 		'FunctionType (Read_Holding_Registers = 3)	
		rc = Form.GModbus1.modbusReadRegistersFromRemoteSlave(addr, nb, index)
		If (rc <> 1)  Then 'ResultStatuses::_RC_SUCCES_ = 1
			res = Form.InfoModbus1()
			'OutMess res & ". ControlDetectors3 ��� ������: " & CStr(rc)
		Else
			readenArray1 = Form.ReadRegistrsWithLongTypeFromMap1(addr, nb, ft, bo, dt)
			firstValves(0) = readenArray1(0,0)
		End If
		'Read data from local salve to vbs array
		'OutMess "firstDetectors(0) = " & firstDetectors(0)
		'ProcessDetectors firstDetectors
		DataTransfer1 = True
	End If
	
	ProcessValves firstValves

End Sub


Sub ProcessValves(data)
	Dim I_BIG : I_BIG = 0				' ������ � data [0..3]
	Dim index : index = 0
	Dim stepen : stepen = 0
	'If Not IsButtonPressed("����������") Then
	'	Dim v : For Each v In VCU.Valves
	'		Dim code : code = ValvesMap.GetCode(UCase(v.Title & v.TitleExt))
	'		' ��������� ������ � �������
	'		I_BIG = 0
	'		If code > 31 Then I_BIG = 1
	'		If code > 63 Then I_BIG = 2
	'		If code > 95 Then I_BIG = 3
	'		' �������
	'		stepen = code - 1
	'		while stepen > 31
	'			stepen = stepen - 32
	'		Wend
	'		
	'		Dim tmp : tmp = Int(data(I_BIG) / (2 ^ stepen))
	'		tmp = tmp - Int(tmp / 2) * 2
	'		If (tmp = 1) Then 
	'			v.ButtonChecked = True
	'		Else
	'			v.ButtonChecked = False
	'		End If
	'	Next
	'	ControlValves
	'End If

	If OutValvesState(0) <> data(0) Or OutValvesState(1) <> data(1) Or OutValvesState(2) <> data(2) Or OutValvesState(3) <> data(3) Then
		FireLamp "�������", DeviceStateBad
	Else
		If OutValvesState(0) = CLng(0) And OutValvesState(1) = CLng(0) And OutValvesState(2) = CLng(0) And OutValvesState(3) = CLng(0) Then
			FireLamp "�������", DeviceStateOff
		Else
			FireLamp "�������", DeviceStateOn
		End If
	End If
	

	
	For i = 0 To NumberOfValves - 1
		' ��������� ������ � �������
		I_BIG = 0
		If i > 31 Then I_BIG = 1
		If i > 63 Then I_BIG = 2
		If i > 95 Then I_BIG = 3
		' �������
		stepen = i
		while stepen > 31
			stepen = stepen - 32
		Wend
		
		Dim checked : checked = Int(data(I_BIG) / (2 ^ stepen))
		checked = checked - Int(checked / 2) * 2
		If (checked = 0) Then 
			Debug.Valves(i).LampColor = RGB(0, 0, 255)
		Else
			Debug.Valves(i).LampColor = RGB(0, 255, 0)
		End If
		'-------��������� ������������----------------
		If i = 3 Then'---������������ ������ 7---
			If (checked = 0) Then
				FireLamp "��������� �7", DeviceStateOn
			Else
				FireLamp "��������� �7", DeviceStateBad
			End If
		ElseIf i = 4 Then'---������������ ������ 8---
			If (checked = 0) Then
				FireLamp "��������� �8", DeviceStateOn
			Else
				FireLamp "��������� �8", DeviceStateBad
			End If
		ElseIf i = 5 Then'---������������ ���������---
			If (checked = 0) Then
				FireLamp "��������", DeviceStateOn
			Else
				FireLamp "��������", DeviceStateBad
			End If		
		ElseIf i = 6 Then'---������������ ���� �---
			If (checked = 0) Then
				FireLamp "��� �", DeviceStateOn
			Else
				FireLamp "��� �", DeviceStateBad
			End If			
		ElseIf i = 7 Then'---������������ ���� �---
			If (checked = 0) Then
				FireLamp "��� �", DeviceStateOn
			Else
				FireLamp "��� �", DeviceStateBad
			End If		
		End If
		'--------------------------------------------		
	Next
End Sub

Sub OnValvesAuto
	Dim v: For Each v In VCU.Valves
		v.ButtonChecktype = False
	Next
	'If Not IsButtonPressed("�������") _
	'		And Not IsButtonPressed("����������") _
	'		And Not IsButtonPressed("��� �������") _
	'		And Not Cyclograms.IsCyclogramStarted("�������� ���������") _
	'		And Not Cyclograms.IsCyclogramStarted("���������� ������") _
	'		Then
	'	PressButton "�������"
	'End If
End Sub

Sub OnValvesManual
	If IsButtonPressed("�������") Then
		ReleaseButton "�������"
	End If
	If IsButtonPressed("����������") Then
		ReleaseButton("����������")
	End If
	Cyclograms.StopAllCyclograms
'	ReleaseSelectedButtons "������"
	Dim v: For Each v In VCU.Valves
		v.ButtonChecked = False
		v.ButtonChecktype = True
	Next
End Sub

Sub OnValvesHide
	Form.chkHydraulicScheme.Value = 0
End Sub