@echo off

title=Uninstalling GS "Granat" software
echo **************************************************************
echo * �ந�������� 㤠����� �ணࠬ����� ���ᯥ祭�� �� "�࠭��" *
echo **************************************************************

regedit /s UnRegister.reg

regsvr32 -s -u ..\Program\GButton.ocx
regsvr32 -s -u ..\Program\GIniFile.ocx
regsvr32 -s -u ..\Program\GTxt.ocx
regsvr32 -s -u ..\Program\GTime.ocx
regsvr32 -s -u ..\Program\SComm.ocx
regsvr32 -s -u ..\Program\SGraph.ocx
regsvr32 -s -u ..\Program\GDDEServ.ocx
regsvr32 -s -u ..\Program\GDatabaseFinal.ocx
regsvr32 -s -u ..\Program\GModBus.ocx
regsvr32 -s -u ..\Program\RGA.ocx


regsvr32 -s -u ..\Program\MCI32.ocx

if exist ..\Program\GButton.oca del /F /Q ..\Program\GButton.oca
if exist ..\Program\GIniFile.oca del /F /Q ..\Program\GIniFile.oca
if exist ..\Program\GIpServ.oca del /F /Q ..\Program\GIpServ.oca
if exist ..\Program\GTxt.oca del /F /Q ..\Program\GTxt.oca
if exist ..\Program\GTime.oca del /F /Q ..\Program\GTime.oca
if exist ..\Program\SComm.oca del /F /Q ..\Program\SComm.oca
if exist ..\Program\SGraph.oca del /F /Q ..\Program\SGraph.oca
if exist ..\Program\GDDEServ.oca del /F /Q ..\Program\GDDEServ.oca
if exist ..\Program\MSI32.oca del /F /Q ..\Program\MSI32.oca

regsvr32 -s -u ..\Program\mscomctl.ocx
if exist "%windir%\system32\msscript.ocx" regsvr32 -s "%windir%\system32\msscript.ocx"
regsvr32 -s -u ..\Program\mscomm32.ocx
if exist "%windir%\system32\mscomm32.ocx" regsvr32 -s "%windir%\system32\mscomm32.ocx"
regsvr32 -s -u ..\Program\msscript.ocx
if exist "%windir%\system32\msscript.ocx" regsvr32 -s "%windir%\system32\msscript.ocx"
regsvr32 -s -u ..\Program\richtx32.ocx
if exist "%windir%\system32\richtx32.ocx" regsvr32 -s "%windir%\system32\richtx32.ocx"
regsvr32 -s -u ..\Program\mswinsck.ocx
if exist "%windir%\system32\mswinsck.ocx" regsvr32 -s "%windir%\system32\mswinsck.ocx"

if exist ..\Program\mscomctl.oca del /F /Q ..\Program\mscomctl.oca
if exist ..\Program\mscomm32.oca del /F /Q ..\Program\mscomm32.oca
if exist ..\Program\msscript.oca del /F /Q ..\Program\msscript.oca
if exist ..\Program\richtx32.oca del /F /Q ..\Program\richtx32.oca
if exist ..\Program\mswinsck.oca del /F /Q ..\Program\mswinsck.oca

del /F /Q ..\*Ant.lnk
for /L %%a in (1, 1, 5) do (del /F /Q ..\*Ant%%a.lnk)

title=Done
echo **************************************************************
echo *  �������� �ணࠬ����� ���ᯥ祭�� �� "�࠭��" �����襭�   *
echo **************************************************************
