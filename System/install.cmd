@echo off

title=Installing GS "Granat" software
echo ***************************************************************
echo * �ந�������� ��⠭���� �ணࠬ����� ���ᯥ祭�� �� "�࠭��" *
echo ***************************************************************

cd ..
set root=%CD%

regsvr32 -s .\Program\GButton.ocx
regsvr32 -s .\Program\GIniFile.ocx
regsvr32 -s .\Program\GIpServ.ocx
regsvr32 -s .\Program\GTxt.ocx
regsvr32 -s .\Program\GTime.ocx
regsvr32 -s .\Program\SComm.ocx
regsvr32 -s .\Program\SGraph.ocx
regsvr32 -s .\Program\GDDEServ.ocx
regsvr32 -s .\Program\GDatabaseFinal.ocx
regsvr32 -s .\Program\GModBus.ocx
regsvr32 -s .\Program\RGA.ocx

regsvr32 -s .\Program\mscomm32.ocx
regsvr32 -s .\Program\mscomctl.ocx
regsvr32 -s .\Program\msscript.ocx
regsvr32 -s .\Program\richtx32.ocx
regsvr32 -s .\Program\mswinsck.ocx
regsvr32 -s .\Program\MCI32.ocx

regsvr32 -s .\Program\msstdfmt.dll


cd .\Object

dir .\ /ad /b /oe > _1._
for /F "delims=" %%i in (_1._) do (
	if not %%i == Common (
		set /a counter += 1
	)
)
if %counter% == 1 (
	set qualify=false
) else (
	set qualify=true
)
set object=%root%\Object\%%i
if %qualify% == true (
	set arg1=%%i...
)
for /F "delims=" %%i in (_1._) do (
	if not %%i == Common (
		setlocal
		cd %%i
		if not exist "%object%\Archive" mkdir "%object%\Archive"
		if exist "%root%\Program\Ant.exe" "%root%\Program\shortcut.exe" /o '%root%\Program\Ant.exe' /l '%root%\Ant.lnk' /w '%root%\Program' /a '!%%i!'
		for /L %%a in (1, 1, 5) do (if exist "%root%\Program\Ant%%a.exe" "%root%\Program\shortcut.exe" /o '%root%\Program\Ant%%a.exe' /l '%root%\Ant%%a.lnk' /w '%root%\Program' /a '!%%i!')
		cd ..\
		endlocal
	)
)
del _1._

cd ..\

rem if exist "%root%\Program\Ant.exe" .\Program\shortcut.exe /o '%root%\Program\Ant.exe' /l '%root%\Ant.lnk' /w '%root%\Program' /a '(������⠫� O2)'

cd .\System

rem ��������� ����������� ����� � ������� �����
reg add "HKCU\Control Panel\International" /v sDecimal /t REG_SZ /d "." /f
reg add "HKU\.DEFAULT\Control Panel\International" /v sDecimal /t REG_SZ /d "." /f

rem reg add "HKCU\Software\Ant\2\3" /ve /t REG_SZ /d "%root%" /f
rem �������� � ������ ������ � GranatIO.Data
reg add "HKEY_CLASSES_ROOT\CLSID\{51868261-AE4E-434D-A23B-B13977FE1220}\LocalServer32" /ve /t REG_SZ /d "%root%\Program\GranatIO.exe" /f
reg add "HKEY_CLASSES_ROOT\TypeLib\{FD44AF77-EFEE-4B1C-B239-32CED1B923C0}\20.0\0\win32" /ve /t REG_SZ /d "%root%\Program\GranatIO.exe" /f
reg add "HKEY_CLASSES_ROOT\TypeLib\{FD44AF77-EFEE-4B1C-B239-32CED1B923C0}\20.0\HELPDIR" /ve /t REG_SZ /d "%root%\Program" /f

set root=

regedit /s Register.reg

title=Done
echo ***************************************************************
echo *  ��⠭���� �ணࠬ����� ���ᯥ祭�� �� "�࠭��" �����襭�   *
echo ***************************************************************
